-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-12-2014 a las 17:17:59
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `departarriendo`
--
CREATE DATABASE IF NOT EXISTS `sistemasad` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sistemasad`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

DROP TABLE IF EXISTS `administrador`;
CREATE TABLE IF NOT EXISTS `administrador` (
  `usuario` varchar(40) NOT NULL,
  `contrasenia` varchar(200) NOT NULL,
  `correoElectronico1` varchar(100) NOT NULL,
  `correoElectronico2` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`usuario`, `contrasenia`, `correoElectronico1`, `correoElectronico2`) VALUES
('admin', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', 'admin@departarriendo.cl', 'admin@departarriendo.cl');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudades`
--

DROP TABLE IF EXISTS `ciudades`;
CREATE TABLE IF NOT EXISTS `ciudades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `ciudades`
--

INSERT INTO `ciudades` (`id`, `nombre`) VALUES
(1, 'Talca'),
(2, 'Santiago'),
(3, 'Concepción'),
(4, 'Valdivia'),
(5, 'Antofagasta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `CIF` int(11) NOT NULL,
  `tratamientoCliente` varchar(10) NOT NULL,
  `nombreCliente` varchar(40) NOT NULL,
  `apellidoCliente` varchar(40) NOT NULL,
  `sexoCliente` varchar(1) NOT NULL,
  `fechaNacCliente` date NOT NULL,
  `nacionalidad` int(11) NOT NULL,
  `email` varchar(80) NOT NULL,
  `emailAlt` varchar(80) NOT NULL,
  `telefonoContacto` int(15) NOT NULL,
  `telefonoContactoAlt` int(15) NOT NULL,
  `skypeCliente` varchar(80) NOT NULL,
  `whatsappCliente` int(11) NOT NULL,
  `profesion` varchar(80) NOT NULL,
  `contacto` int(11) NOT NULL,
  `tipoTarjetaCliente` int(11) NOT NULL,
  `numTarjetaCliente` int(11) NOT NULL,
  PRIMARY KEY (`CIF`),
  KEY `clientesFK1` (`nacionalidad`),
  KEY `clientesFK2` (`contacto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`CIF`, `tratamientoCliente`, `nombreCliente`, `apellidoCliente`, `sexoCliente`, `fechaNacCliente`, `nacionalidad`, `email`, `emailAlt`, `telefonoContacto`, `telefonoContactoAlt`, `skypeCliente`, `whatsappCliente`, `profesion`, `contacto`, `tipoTarjetaCliente`, `numTarjetaCliente`) VALUES
(0, '0', 'Cliente ', 'No asignado', '0', '1947-12-31', 3, 'Sin correo', 'Sin Correo', 1234, 1234, 'No posee', 223423423, 'No posee', 2, 2, 1234567),
(324141, '0', 'Ernestino', 'Tapia', '0', '1947-12-31', 3, 'ernestino@fxabogados.cl', 'ern@tapia.cl', 234234, 124124, 'ernestino.cl', 223423423, 'Abogado', 2, 2, 21412312),
(8974254, '0', 'Hal', '9000', '0', '1969-12-31', 17, 'hal@9000.us', 'hal_1@9000.us', 9898989, 9898989, 'hal.9000', 566546, 'Máquina', 1, 1, 3424234),
(81188118, '0', 'Diego', 'Vergara', '0', '1992-11-20', 41, 'diego@vergara.cl', '', 4045054, 450245254, 'elDiego', 2145254254, 'Estudiante', 1, 1, 2147483647),
(213123123, '0', 'John', 'Lennon', '0', '1940-10-08', 89, 'johnlennon@hotmail.com', '', 1313131313, 1313131313, '', 32423, 'Músico', 1, 1, 3453453);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

DROP TABLE IF EXISTS `contactos`;
CREATE TABLE IF NOT EXISTS `contactos` (
  `idContacto` int(11) NOT NULL AUTO_INCREMENT,
  `nombreContacto` varchar(40) NOT NULL,
  PRIMARY KEY (`idContacto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `contactos`
--

INSERT INTO `contactos` (`idContacto`, `nombreContacto`) VALUES
(1, 'Despegar.com'),
(2, 'Booking.com'),
(3, 'Hotels.com'),
(4, 'Agoda.com'),
(5, 'Expedia.com'),
(6, 'Wotif.com'),
(7, 'Trivago.com'),
(8, 'HostelWorld.com'),
(9, 'PriceLine.com'),
(10, 'TripAdvisor.cl'),
(11, 'Airbnb.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

DROP TABLE IF EXISTS `departamentos`;
CREATE TABLE IF NOT EXISTS `departamentos` (
  `idEdificio` int(11) NOT NULL,
  `numDpto` int(11) NOT NULL,
  `numPiso` int(11) NOT NULL,
  `orientacion` varchar(10) NOT NULL,
  `cantDormitorios` int(11) NOT NULL,
  `cantBanios` int(11) NOT NULL,
  `metrosConstruidos` int(11) NOT NULL,
  `capacidadPersonas` int(11) NOT NULL,
  `descripcion` varchar(1000) NOT NULL,
  `dispWIFI` tinyint(1) NOT NULL,
  `dispCocina` tinyint(1) NOT NULL,
  `dispCable` tinyint(1) NOT NULL,
  `dispRefrigerador` tinyint(1) NOT NULL,
  `dispCalefactor` tinyint(1) NOT NULL,
  `dispMicroondas` tinyint(1) NOT NULL,
  PRIMARY KEY (`idEdificio`,`numDpto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`idEdificio`, `numDpto`, `numPiso`, `orientacion`, `cantDormitorios`, `cantBanios`, `metrosConstruidos`, `capacidadPersonas`, `descripcion`, `dispWIFI`, `dispCocina`, `dispCable`, `dispRefrigerador`, `dispCalefactor`, `dispMicroondas`) VALUES
(1, 305, 3, '0', 2, 2, 80, 3, 'Cocina Americana', 1, 1, 0, 1, 0, 1),
(3, 333, 3, '6', 1, 1, 500, 5, 'fsvas', 1, 0, 0, 0, 0, 0),
(5, 801, 8, '6', 2, 2, 100, 4, 'holaaaaa', 1, 1, 0, 1, 1, 0),
(6, 12, 10, '6', 4, 2, 152, 10, 'El software comprado es una caja negra', 1, 1, 1, 1, 0, 0),
(6, 444, 4, '0', 1, 1, 80, 2, 'vklhbdajv', 0, 1, 0, 0, 0, 0),
(6, 901, 9, '6', 2, 2, 100, 4, 'El software comprado es una caja negra', 1, 1, 1, 1, 1, 1),
(7, 100, 18, '0', 4, 2, 90, 6, 'Excelente vista norte.', 1, 1, 1, 1, 1, 1),
(8, 123, 1, '0', 1, 1, 40, 1, 'gdsfgtq', 1, 0, 0, 0, 0, 0),
(8, 318, 3, '7', 1, 1, 80, 2, 'Departamento Studio', 0, 1, 0, 1, 0, 1),
(8, 322, 3, '7', 1, 1, 50, 2, 'Studio', 0, 1, 0, 1, 0, 1),
(11, 555, 5, '5', 2, 2, 90, 3, 'El software comprado es una caja negra', 1, 1, 1, 0, 0, 0),
(11, 999, 8, '0', 1, 1, 1, 1, 'fgdfsb', 0, 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `edificios`
--

DROP TABLE IF EXISTS `edificios`;
CREATE TABLE IF NOT EXISTS `edificios` (
  `idEdificio` int(11) NOT NULL AUTO_INCREMENT,
  `nombreEdificio` varchar(40) NOT NULL,
  `nombreAdministrador` varchar(40) NOT NULL,
  `emailAdministrador` varchar(40) NOT NULL,
  `telefonoAdministracion` int(11) NOT NULL,
  `telefonoConsejeria` int(11) NOT NULL,
  `datosBancarios` varchar(300) NOT NULL,
  `cantidadPisos` int(11) NOT NULL,
  `direccionEdificio` varchar(40) NOT NULL,
  `numeroEdificio` int(11) NOT NULL,
  `ciudadEdificio` int(11) NOT NULL,
  `descripcionEdificio` varchar(300) NOT NULL,
  PRIMARY KEY (`idEdificio`),
  UNIQUE KEY `nombreEdificio` (`nombreEdificio`),
  KEY `edificiosFK1` (`ciudadEdificio`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `edificios`
--

INSERT INTO `edificios` (`idEdificio`, `nombreEdificio`, `nombreAdministrador`, `emailAdministrador`, `telefonoAdministracion`, `telefonoConsejeria`, `datosBancarios`, `cantidadPisos`, `direccionEdificio`, `numeroEdificio`, `ciudadEdificio`, `descripcionEdificio`) VALUES
(1, 'San Fracisco', 'Luis Valenzuela', 'lvalenzuela@aol.com', 4548477, 6566232, 'Banco CrediChile', 14, '1 sur', 660, 1, 'Cercano a la plaza de armas'),
(3, 'Alto del Monte', 'Jaime Santander', 'santander@monte.cl', 9885517, 6262474, 'Banco de Chile', 25, 'Domínicos', 1982, 2, 'Cercano a comercio'),
(4, 'Bosques del Sur', 'Patricio Córdoba', 'cordoba@bosquesdelsur.cl', 3425235, 12351246, 'Banco Riggs', 10, 'Alameda', 964, 3, 'Excelente ubicación, cercano a comercio y áreas verdes'),
(5, 'Parque Alameda', 'Óscar Acevedo', 'acevedoyacevedo@fiscalia.cl', 2141245, 5423413, 'Banco Banefe', 15, 'Colo-Colo', 2141, 4, 'Acojedor edificio con seguridad, cercano a plaza de armas'),
(6, 'Desembarco del Rey', 'Robb Stark', 'winteriscoming@got.com', 32141241, 2147483647, 'Banco de Hierro', 12, 'Av. Invernalia', 14124, 3, 'Cercano al muro.'),
(7, 'Arboleda Paraíso', 'Domingo Barnechea', 'barnecheadomingo@gmail.com', 452356, 893512, 'Banco Itaú', 30, 'Av. Kennedy', 235, 2, 'Sector residencial.'),
(8, 'Héroes II', 'Ernesto Soto', 'esoto@outlook.com', 784512, 986532, 'Banco Santander Banefe', 21, 'Av. Huérfanos', 597, 2, 'Cercano al metro.'),
(11, 'San Francisco III', 'Alberto Maturana', 'maturana@gmail.com', 234423432, 2147483647, '3243423423', 14, 'Cualquiera', 660, 1, 'Ninguna');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

DROP TABLE IF EXISTS `empresas`;
CREATE TABLE IF NOT EXISTS `empresas` (
  `idEmpresa` int(11) NOT NULL AUTO_INCREMENT,
  `nombreEmpresa` varchar(100) NOT NULL,
  PRIMARY KEY (`idEmpresa`),
  UNIQUE KEY `nombreEmpresa` (`nombreEmpresa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `empresas`
--

INSERT INTO `empresas` (`idEmpresa`, `nombreEmpresa`) VALUES
(3, 'American Airlines'),
(1, 'LAN'),
(2, 'PAM');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

DROP TABLE IF EXISTS `estados`;
CREATE TABLE IF NOT EXISTS `estados` (
  `idEstado` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`idEstado`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`idEstado`, `nombre`, `descripcion`) VALUES
(0, 'Prematura', NULL),
(1, 'En curso', NULL),
(2, 'Vencida', NULL),
(3, 'Cancelada', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotografiascliente`
--

DROP TABLE IF EXISTS `fotografiascliente`;
CREATE TABLE IF NOT EXISTS `fotografiascliente` (
  `CIF` int(11) NOT NULL,
  `numFotografia` int(11) NOT NULL,
  `nombreArchivo` varchar(100) NOT NULL,
  PRIMARY KEY (`CIF`,`numFotografia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fotografiascliente`
--

INSERT INTO `fotografiascliente` (`CIF`, `numFotografia`, `nombreArchivo`) VALUES
(324141, 0, '324141_0.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotografiasdepartamento`
--

DROP TABLE IF EXISTS `fotografiasdepartamento`;
CREATE TABLE IF NOT EXISTS `fotografiasdepartamento` (
  `idEdificio` int(11) NOT NULL,
  `numDpto` int(11) NOT NULL,
  `numFotografia` int(11) NOT NULL,
  `nombreArchivo` varchar(100) NOT NULL,
  PRIMARY KEY (`idEdificio`,`numDpto`,`numFotografia`),
  UNIQUE KEY `nombreArchivo` (`nombreArchivo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fotografiasdepartamento`
--

INSERT INTO `fotografiasdepartamento` (`idEdificio`, `numDpto`, `numFotografia`, `nombreArchivo`) VALUES
(11, 555, 0, '11_555_0.jpg'),
(7, 100, 0, '7_100_0.jpg'),
(7, 100, 1, '7_100_1.jpg'),
(8, 318, 0, '8_318_0.jpg'),
(8, 318, 1, '8_318_1.jpg'),
(8, 322, 0, '8_322_0.jpg'),
(8, 322, 1, '8_322_1.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fotografiasreservas`
--

DROP TABLE IF EXISTS `fotografiasreservas`;
CREATE TABLE IF NOT EXISTS `fotografiasreservas` (
  `idReserva` int(11) NOT NULL,
  `numFotografia` int(11) NOT NULL,
  `nombreArchivo` varchar(100) NOT NULL,
  PRIMARY KEY (`idReserva`,`numFotografia`),
  UNIQUE KEY `nombreArchivo` (`nombreArchivo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fotografiasreservas`
--

INSERT INTO `fotografiasreservas` (`idReserva`, `numFotografia`, `nombreArchivo`) VALUES
(4, 0, '4_0.jpg'),
(4, 1, '4_1.jpg'),
(4, 2, '4_2.jpg'),
(5, 0, '5_0.jpg'),
(5, 1, '5_1.jpg'),
(5, 2, '5_2.jpg'),
(5, 3, '5_3.jpg'),
(5, 4, '5_4.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historicoindicadores`
--

DROP TABLE IF EXISTS `historicoindicadores`;
CREATE TABLE IF NOT EXISTS `historicoindicadores` (
  `idIndicador` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `valor` int(11) NOT NULL,
  PRIMARY KEY (`idIndicador`,`fecha`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `indicadores`
--

DROP TABLE IF EXISTS `indicadores`;
CREATE TABLE IF NOT EXISTS `indicadores` (
  `idIndicador` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `ultimoPrecio` int(11) NOT NULL,
  PRIMARY KEY (`idIndicador`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingresos`
--

DROP TABLE IF EXISTS `ingresos`;
CREATE TABLE IF NOT EXISTS `ingresos` (
  `sessionId` varchar(100) NOT NULL,
  `fecha` datetime NOT NULL,
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`sessionId`,`fecha`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ingresos`
--

INSERT INTO `ingresos` (`sessionId`, `fecha`, `ip`) VALUES
('045765c35927453e2d9ca1682008bdf9', '2014-12-04 12:18:36', '127.0.0.1'),
('04c49925016cd05d8e86b269173d65ef', '2014-12-03 22:27:08', '190.20.120.255'),
('12d88d689b3fb230e58dbf96ba3c685a', '2014-11-30 23:41:03', '127.0.0.1'),
('138be95a0de676002bd5a6e6a284ced8', '2014-12-04 03:48:29', '127.0.0.1'),
('179650910fe2922cb6832c944df1dce6', '2014-12-04 01:39:05', '127.0.0.1'),
('1e5a09b76572a1bb22c7d99b8c9b25d0', '2014-12-03 11:33:27', '127.0.0.1'),
('1ff7e98f21f02c4aae5418699bd65611', '2014-11-25 03:32:04', '127.0.0.1'),
('215149ddf2d5d506daf22a7a3ac03133', '2014-11-24 17:23:23', '127.0.0.1'),
('2fb318f8126512b142bb02d73a50691b', '2014-11-26 12:09:47', '127.0.0.1'),
('354aa44d196af5a02df0f866ab164da4', '2014-12-03 15:01:24', '192.168.2.4'),
('4adbbdf95d9a37bd13fd44d372d63653', '2014-12-04 06:44:00', '190.20.106.163'),
('4d909de276c25ba49da46de014527692', '2014-11-25 09:31:51', '127.0.0.1'),
('500d6f54e9574fcff48a09e4c68d500f', '2014-11-30 23:52:31', '127.0.0.1'),
('532c9a9c8de94a4c1601ddd0667e4107', '2014-11-24 12:51:17', '127.0.0.1'),
('536f5e6cca1b0ab9c8024607f0a1178f', '2014-12-03 21:37:38', '127.0.0.1'),
('60617e996eaf39e553d502e0c8014880', '2014-11-30 12:48:15', '127.0.0.1'),
('635a9fdd84334591be3ebf6024dea919', '2014-11-23 18:13:57', '127.0.0.1'),
('69877d44feb449ee44ba526dceb3e321', '2014-11-30 01:48:51', '127.0.0.1'),
('6c53f22e92ebf732015906d2c37ecf14', '2014-11-25 20:01:08', '127.0.0.1'),
('6e58e8c7478fccbebddec6c3cab41db2', '2014-12-03 18:35:07', '192.168.2.4'),
('701f0a202cd2f8b294152c2e16c36583', '2014-12-01 23:14:06', '127.0.0.1'),
('7301e4b817daeedcef49a41ff9c647f1', '2014-12-03 19:10:43', '127.0.0.1'),
('76bb87a09818354490fe30feae726bb3', '2014-12-01 02:24:00', '127.0.0.1'),
('78d0ba59810ccacbbf56612478f5198f', '2014-11-24 19:23:54', '127.0.0.1'),
('7f37715dc26d26548c22789a970e9e57', '2014-11-23 15:15:52', '127.0.0.1'),
('7f37715dc26d26548c22789a970e9e57', '2014-11-23 15:16:01', '127.0.0.1'),
('7f37715dc26d26548c22789a970e9e57', '2014-11-23 15:16:11', '127.0.0.1'),
('83d14d285c0a97747dc3483113694b37', '2014-11-28 23:56:23', '127.0.0.1'),
('83e50fad4cd836804a12e586b3f11ef8', '2014-11-24 21:24:06', '127.0.0.1'),
('95300987bba76202f80b1e7fd32a76d3', '2014-11-30 20:53:52', '127.0.0.1'),
('98be9dea35912d61e3dc6697a5195893', '2014-11-24 15:00:02', '127.0.0.1'),
('9d0385330a88518e0a4d2d01221cd7e8', '2014-12-03 23:37:47', '127.0.0.1'),
('a04cf886b7987fad591b6ccf55a3224d', '2014-11-23 22:06:13', '127.0.0.1'),
('aba9a882608b783612329e68cc8a44ea', '2014-12-04 10:51:57', '190.20.106.163'),
('b25aeba93f6fc7ddaa15c337a135ab75', '2014-12-04 08:47:00', '127.0.0.1'),
('b60ec0ef274737c75474a2ef69090a28', '2014-12-04 01:32:52', '190.20.106.163'),
('b9520b427fb680a03adc57b946aff3b4', '2014-12-01 16:43:13', '127.0.0.1'),
('c3d0eb56580bd4fb899f30cccbcca113', '2014-12-04 07:07:42', '190.20.106.163'),
('ca3eb35d6f64b0dcc6d3bf49e2215214', '2014-11-24 00:17:06', '127.0.0.1'),
('cb75bc363bed91fe62227d75455fd8ca', '2014-11-29 23:19:20', '127.0.0.1'),
('cc1c6e0797ef8eb494329d67f501f2df', '2014-12-04 10:49:14', '127.0.0.1'),
('cc33a10e426f981f299f39096bb77065', '2014-12-02 18:44:48', '192.168.1.101'),
('cdda60afdce47c934758edaeed04411f', '2014-11-30 16:36:18', '127.0.0.1'),
('d3dd9cbeda8d3eb26039465fe38fb64f', '2014-12-04 07:05:25', '190.20.106.163'),
('d6473fe943e0a117feabe30f8a339d2b', '2014-11-24 23:25:49', '127.0.0.1'),
('de0751096f0f6791fd7ba53eeb965d08', '2014-12-02 01:15:12', '127.0.0.1'),
('df7de1f15724c70bf6c082b1e2411eba', '2014-12-02 18:22:11', '127.0.0.1'),
('dfe1941caffa0801e8924a54a911c1c0', '2014-11-30 18:53:39', '127.0.0.1'),
('e2ee29571c6f31c536d4b0f88ccc82f3', '2014-12-04 01:24:54', '190.20.106.163'),
('e44c17d3de5b9972924255afb0c53b1a', '2014-11-24 10:49:34', '127.0.0.1'),
('e52cb61d0e05a55bcbf30df630e744de', '2014-12-03 15:01:43', '127.0.0.1'),
('f20f1bcb4b42232653e5899c7bea64a4', '2014-12-03 17:05:43', '127.0.0.1'),
('f41d75dc58f64e3756fb05a7b186bc47', '2014-12-01 21:13:38', '127.0.0.1'),
('f98b7099777e95812cda391a31e43d87', '2014-12-04 07:05:40', '190.20.106.163'),
('f9cf1df25a3e250e3d186043d4b79288', '2014-12-01 09:59:17', '127.0.0.1'),
('fb208b03143770e64829ac0738868746', '2014-11-25 01:30:24', '127.0.0.1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ocupaciones`
--

DROP TABLE IF EXISTS `ocupaciones`;
CREATE TABLE IF NOT EXISTS `ocupaciones` (
  `idReserva` int(11) NOT NULL,
  `idEdificio` int(11) NOT NULL,
  `numDpto` int(11) NOT NULL,
  `fechaLlegada` datetime NOT NULL,
  `fechaPartida` datetime NOT NULL,
  `capacidad` int(11) NOT NULL,
  `precioTotal` int(11) NOT NULL,
  PRIMARY KEY (`idReserva`,`idEdificio`,`numDpto`,`fechaLlegada`),
  KEY `OcupacionesFK1` (`idEdificio`,`numDpto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ocupaciones`
--

INSERT INTO `ocupaciones` (`idReserva`, `idEdificio`, `numDpto`, `fechaLlegada`, `fechaPartida`, `capacidad`, `precioTotal`) VALUES
(4, 1, 305, '2014-12-04 09:15:00', '2014-12-10 09:15:00', 1, 60000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

DROP TABLE IF EXISTS `paises`;
CREATE TABLE IF NOT EXISTS `paises` (
  `idPais` int(11) NOT NULL AUTO_INCREMENT,
  `nombrePais` varchar(40) NOT NULL,
  `gentilicioPais` varchar(40) NOT NULL,
  PRIMARY KEY (`idPais`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=112 ;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`idPais`, `nombrePais`, `gentilicioPais`) VALUES
(1, 'Angola', 'Angoleña'),
(2, 'Argelia', 'Argelina'),
(3, 'Camerún', 'Camerunesa'),
(4, 'Etiopía', 'Etíope'),
(5, 'Guinea Ecuatorial', 'Ecuatoguineana'),
(6, 'Egipto', 'Egipcia'),
(7, 'Liberia', 'Liberiana'),
(8, 'Libya', 'Libia'),
(9, 'Marruecos', 'Marroquí'),
(10, 'Namibia', 'Namibia'),
(11, 'Nigeria', 'Nigeriana'),
(12, 'República Árabe Saharaui Democrática', 'Saharaui'),
(13, 'Senegal', 'Senegalesa'),
(14, 'Sudáfrica', 'Sudafricana'),
(15, 'Togo', 'Togolesa'),
(16, 'Canadá', 'Canadiense'),
(17, 'Estados Unidos', 'Estadounidense'),
(18, 'México', 'Mexicana'),
(19, 'Belice', 'Beliceña'),
(20, 'Costa Rica', 'Costarricense'),
(21, 'Guatemala', 'Guatemalteca'),
(22, 'Honduras', 'Hondureña'),
(23, 'Nicaragua', 'Nicaragüense'),
(24, 'Panamá', 'Panameña'),
(25, 'El Salvador', 'Salvadoreña'),
(26, 'Cuba', 'Cubana'),
(27, 'Aruba', 'Arubana'),
(28, 'Bahamas', 'Bahameña'),
(29, 'Barbados', 'Barbadense'),
(30, 'Dominica', 'Dominiquesa'),
(31, 'República Dominicana', 'Dominicana'),
(32, 'Haiti', 'Haitiana'),
(33, 'Jamaica', 'Jamaiquina'),
(34, 'Puerto Rico', 'Puertorriqueña'),
(35, 'Saint Kitts and Nevis', 'Sancristobaleña'),
(36, 'Saint Lucia', 'Santaluciana'),
(37, 'Saint Vincent and the Grenadines', 'Sanvicentina'),
(38, 'Argentina', 'Argentina'),
(39, 'Bolivia', 'Boliviana'),
(40, 'Brasil', 'Brasileña'),
(41, 'Chile', 'Chilena'),
(42, 'Colombia', 'Colombiana'),
(43, 'Ecuador', 'Ecuatoriana'),
(44, 'Guyana', 'Guyanés'),
(45, 'Paraguay', 'Paraguaya'),
(46, 'Perú', 'Peruana'),
(47, 'Surinam', 'Surinamés'),
(48, 'Uruguay', 'Uruguaya'),
(49, 'Venezuela', 'Venezolana'),
(50, 'Afganistán', 'Afgana'),
(51, 'Azerbaiyán', 'Azerbaiyana'),
(52, 'Bangladés', 'Bangladesí'),
(53, 'Baréin', 'Bareiní'),
(54, 'República Popular China', 'China'),
(55, 'Filipinas', 'Filipina'),
(56, 'Georgia', 'Georgiana'),
(57, 'India', 'Hindú'),
(58, 'Indonesia', 'Indonesia'),
(59, 'Israel', 'Israelí'),
(60, 'Japón', 'Japonesa'),
(61, 'Líbano', 'Libanesa'),
(62, 'Mongolia', 'Mongola'),
(63, 'Corea del Norte', 'Norcoreana'),
(64, 'Siria', 'Siria'),
(65, 'Corea del Sur', 'Surcoreana'),
(66, 'Vietnam', 'Vietnamita'),
(67, 'Albania', 'Albanesa'),
(68, 'Alemania', 'Alemana'),
(69, 'Andorra', 'Andorrana'),
(70, 'Armenia', 'Armenia'),
(71, 'Austria', 'Austríaca'),
(72, 'Bélgica', 'Belga'),
(73, 'Bielorrusia', 'Bielorrusa'),
(74, 'Bosnia y Herzegovina', 'Bosnia'),
(75, 'Bulgaria', 'Búlgara'),
(76, 'República Checa', 'Checa'),
(77, 'Chipre', 'Chipriota'),
(78, 'Croacia', 'Croata'),
(79, 'Dinamarca', 'Danesa'),
(80, 'Escocia', 'Escocesa'),
(81, 'Slovakia', 'Eslovaca'),
(82, 'Slovenia', 'Eslovena'),
(83, 'España', 'Española'),
(84, 'Estonia', 'Estonia'),
(85, 'Finlandia', 'Finlandesa'),
(86, 'Francia', 'Francesa'),
(87, 'Grecia', 'Griega'),
(88, 'Hungría', 'Húngara'),
(89, 'Reino Unido', 'Británica'),
(90, 'Irlanda', 'Irlandesa'),
(91, 'Italia', 'Italiana'),
(92, 'Letonia', 'Letona'),
(93, 'Lituania', 'Lituana'),
(94, 'Luxemburgo', 'Luxemburguesa'),
(95, 'Malta', 'Maltesa'),
(96, 'Moldavia', 'Moldava'),
(97, 'Monaco', 'Monegasca'),
(98, 'Montenegro', 'Montenegrina'),
(99, 'Noruega', 'Noruega'),
(100, 'Los Países Bajos', 'Neerlandesa'),
(101, 'Polonia', 'Polaca'),
(102, 'Portugal', 'Portuguesa'),
(103, 'Rumania', 'Rumana'),
(104, 'Rusia', 'Rusa'),
(105, 'Serbia', 'Serbia'),
(106, 'Suecia', 'Sueca'),
(107, 'Suiza', 'Suiza'),
(108, 'Turquía', 'Turca'),
(109, 'Ucrania', 'Ucraniana'),
(110, 'Australia', 'Australiana'),
(111, 'Nueva Zelanda', 'Neozelandesa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precios`
--

DROP TABLE IF EXISTS `precios`;
CREATE TABLE IF NOT EXISTS `precios` (
  `idEdificio` int(11) NOT NULL,
  `numDpto` int(11) NOT NULL,
  `cantidadPersonas` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `precio` int(11) NOT NULL,
  PRIMARY KEY (`idEdificio`,`numDpto`,`cantidadPersonas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `precios`
--

INSERT INTO `precios` (`idEdificio`, `numDpto`, `cantidadPersonas`, `fecha`, `precio`) VALUES
(1, 305, 1, '2014-11-30', 10000),
(1, 305, 2, '2014-11-30', 20000),
(1, 305, 3, '2014-11-30', 30000),
(3, 333, 1, '2014-12-03', 0),
(3, 333, 2, '2014-12-03', 0),
(3, 333, 3, '2014-12-03', 0),
(3, 333, 4, '2014-12-03', 0),
(3, 333, 5, '2014-12-03', 0),
(5, 801, 1, '2014-12-03', 0),
(5, 801, 2, '2014-12-03', 0),
(5, 801, 3, '2014-12-03', 0),
(5, 801, 4, '2014-12-03', 0),
(6, 12, 1, '2014-12-03', 0),
(6, 12, 2, '2014-12-03', 0),
(6, 12, 3, '2014-12-03', 0),
(6, 12, 4, '2014-12-03', 0),
(6, 12, 5, '2014-12-03', 0),
(6, 12, 6, '2014-12-03', 0),
(6, 12, 7, '2014-12-03', 0),
(6, 12, 8, '2014-12-03', 0),
(6, 12, 9, '2014-12-03', 0),
(6, 12, 10, '2014-12-03', 0),
(6, 444, 1, '2014-12-03', 0),
(6, 444, 2, '2014-12-03', 0),
(6, 901, 1, '0000-00-00', 0),
(6, 901, 2, '0000-00-00', 0),
(6, 901, 3, '0000-00-00', 0),
(6, 901, 4, '0000-00-00', 0),
(7, 100, 1, '2014-12-03', 0),
(7, 100, 2, '2014-12-03', 0),
(7, 100, 3, '2014-12-03', 0),
(7, 100, 4, '2014-12-03', 0),
(7, 100, 5, '2014-12-03', 0),
(7, 100, 6, '2014-12-03', 0),
(8, 123, 1, '2014-12-03', 0),
(8, 318, 1, '2014-12-03', 0),
(8, 318, 2, '2014-12-03', 0),
(8, 322, 1, '2014-12-03', 0),
(8, 322, 2, '2014-12-03', 0),
(11, 555, 1, '2014-12-03', 0),
(11, 555, 2, '2014-12-03', 0),
(11, 555, 3, '2014-12-03', 0),
(11, 999, 1, '2014-12-03', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservas`
--

DROP TABLE IF EXISTS `reservas`;
CREATE TABLE IF NOT EXISTS `reservas` (
  `idReserva` int(11) NOT NULL AUTO_INCREMENT,
  `idCliente` int(11) NOT NULL,
  `idTarjeta` int(11) NOT NULL,
  `codigoCVC` int(11) NOT NULL,
  `mesExpiracion` int(11) NOT NULL,
  `anioExpiracion` int(11) NOT NULL,
  `motivoViaje` varchar(20) NOT NULL,
  `estacInicio` date NOT NULL,
  `estacFin` date NOT NULL,
  `modoLlegada` varchar(20) NOT NULL,
  `fechaLlegada` date NOT NULL,
  `horaLlegada` time NOT NULL,
  `peticionAdicional` varchar(1000) DEFAULT NULL,
  `cantidadPersonas` int(11) NOT NULL,
  `numeroVuelo` int(11) DEFAULT NULL,
  `idEmpresa` int(11) DEFAULT NULL,
  `cantidadAcompaniantes` int(11) NOT NULL,
  `valorReserva` int(11) NOT NULL,
  `descuento` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  PRIMARY KEY (`idReserva`),
  KEY `ReservaFK1` (`idCliente`),
  KEY `ReservaFK2` (`idEmpresa`),
  KEY `ReservaFK3` (`estado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `reservas`
--

INSERT INTO `reservas` (`idReserva`, `idCliente`, `idTarjeta`, `codigoCVC`, `mesExpiracion`, `anioExpiracion`, `motivoViaje`, `estacInicio`, `estacFin`, `modoLlegada`, `fechaLlegada`, `horaLlegada`, `peticionAdicional`, `cantidadPersonas`, `numeroVuelo`, `idEmpresa`, `cantidadAcompaniantes`, `valorReserva`, `descuento`, `estado`, `fechaCreacion`) VALUES
(1, 0, 1, 4545, 5, 2018, 'Negocios', '1969-12-31', '1969-12-31', 'Avion', '1969-12-31', '02:30:00', 'Almuerzo al llegar', 10, 3321, 3, 10, 200000, 0, 0, '2014-12-04 02:38:27'),
(4, 8974254, 3, 4545, 5, 2018, 'Negocios', '1969-12-31', '1969-12-31', 'Avion', '1969-12-31', '09:30:00', 'Almuerzo al llegar', 2, 3321, 3, 2, 60000, 0, 0, '2014-12-04 09:26:40'),
(5, 213123123, 1, 4545, 5, 2018, 'Negocios', '1969-12-31', '1969-12-31', 'Avion', '1969-12-31', '10:30:00', 'Almuerzo al llegar', 5, 3321, 3, 5, 0, 0, 0, '2014-12-04 10:45:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarjetascredito`
--

DROP TABLE IF EXISTS `tarjetascredito`;
CREATE TABLE IF NOT EXISTS `tarjetascredito` (
  `idTarjeta` int(11) NOT NULL AUTO_INCREMENT,
  `nombreTarjeta` varchar(100) NOT NULL,
  PRIMARY KEY (`idTarjeta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `tarjetascredito`
--

INSERT INTO `tarjetascredito` (`idTarjeta`, `nombreTarjeta`) VALUES
(1, 'MasterCard'),
(2, 'VISA'),
(3, 'Dinners Club'),
(4, 'American Express');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videosdepartamento`
--

DROP TABLE IF EXISTS `videosdepartamento`;
CREATE TABLE IF NOT EXISTS `videosdepartamento` (
  `idEdificio` int(11) NOT NULL,
  `numDpto` int(11) NOT NULL,
  `numVideo` int(11) NOT NULL,
  `nombreArchivo` varchar(100) NOT NULL,
  PRIMARY KEY (`idEdificio`,`numDpto`,`numVideo`),
  UNIQUE KEY `nombreArchivo` (`nombreArchivo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `clientesFK1` FOREIGN KEY (`nacionalidad`) REFERENCES `paises` (`idPais`),
  ADD CONSTRAINT `clientesFK2` FOREIGN KEY (`contacto`) REFERENCES `contactos` (`idContacto`);

--
-- Filtros para la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD CONSTRAINT `departamentosFK1` FOREIGN KEY (`idEdificio`) REFERENCES `edificios` (`idEdificio`);

--
-- Filtros para la tabla `edificios`
--
ALTER TABLE `edificios`
  ADD CONSTRAINT `edificiosFK1` FOREIGN KEY (`ciudadEdificio`) REFERENCES `ciudades` (`id`);

--
-- Filtros para la tabla `fotografiascliente`
--
ALTER TABLE `fotografiascliente`
  ADD CONSTRAINT `FotografiasClienteFK` FOREIGN KEY (`CIF`) REFERENCES `clientes` (`CIF`);

--
-- Filtros para la tabla `fotografiasdepartamento`
--
ALTER TABLE `fotografiasdepartamento`
  ADD CONSTRAINT `FotografiasDepartamentoFK` FOREIGN KEY (`idEdificio`, `numDpto`) REFERENCES `departamentos` (`idEdificio`, `numDpto`);

--
-- Filtros para la tabla `fotografiasreservas`
--
ALTER TABLE `fotografiasreservas`
  ADD CONSTRAINT `FotografiasReservasFK` FOREIGN KEY (`idReserva`) REFERENCES `reservas` (`idReserva`);

--
-- Filtros para la tabla `historicoindicadores`
--
ALTER TABLE `historicoindicadores`
  ADD CONSTRAINT `HistoricoIndicadoresFK` FOREIGN KEY (`idIndicador`) REFERENCES `indicadores` (`idIndicador`);

--
-- Filtros para la tabla `ocupaciones`
--
ALTER TABLE `ocupaciones`
  ADD CONSTRAINT `OcupacionesFK1` FOREIGN KEY (`idEdificio`, `numDpto`) REFERENCES `departamentos` (`idEdificio`, `numDpto`),
  ADD CONSTRAINT `OcupacionesFK2` FOREIGN KEY (`idReserva`) REFERENCES `reservas` (`idReserva`);

--
-- Filtros para la tabla `precios`
--
ALTER TABLE `precios`
  ADD CONSTRAINT `preciosFK1` FOREIGN KEY (`idEdificio`, `numDpto`) REFERENCES `departamentos` (`idEdificio`, `numDpto`);

--
-- Filtros para la tabla `reservas`
--
ALTER TABLE `reservas`
  ADD CONSTRAINT `ReservaFK1` FOREIGN KEY (`idCliente`) REFERENCES `clientes` (`CIF`),
  ADD CONSTRAINT `ReservaFK2` FOREIGN KEY (`idEmpresa`) REFERENCES `empresas` (`idEmpresa`),
  ADD CONSTRAINT `ReservaFK3` FOREIGN KEY (`estado`) REFERENCES `estados` (`idEstado`);

--
-- Filtros para la tabla `videosdepartamento`
--
ALTER TABLE `videosdepartamento`
  ADD CONSTRAINT `VideosDepartamentoFK` FOREIGN KEY (`idEdificio`, `numDpto`) REFERENCES `departamentos` (`idEdificio`, `numDpto`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
