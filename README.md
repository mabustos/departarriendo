#Departarriendo

Departarriendo is a web application that will help you to manage your rental properties by using a simple yet powerful interface.

##Features

- Manage your rental properties.
- Manage your client data.
- Book your properties and keep track of their occupancy.
- Generate reports.
- and much more!

##How to use?

- Place departarriendo contents in an PHP server.
- Included SQL dump file will create the database and will populate it with tables and mock data.
- Default access credentials:
      - username: admin
      - password: admin