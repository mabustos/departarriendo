<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_nacionalidades extends CI_Model {
    public function listarNacionalidades(){
        return $this->db->get('Paises');
    }
}