<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_tarjetas_credito extends CI_Model {
    public function listarTarjetasCredito(){
        return $this->db->get('TarjetasCredito');
    }   
    public function seleccionarTarjetasCredito($id){
        return $this->db->get_where('TarjetasCredito', array('idTarjeta' => $id));;
    }  
}