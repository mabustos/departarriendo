<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_clientes extends CI_Model {
  public function ingresarCliente($data){
      if($data['CIF']!=0){
            if($this->db->insert('Clientes', $data)){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
  }

  public function eliminarCliente($CIF){
        $this->db->where('idCliente', $CIF);
        $this->db->where('idCliente !=', 0);
        $this->db->update('Reservas', array('idCliente'=>0));

        $this->db->delete('Clientes', array('CIF' => $CIF));
        if($this->db->affected_rows()>0){
            return true;
        }
        else{
            return false;
        }
    }

  public function editarCliente($CIF,$data){
        $this->db->where('CIF !=', 0);
        $this->db->where('CIF', $CIF);
        return $this->db->update('Clientes', $data);
  }

    public function editarFotografiasCliente($CIFOld,$CIF){
        $this->db->where('CIF', $CIFOld);
        $data = array(
               'CIF' => $CIF
            );
        return $this->db->update('FotografiasCliente', $data);
    }

  public function listarClientes(){
      return $this->db->get('Clientes');
  }

  public function seleccionarCliente($CIF){
      return $this->db->get_where('Clientes', array('CIF' => $CIF));
  }

  public function seleccionarTratamiento($idTrat){
      switch($idTrat){
          case 0: return "Sr."; break;
          case 1: return "Sra."; break;
          case 2: return "Srta."; break;
      }
  }

  public function seleccionarNacionalidad($nac){
      return $this->db->get_where('Paises', array('idPais' => $nac));;
  }


  public function ingresarImagenes($imagenes){
      if($this->db->insert_batch('FotografiasCliente',$imagenes)){
          return true;
      }else{
          return false;
      }
  }



  public function seleccionarFotografiasCliente($CIF){
       return $this->db->get_where('FotografiasCliente', array('CIF' => $CIF));
  }

    public function eliminarFotografiasCliente($CIF,$fotografias){
        if($fotografias==null){
            $fotografias = $this->db->get_where('FotografiasCliente', array('CIF' => $CIF));
            $fotografias = $fotografias->result();
            if(is_null($fotografias)){
                return array();
            }
            else{
                $this->db->delete('FotografiasCliente', array(
                    'CIF' => ($CIF)
                ));
                return $fotografias;
            }


        }else{
            foreach($fotografias as $fotografia){
                $this->db->delete('FotografiasCliente', array(
                    'CIF' => $CIF,
                    'numFotografia' => ($fotografia->numFotografia)
                    )
                );
            }
        }
        return true;
    }
}
