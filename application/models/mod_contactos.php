<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_contactos extends CI_Model {
    public function listarContactos(){
        return $this->db->get('Contactos');
    }   
}