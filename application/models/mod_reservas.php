<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_reservas extends CI_Model {
    public function seleccionarDepartamentosFecha($fechaInicio,$fechaTermino){
        $sql = "SELECT d1.*
FROM Departamentos d1
WhERE d1.idEdificio NOT IN (
    					 SELECT o1.idEdificio
    					 FROM Ocupaciones o1
    					 WHERE date(?) >= o1.fechaLlegada
                         AND date(?) <= o1.fechaPartida

                         OR
                         (date(?) <= o1.fechaLlegada
                         AND date(?) >= o1.fechaPartida)

                         OR
                         (date(?) >= o1.fechaLlegada
                         AND date(?) >= o1.fechaPartida)

                         OR
                         (date(?) <= o1.fechaLlegada
                         AND date(?) <= o1.fechaPartida)

    					)
      AND
      d1.numDpto NOT IN (
    					 SELECT o1.numDpto
    					 FROM Ocupaciones o1
          				 WHERE date(?) >= o1.fechaLlegada
                         AND date(?) <= o1.fechaPartida

                         OR
                         date(?) <= o1.fechaLlegada
                         AND date(?) >= o1.fechaPartida

                         OR
                         (date(?) >= o1.fechaLlegada
                         AND date(?) >= o1.fechaPartida)

                         OR
                         (date(?) <= o1.fechaLlegada
                         AND date(?) <= o1.fechaPartida)

    					)";
        return $this->db->query($sql, array($fechaInicio,$fechaTermino, $fechaInicio, $fechaTermino, $fechaInicio, $fechaTermino, $fechaInicio, $fechaTermino, $fechaInicio, $fechaTermino, $fechaInicio, $fechaTermino, $fechaInicio, $fechaTermino, $fechaInicio, $fechaTermino));
    }

    public function seleccionarOcupacionesDepartamento($idEdificio,$numDpto){
        $sql = "SELECT o1.*
                FROM Ocupaciones o1
                WHERE o1.idEdificio=? AND o1.numDpto=?";
        return $this->db->query($sql, array($idEdificio,$numDpto));
    }

    public function listarOcupacionesEdificio($idEdificio){
        $sql = "SELECT o1.*
                FROM Ocupaciones o1
                WHERE o1.idEdificio=?";
        return $this->db->query($sql, array($idEdificio));
    }

    public function eliminarOcupacion($idEdificio,$numDpto,$fechaInicio,$fechaTermino){
        $this->db->delete('Ocupaciones', array(
            'idEdificio' => $idEdificio,
            'numDpto' => $numDpto,
            'fechaLlegada' => $fechaInicio,
            'fechaPartida' => $fechaTermino
            )
        );
        return ($this->db->affected_rows() > 0)?true:false;
    }

    public function listarReservasCliente($CIF){
        $sql = "SELECT o1.*
                FROM Reservas o1
                WHERE o1.idCliente=?";
        return $this->db->query($sql, array($CIF));
    }

    public function listarEmpresasVuelo(){
        return $this->db->get('Empresas');
    }

    public function listarReservas(){
        return $this->db->get('Reservas');
    }

    public function listarOcupaciones(){
        return $this->db->get('Ocupaciones');
    }

    public function editarReserva($idReserva,$data){
        $this->db->where('idReserva', $idReserva);
        return $this->db->update('Reservas', $data);
    }

    public function editarClienteReserva($actualCIF,$nuevoCIF){
        $data = array('idCliente' => $nuevoCIF);
        $this->db->where('idCliente', $actualCIF);
        return $this->db->update('Reservas', $data);
    }

    public function seleccionarIdMaxima(){
        $this->db->select_max('idReserva');
        return $this->db->get('Reservas');
    }

    public function seleccionarReserva($idReserva){
         return $this->db->get_where('Reservas', array('idReserva' => $idReserva));
    }

    public function seleccionarOcupacionesReserva($idReserva){
         return $this->db->get_where('Ocupaciones', array('idReserva' => $idReserva));
    }

    public function seleccionarFotografiasReserva($idReserva){
        return $this->db->get_where('FotografiasReservas', array('idReserva' => $idReserva));
    }

    public function listarEstados(){
        return $this->db->get('Estados');
    }

    public function listarOcupacionesDepartamentoFecha($idEdificio,$numDpto,$fechaInicio,$fechaTermino){
        $sql = "SELECT o1.*
                FROM Ocupaciones o1
                WHERE o1.fechaLlegada >= date(?)
                AND o1.fechaPartida <= date(?)
                AND o1.idEdificio = ?
                AND o1.numDpto = ?
                ";
        return $this->db->query($sql, array($fechaInicio,$fechaTermino, $idEdificio, $numDpto));
    }

    public function editarDepartamentoReserva($actualIdEdificio,$actualNumDpto,$nuevoIdEdificio,$nuevoNumDpto){
        $data = array('idEdificio' => $nuevoIdEdificio,
                      'numDpto'    => $nuevoNumDpto
        );
        $this->db->where('idEdificio', $actualIdEdificio);
        $this->db->where('numDpto', $actualNumDpto);
        return $this->db->update('Ocupaciones', $data);
    }

    public function editarOcupacion($idReserva,$idEdificio,$numDpto,$fechaInicioNew,$fechaTerminoNew,$fechaInicioOld,$fechaTerminoOld,$capacidad,$precioTotal){
        $data = array(
            'fechaLlegada' => $fechaInicioNew,
            'fechaPartida' => $fechaTerminoNew,
            'capacidad' => $capacidad,
            'precioTotal' => $precioTotal
        );
        $this->db->where('idReserva', $idReserva);
        $this->db->where('idEdificio', $idEdificio);
        $this->db->where('numDpto', $numDpto);
        $this->db->where('fechaLlegada', $fechaInicioOld);
        $this->db->where('fechaPartida', $fechaTerminoOld);
        return $this->db->update('Ocupaciones', $data);
    }

    public function ingresarReserva($data){
        if($this->db->insert('Reservas',$data)){
            return true;
        }
        else{
            return false;
        }
    }

    public function ingresarImagenes($imagenes){
        if($this->db->insert_batch('FotografiasReservas',$imagenes)){
            return true;
        }else{
            return false;
        }
    }

    public function ingresarOcupaciones($ocupaciones){
        if($this->db->insert_batch('Ocupaciones',$ocupaciones)){
            return true;
        }else{
            return false;
        }
    }

    public function verificarDisponibilidadFecha($fechaInicio,$fechaTermino,$idEdificio,$numDpto){
        $sql = "
        SELECT d1.*
FROM Departamentos d1
WhERE d1.idEdificio IN (
      SELECT o1.idEdificio
      FROM Ocupaciones o1
      WHERE o1.fechaLlegada >= TIMESTAMP(?)
      AND o1.fechaPartida <= TIMESTAMP(?)
      )
AND d1.numDpto IN (
      SELECT o1.numDpto
      FROM Ocupaciones o1
      WHERE o1.fechaLlegada >= TIMESTAMP(?)
      AND o1.fechaPartida <= TIMESTAMP(?)
      )
AND d1.idEdificio = ?
AND d1.numDpto = ?
                        ";

        $query = $this->db->query($sql, array($fechaInicio,$fechaTermino, $fechaInicio, $fechaTermino,$idEdificio,$numDpto));
        foreach($query->result() as $row){
                return false;
        }
        return true;
    }

    public function eliminarFotografiasReserva($idReserva,$fotografias){
        if($fotografias==null){
            $fotografias = $this->db->get_where('FotografiasReservas', array('idReserva' => $idReserva));
            if(is_null($fotografias->result())){
                return array();
            }
            else{
                $this->db->delete('FotografiasReservas', array(
                    'idReserva' => ($idReserva)
                ));
                return $fotografias->result();
            }


        }else{
            foreach($fotografias as $fotografia){
                $this->db->delete('FotografiasReservas', array(
                    'idReserva' => $idReserva,
                    'numFotografia' => $fotografia->numFotografia
                    )
                );
            }
        }
        return true;
    }

    public function eliminarReserva($idReserva){
        $this->db->delete('Ocupaciones', array('idReserva' => $idReserva));
        $this->db->delete('Reservas', array('idReserva' => $idReserva));

        if($this->db->affected_rows()>0){
            return true;
        }
        else{
            return false;
        }
    }


}
