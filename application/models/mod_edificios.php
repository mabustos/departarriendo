<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_edificios extends CI_Model {
    public function ingresarEdificio($data){
        if($this->db->insert('Edificios', $data)){
            return true;
        }
        else{
            return false;
        }
    }

    public function listarEdificiosDepartamento(){
        $this->db->select('idEdificio,nombreEdificio,cantidadPisos,ciudadEdificio');
        return $this->db->get('Edificios');
    }

    public function listarEdificios(){
        return $this->db->get('Edificios');
    }

    public function eliminarEdificio($id){
        $this->db->delete('Ocupaciones', array('idEdificio' => $id));
        $this->db->delete('Precios', array('idEdificio' => $id));
        $this->db->delete('FotografiasDepartamento', array('idEdificio' => $id));
        $this->db->delete('Departamentos', array('idEdificio' => $id));
        $this->db->delete('Edificios', array('idEdificio' => $id));
        if($this->db->affected_rows()>0){
            return true;
        }
        else{
            return false;
        }
    }

    public function seleccionarEdificio($id){
        return $this->db->get_where('Edificios', array('idEdificio' => $id));
    }

    public function seleccionarEdificioCaracteristico($dataInput){
        return $this->db->get_where('Edificios',
            array(
                'nombreEdificio'=>$dataInput['nombreEdificio'],
                'ciudadEdificio'=>$dataInput['ciudadEdificio'],
                'numeroEdificio'=>$dataInput['numeroEdificio']
            )
        );
    }

    public function editarEdificio($id,$data){
        $this->db->where('idEdificio', $id);
        return $this->db->update('Edificios', $data);
    }


}
