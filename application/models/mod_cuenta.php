<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_cuenta extends CI_Model {
  public function comprobarContrasenia($contrasenia){
    $query = $this->db->get_where('Administrador', array('contrasenia' => $contrasenia));
    if($query->num_rows()>0){
      return true;
    }
    else{
      return false;
    }
  }
  public function modificarCuenta($data,$usuario){
    $query=$this->db->where('usuario',$usuario);
    if($this->db->update('Administrador',$data)){
      return true;
    }else{
      return false;
    }
  }
  
  
  public function obtenerCuenta(){
    $query = $this->db->get('Administrador');
    foreach ($query->result() as $row){
        return $row->usuario;
    }
  }
}