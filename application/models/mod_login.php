<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_login extends CI_Model {

	public function selectUser($user)
	{
      $this->db->select('contrasenia'); //se tiene que llamar como en la base de datos
      $this->db->where('usuario',$user);
      $this->db->from('Administrador');
      $data = $this->db->get();
      return $data;
	}

	public function cambiarContrasenia($contrasenia){
		return $this->db->update('Administrador', array('contrasenia'=>$contrasenia));
	}

	public function seleccionarCorreo()
	{
	$this->db->select('correoElectronico1'); //se tiene que llamar como en la base de datos
	$this->db->from('Administrador');
	$data = $this->db->get();
	return $data;
	}

  public function registrarIngreso($sessionData){
    $datos= array (
      'sessionId' => $sessionData['session_id'],
      'fecha' => $sessionData['horaFecha'],
      'ip' => $sessionData['ip_address'],
    );
    if($this->db->insert('Ingresos', $datos)){
      return true;
    }
    else{
      return false;
    }
  }

  public function listarIngresos(){
      return $this->db->get('Ingresos');
  }




}
