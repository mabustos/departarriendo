<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_ciudades extends CI_Model {
  public function listarCiudades(){
    $query = $this->db->get('Ciudades');
    return $query;
  }
}