<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_departamentos extends CI_Model {
  public function ingresarDepartamento($data){
      if($this->db->insert('Departamentos',$data)){
          return true;
      }
      else{
          return false;
      }
   }

   public function listarDepartamentosEdificio($idEdificio){
      $this->db->where('idEdificio', $idEdificio);
      return $this->db->get('Departamentos');
   }

   public function listarDepartamentos(){
      return $this->db->get('Departamentos');
   }

    public function seleccionarDepartamento($idEdificio,$numDpto){
       return $this->db->get_where('Departamentos', array('idEdificio' => $idEdificio,'numDpto' => $numDpto));
    }

    public function editarDepartamento($idEdificio,$numDpto,$data){
        $this->db->where('idEdificio', $idEdificio);
        $this->db->where('numDpto', $numDpto);
        return $this->db->update('Departamentos', $data);
    }

    public function seleccionarFotografiasDepartamento($idEdificio,$numDpto){
       return $this->db->get_where('FotografiasDepartamento', array('idEdificio' => $idEdificio,'numDpto' => $numDpto));
    }

    public function editarFotografiasDepartamento($idEdificioOld,$numDptoOld,$idEdificio,$numDpto){
        $this->db->where('idEdificio', $idEdificioOld);
        $this->db->where('numDpto', $numDptoOld);
        $data = array(
               'idEdificio' => $idEdificio,
               'numDpto' => $numDpto
            );
        return $this->db->update('FotografiasDepartamento', $data);
    }

    public function eliminarFotografiasDepartamento($idEdificio,$numDpto,$fotografias){
        if($fotografias==null){
            $fotografias = $this->db->get_where('FotografiasDepartamento', array('idEdificio' => $idEdificio,'numDpto' => $numDpto));
            if(is_null($fotografias->result())){
                return array();
            }
            else{
                $this->db->delete('FotografiasDepartamento', array(
                    'idEdificio' => ($idEdificio),
                    'numDpto' => ($numDpto)
                ));
                return $fotografias->result();
            }


        }else{
            foreach($fotografias as $fotografia){
                $this->db->delete('FotografiasDepartamento', array(
                    'idEdificio' => ($fotografia->idEdificio),
                    'numDpto' => ($fotografia->numDpto),
                    'numFotografia' => ($fotografia->numFotografia)
                    )
                );
            }
        }
        return true;
    }

    public function eliminarDepartamento($idEdificio,$numDpto){
        $this->db->delete('Ocupaciones', array('idEdificio' => $idEdificio,'numDpto' => $numDpto));
        $this->db->delete('Precios', array('idEdificio' => $idEdificio,'numDpto' => $numDpto));
        $this->db->delete('Departamentos', array('idEdificio' => $idEdificio,'numDpto' => $numDpto));

        if($this->db->affected_rows()>0){
            return true;
        }
        else{
            return false;
        }
    }



    public function ingresarImagenes($imagenes){
        if($this->db->insert_batch('FotografiasDepartamento',$imagenes)){
            return true;
        }else{
            return false;
        }
    }

    public function seleccionarCapacidadMaxima(){
        $this->db->select('idEdificio, numDpto, capacidadPersonas');
        return $this->db->get('Departamentos');
    }

}
