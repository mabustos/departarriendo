<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mod_precios extends CI_Model {
    public function definirPlantilla($idEdificio,$numDpto,$cantPersonas,$fecha){
        $data['idEdificio'] = $idEdificio;
        $data['numDpto'] = $numDpto;
        $data['fecha'] = $fecha;
        $data['precio'] = 0;
        $valor = true;
        for($i=0;$i<$cantPersonas;$i++){
            $data['cantidadPersonas'] = $i+1;
            $valor = $valor && $this->db->insert('Precios',$data);
        }
        return $valor;

    }

    public function seleccionarPrecioCapacidad($idEdificio,$numDpto,$capacidad){
        $this->db->where('idEdificio',$idEdificio);
        $this->db->where('numDpto',$numDpto);
        $this->db->where('cantidadPersonas',$capacidad);
        return $this->db->get('Precios');



    }

    public function seleccionarPrecios($idEdificio,$numDpto){
        return $this->db->get_where('Precios', array('idEdificio' => $idEdificio,
                                              'numDpto' => $numDpto
                                                    )
                                    );
    }

    public function ingresarPrecios($dataPreciosDepartamento){
        foreach($dataPreciosDepartamento as $precioDpto){
            $this->db->insert('Precios',$precioDpto);
        }
    }

    public function listarPrecios(){
        return $this->db->get('Precios');
    }

    public function editarPrecios($idEdificio,$numDpto,$precios){
        /*echo '<pre>';
        print_r($precios);
        echo '</pre>';
        //exit('<pre>'.print_r($precios).'</pre>');
        exit();*/
        $this->db->where('idEdificio',$idEdificio);
        $this->db->where('numDpto',$numDpto);
        foreach($precios as $llave => $precio){
            $this->db->where('cantidadPersonas',$llave);
            $this->db->update('Precios',$precio);
        }

    }

    public function eliminarPreciosDepartamento($idEdificio,$numDpto,$capacidad){
        $this->db->where('idEdificio', $idEdificio);
        $this->db->where('numDpto', $numDpto);
        $this->db->where('cantidadPersonas >', $capacidad);
        $this->db->delete('Precios');
    }

    public function rellenarPreciosDepartamento($idEdificio,$numDpto,$capacidadAnterior,$capacidadPosterior,$fecha){
        $data['idEdificio'] = $idEdificio;
        $data['numDpto'] = $numDpto;
        $data['fecha'] = $fecha;
        $data['precio'] = 0;
        for($i=($capacidadAnterior-1);$i<$capacidadPosterior;$i++){
            $data['cantidadPersonas'] = $i+1;
            $this->db->insert('Precios',$data);
        }
    }

    public function editarPreciosDepartamento($actualIdEdificio,$actualNumDpto,$nuevoIdEdificio,$nuevoNumDpto){
        $data = array('idEdificio' => $nuevoIdEdificio,
                      'numDpto'    => $nuevoNumDpto
        );
        $this->db->where('idEdificio', $actualIdEdificio);
        $this->db->where('numDpto', $actualNumDpto);
        return $this->db->update('Precios', $data);
    }
}
