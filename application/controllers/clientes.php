<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
class Clientes extends MY_vistaGeneral {
	function __construct() {
		parent::__construct();
        date_default_timezone_set('America/Santiago');
		$this->load->model('mod_nacionalidades');
		$this->load->model('mod_clientes');
		$this->load->model('mod_contactos');
		$this->load->model('mod_reservas');
		$this->load->model('mod_tarjetas_credito');
		$this->load->helper('form');
		$this->load->library('form_validation');
	}
	private function generarInput($dataInput) {
		$data['CIF']                = array(
			'name' => 'CIF',
			'id' => 'CIF',
			'value' => set_value('CIF', $dataInput['CIF']),
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el RUT del cliente'
		);
		$data['tratamientoCliente'] = array(
			'0' => 'Sr',
			'1' => 'Sra',
			'2' => 'Srta'
		);
		$i                          = 0;
		foreach ($this->mod_nacionalidades->listarNacionalidades()->result() as $fila) {
			$data['nacionalidad'][$fila->idPais] = $fila->gentilicioPais . ' [' . $fila->nombrePais . ']';
		}
		$i = 0;
		foreach ($this->mod_contactos->listarContactos()->result() as $fila) {
			$data['contacto'][$fila->idContacto] = $fila->nombreContacto;
		}
		$i = 0;
		foreach ($this->mod_tarjetas_credito->listarTarjetasCredito()->result() as $fila) {
			$data['tipoTarjetaCliente'][$fila->idTarjeta] = $fila->nombreTarjeta;
		}
		foreach ($this->mod_clientes->seleccionarFotografiasCliente($dataInput['CIF'])->result() as $fila) {
			$data['fotografias'][$fila->numFotografia] = $fila->nombreArchivo;
		}
		$data['nombreCliente']                  = array(
			'name' => 'nombreCliente',
			'id' => 'nombreCliente',
			'value' => set_value('nombreCliente', $dataInput['nombreCliente']),
			'maxlength' => '40',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el nombre del cliente'
		);
		$data['apellidoCliente']                = array(
			'name' => 'apellidoCliente',
			'id' => 'apellidoCliente',
			'value' => set_value('apellidoCliente', $dataInput['apellidoCliente']),
			'maxlength' => '40',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el apellido del cliente'
		);
		$data['sexoCliente']                    = array(
			'0' => 'Masculino',
			'1' => 'Femenino'
		);

        $phpdate = strtotime($dataInput['fechaNacCliente']);
        $mysqldate = date( 'd-m-Y', $phpdate );

		$data['fechaNacCliente']                = array(
			'name' => 'fechaNacCliente',
			'id' => 'fechaNacCliente',
            'value' => set_value('fechaNacCliente',$dataInput['fechaNacCliente']),
			'class' => 'form-control'
		);
		$data['email']                          = array(
			'name' => 'email',
			'id' => 'email',
			'value' => set_value('email', $dataInput['email']),
			'maxlength' => '80',
			'class' => 'form-control',
			'placeholder' => 'Ingrese un correo electrónico'
		);
		$data['emailAlt']                       = array(
			'name' => 'emailAlt',
			'id' => 'emailAlt',
			'value' => set_value('emailAlt', $dataInput['emailAlt']),
			'maxlength' => '80',
			'class' => 'form-control',
			'placeholder' => 'Ingrese un correo de electrónico alternativo'
		);
		$data['telefonoContacto']               = array(
			'name' => 'telefonoContacto',
			'id' => 'telefonoContacto',
			'value' => set_value('telefonoContacto', $dataInput['telefonoContacto']),
			'maxlength' => '15',
			'class' => 'form-control',
			'placeholder' => 'Ingrese un teléfono'
		);
		$data['telefonoContactoAlt']            = array(
			'name' => 'telefonoContactoAlt',
			'id' => 'telefonoContactoAlt',
			'value' => set_value('telefonoContactoAlt', $dataInput['telefonoContactoAlt']),
			'maxlength' => '15',
			'class' => 'form-control',
			'placeholder' => 'Ingrese un teléfono de contacto alternativo'
		);
		$data['skypeCliente']                   = array(
			'name' => 'skypeCliente',
			'id' => 'skypeCliente',
			'value' => set_value('skypeCliente', $dataInput['skypeCliente']),
			'maxlength' => '80',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el usuario Skype del cliente'
		);
		$data['whatsappCliente']                = array(
			'name' => 'whatsappCliente',
			'id' => 'whatsappCliente',
			'value' => set_value('whatsappCliente', $dataInput['whatsappCliente']),
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el Whatsapp del cliente'
		);
		$data['profesion']                      = array(
			'name' => 'profesion',
			'id' => 'profesion',
			'value' => set_value('profesion', $dataInput['profesion']),
			'maxlength' => '80',
			'class' => 'form-control',
			'placeholder' => 'Ingrese profesión'
		);
		$data['numTarjetaCliente']              = array(
			'name' => 'numTarjetaCliente',
			'id' => 'numTarjetaCliente',
			'value' => set_value('numTarjetaCliente', $dataInput['numTarjetaCliente']),
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese número de tarjeta'
		);
		$data['tratamientoClienteSeleccionado'] = $dataInput['tratamientoCliente'];
		$data['sexoSeleccionado']               = $dataInput['sexoCliente'];
		$data['nacionalidadSeleccionada']       = $dataInput['nacionalidad'];
		$data['contactoSeleccionado']           = $dataInput['contacto'];
		$data['tarjetaCreditoSeleccionado']     = $dataInput['tipoTarjetaCliente'];
		return $data;
	}
	public function index() {

	}
	public function agregarCliente() {
		$this->form_validation->set_error_delimiters('', '<br />');
		$this->form_validation->set_rules('CIF', 'RUT o pasaporte', 'required|min_length[1]|max_length[11]|numeric');
		$this->form_validation->set_rules('tratamientoCliente', 'Tratamiento', 'required|min_length[1]|max_length[10]|numeric');
		$this->form_validation->set_rules('nombreCliente', 'Nombre cliente', 'required|min_length[1]|max_length[40]');
		$this->form_validation->set_rules('apellidoCliente', 'Apellido cliente', 'required|min_length[1]|max_length[40]');
		$this->form_validation->set_rules('sexoCliente', 'Sexo', 'required|min_length[1]|max_length[1]|numeric');
		$this->form_validation->set_rules('fechaNacCliente', 'Fecha de nacimiento', 'required|min_length[1]|max_length[11]'); //No supe que tipo de dato ponerle si es date.
		$this->form_validation->set_rules('nacionalidad', 'Nacionalidad', 'required|min_length[1]|max_length[11]|numeric');
		$this->form_validation->set_rules('email', 'E-mail', 'required|min_length[1]|max_length[80]|valid_email');
		$this->form_validation->set_rules('emailAlt', 'E-mail alternativo', 'min_length[5]|max_length[40]|valid_email');
		$this->form_validation->set_rules('telefonoContacto', 'Teléfono de contacto', 'required|min_length[1]|max_length[15]|numeric');
		$this->form_validation->set_rules('telefonoContactoAlt', 'Teléfono de contacto alternativo', 'min_length[1]|max_length[15]|numeric');
		$this->form_validation->set_rules('skypeCliente', 'Skype', 'min_length[1]|max_length[80]');
		$this->form_validation->set_rules('whatsappCliente', 'Whatsapp', 'DEmin_length[1]|max_length[11]|numeric');
		$this->form_validation->set_rules('profesion', 'Profesión', 'required|min_length[1]|max_length[40]');
		$this->form_validation->set_rules('contacto', 'Contacto', 'required|min_length[1]|max_length[11]|numeric');
		$this->form_validation->set_rules('tipoTarjetaCliente', 'Tipo de tarjeta', /*en la BD dice tarjetra*/ 'required|min_length[1]|max_length[11]|numeric');
		$this->form_validation->set_rules('numTarjetaCliente', 'Número de tarjeta', 'required|min_length[1]|max_length[11]|numeric');
		if ($this->form_validation->run() != FALSE) {

			$dataInput          = array(
				'CIF' => $this->input->post('CIF'),
				'tratamientoCliente' => $this->input->post('tratamientoCliente'),
				'nombreCliente' => $this->input->post('nombreCliente'),
				'apellidoCliente' => $this->input->post('apellidoCliente'),
				'sexoCliente' => $this->input->post('sexoCliente'),
				'fechaNacCliente' => date('Y-m-d', strtotime(str_replace("/", "-", ($this->input->post('fechaNacCliente'))))),
				'nacionalidad' => $this->input->post('nacionalidad'),
				'email' => $this->input->post('email'),
				'emailAlt' => $this->input->post('emailAlt'),
				'telefonoContacto' => $this->input->post('telefonoContacto'),
				'telefonoContactoAlt' => $this->input->post('telefonoContactoAlt'),
				'skypeCliente' => $this->input->post('skypeCliente'),
				'whatsappCliente' => $this->input->post('whatsappCliente'),
				'profesion' => $this->input->post('profesion'),
				'contacto' => $this->input->post('contacto'),
				'tipoTarjetaCliente' => $this->input->post('tipoTarjetaCliente'),
				'numTarjetaCliente' => $this->input->post('numTarjetaCliente')
			);
			$fotografiasCliente = $this->session->userdata('fotografiasCliente');
			$i                  = 0;
			if (is_array($fotografiasCliente)) {
				foreach ($fotografiasCliente as $recuperada) {
					$recuperada = preg_replace('/[^(\x20-\x7F)]*/', '', $recuperada); // Reemplazo de nombre de archivo
					$recuperada = preg_replace('/\s+/', '', $recuperada);
					$llegada    = $dataInput['CIF'] . '_' . $i . '.' . substr(strrchr($recuperada, '.'), 1);
					rename((($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/temp/') . $recuperada), (($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/clientes/') . $llegada));
					$archivosClientes[$i] = array(
						'CIF' => $this->input->post('CIF'),
						'numFotografia' => $i,
						'nombreArchivo' => $llegada
					);
					$i++;
				}
			}
			$this->eliminarFotografiasSesion();
			$data = $this->generarInput($dataInput);
			if (!empty($archivosClientes)) {
				$data['ingresoCorreto'] = $this->mod_clientes->ingresarCliente($dataInput) && $this->mod_clientes->ingresarImagenes($archivosClientes);
			} else {
				$data['ingresoCorreto'] = $this->mod_clientes->ingresarCliente($dataInput);
			}
		} else {
			$dataInput = array(
				'CIF' => '',
				'tratamientoCliente' => '',
				'nombreCliente' => '',
				'apellidoCliente' => '',
				'sexoCliente' => '',
				'fechaNacCliente' => '',
				'nacionalidad' => '',
				'email' => '',
				'emailAlt' => '',
				'telefonoContacto' => '',
				'telefonoContactoAlt' => '',
				'skypeCliente' => '',
				'whatsappCliente' => '',
				'profesion' => '',
				'contacto' => '',
				'tipoTarjetaCliente' => '',
				'numTarjetaCliente' => ''
			);
			$data      = $this->generarInput($dataInput);
		}
		$this->cargarNavSup();
		$this->load->view('agregarClienteContent', $data);
		$this->cargarNavInf();
	}
	function editarCliente($CIF = null) {
		$this->form_validation->set_error_delimiters('', '<br />');
		$this->form_validation->set_rules('CIF', 'RUT o pasaporte', 'required|min_length[1]|max_length[11]|numeric');
		$this->form_validation->set_rules('tratamientoCliente', 'Tratamiento', 'required|min_length[1]|max_length[10]|numeric');
		$this->form_validation->set_rules('nombreCliente', 'Nombre cliente', 'required|min_length[1]|max_length[40]');
		$this->form_validation->set_rules('apellidoCliente', 'Apellido cliente', 'required|min_length[1]|max_length[40]');
		$this->form_validation->set_rules('sexoCliente', 'Sexo', 'required|min_length[1]|max_length[1]|numeric');
		$this->form_validation->set_rules('fechaNacCliente', 'Fecha de nacimiento', 'required|min_length[1]|max_length[11]'); //No supe que tipo de dato ponerle si es date.
		$this->form_validation->set_rules('nacionalidad', 'Nacionalidad', 'required|min_length[1]|max_length[11]|numeric');
		$this->form_validation->set_rules('email', 'E-mail', 'required|min_length[1]|max_length[80]|valid_email');
		$this->form_validation->set_rules('emailAlt', 'E-mail alternativo', 'min_length[5]|max_length[40]|valid_email');
		$this->form_validation->set_rules('telefonoContacto', 'Teléfono de contacto', 'required|min_length[1]|max_length[15]|numeric');
		$this->form_validation->set_rules('telefonoContactoAlt', 'Teléfono de contacto alternativo', 'min_length[1]|max_length[15]|numeric');
		$this->form_validation->set_rules('skypeCliente', 'Skype', 'min_length[1]|max_length[80]');
		$this->form_validation->set_rules('whatsappCliente', 'Whatsapp', 'DEmin_length[1]|max_length[11]|numeric');
		$this->form_validation->set_rules('profesion', 'Profesión', 'required|min_length[1]|max_length[40]');
		$this->form_validation->set_rules('contacto', 'Contacto', 'required|min_length[1]|max_length[11]|numeric');
		$this->form_validation->set_rules('tipoTarjetaCliente', 'Tipo de tarjeta', /*en la BD dice tarjetra*/ 'required|min_length[1]|max_length[11]|numeric');
		$this->form_validation->set_rules('numTarjetaCliente', 'Número de tarjeta', 'required|min_length[1]|max_length[11]|numeric');
		if ($CIF != null) {
			$query = $this->mod_clientes->seleccionarCliente($CIF);
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$dataObtenida = array(
						'CIF' => $CIF,
						'tratamientoCliente' => $row->tratamientoCliente,
						'nombreCliente' => $row->nombreCliente,
						'apellidoCliente' => $row->apellidoCliente,
						'sexoCliente' => $row->sexoCliente,
						'fechaNacCliente' => $row->fechaNacCliente,
						'nacionalidad' => $row->nacionalidad,
						'email' => $row->email,
						'emailAlt' => $row->emailAlt,
						'telefonoContacto' => $row->telefonoContacto,
						'telefonoContactoAlt' => $row->telefonoContactoAlt,
						'skypeCliente' => $row->skypeCliente,
						'whatsappCliente' => $row->whatsappCliente,
						'profesion' => $row->profesion,
						'contacto' => $row->contacto,
						'tipoTarjetaCliente' => $row->tipoTarjetaCliente,
						'numTarjetaCliente' => $row->numTarjetaCliente
					);
				}
				if ($this->form_validation->run() != FALSE) {
					date_default_timezone_set('America/Santiago');
					$dataInput          = array(
						'CIF' => $this->input->post('CIF'),
						'tratamientoCliente' => $this->input->post('tratamientoCliente'),
						'nombreCliente' => $this->input->post('nombreCliente'),
						'apellidoCliente' => $this->input->post('apellidoCliente'),
						'sexoCliente' => $this->input->post('sexoCliente'),
						'fechaNacCliente' => date('Y-m-d', strtotime(str_replace("/", "-", ($this->input->post('fechaNacCliente'))))),
						'nacionalidad' => $this->input->post('nacionalidad'),
						'email' => $this->input->post('email'),
						'emailAlt' => $this->input->post('emailAlt'),
						'telefonoContacto' => $this->input->post('telefonoContacto'),
						'telefonoContactoAlt' => $this->input->post('telefonoContactoAlt'),
						'skypeCliente' => $this->input->post('skypeCliente'),
						'whatsappCliente' => $this->input->post('whatsappCliente'),
						'profesion' => $this->input->post('profesion'),
						'contacto' => $this->input->post('contacto'),
						'tipoTarjetaCliente' => $this->input->post('tipoTarjetaCliente'),
						'numTarjetaCliente' => $this->input->post('numTarjetaCliente')
					);
					$fotografiasCliente = $this->session->userdata('fotografiasCliente');
					if (is_array($fotografiasCliente)) {
						$fotografiasActuales = $this->mod_clientes->seleccionarFotografiasCliente($dataObtenida['CIF'])->result();
						$j                   = 0;
						for ($i = 0; $i < 15; $i++) {
							$banderaDisponibilidad = true;
							foreach ($fotografiasActuales as $recuperada) {
								if (($recuperada->numFotografia) == $i)
									$banderaDisponibilidad = false;
							}
							if ($banderaDisponibilidad == true) {
								$fotografiasDisponibles[$j] = $i;
								$j++;
							}
						}
						$i = 0;
						if (!empty($fotografiasDisponibles)) {
							foreach ($fotografiasCliente as $recuperada) {
								$recuperada = preg_replace('/[^(\x20-\x7F)]*/', '', $recuperada); // Reemplazo de nombre de archivo
								$recuperada = preg_replace('/\s+/', '', $recuperada);
								$llegada    = $dataObtenida['CIF'] . '_' . $fotografiasDisponibles[$i] . '.' . substr(strrchr($recuperada, '.'), 1);
								rename((($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/temp/') . $recuperada), (($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/clientes/') . $llegada));
								$archivosClientes[$i] = array(
									'CIF' => $this->input->post('CIF'),
									'numFotografia' => $fotografiasDisponibles[$i],
									'nombreArchivo' => $llegada
								);
								$i++;
								if ($i == $j)
									break;
							}
						}
					}
					$data = $this->generarInput($dataInput);
					foreach ($this->mod_clientes->seleccionarFotografiasCliente($CIF)->result() as $fila) {
						if ($this->input->post(str_replace(".", "_", "$fila->nombreArchivo"))) {
							$fotografiasEliminadas[$fila->nombreArchivo] = $fila;
						}
					}
					if ($dataInput == $dataObtenida && empty($fotografiasEliminadas) && empty($fotografiasCliente)) {
						$data['datosIdenticos'] = TRUE;
					} else {
						$data['datosIdenticos'] = FALSE;
						if (!empty($fotografiasEliminadas)) {
							$this->eliminarFotografias($CIF, $fotografiasEliminadas);
							$data                   = $this->generarInput($dataInput);
							$data['datosIdenticos'] = FALSE;
						}
						if (!empty($fotografiasCliente)) {
							$this->mod_clientes->ingresarImagenes($archivosClientes);
							$this->eliminarFotografiasSesion();
							$data                   = $this->generarInput($dataInput);
							$data['datosIdenticos'] = FALSE;
						}
						if ($dataInput['CIF'] != $dataObtenida['CIF']) {
							$data['ingresoCorreto'] = $this->mod_clientes->ingresarCliente($dataInput);
							if ($data['ingresoCorreto']) {
								$this->mod_clientes->editarFotografiasCliente($dataObtenida['CIF'], $dataInput['CIF']);
								$this->mod_reservas->editarClienteReserva($dataObtenida['CIF'], $dataInput['CIF']);
								$this->mod_clientes->eliminarCliente($dataObtenida['CIF']);
							}
						} else {
							$this->mod_clientes->editarFotografiasCliente($dataObtenida['CIF'], $dataInput['CIF']);
							$data['ingresoCorreto'] = $this->mod_clientes->editarCliente($CIF, $dataInput);
						}
					}
				} else {
					$data                   = $this->generarInput($dataObtenida);
					$data['datosIdenticos'] = false;
				}
				$this->cargarNavSup();
				$this->load->view('editarClienteContent', $data);
				$this->cargarNavInf();
			} else {
				$mensaje = '<div class="alert alert-warning alert-dismissable"><i class="fa fa-ban"></i>No se ha podido encontrar al cliente</div>';
				$this->listarClientes($mensaje);
			}
		} else {
			redirect(base_url('index.php/clientes/listarClientes'));
		}
	}
	public function listarClientes($mensaje = '') {
		$query        = $this->mod_clientes->listarClientes();
		$dataClientes = array();
		foreach ($query->result() as $row) {
			if($row->CIF!=0){
				$archivo = '';
				foreach ($this->mod_clientes->seleccionarFotografiasCliente($row->CIF)->result() as $fila) {
					$archivo = $fila->nombreArchivo;
					break;
				}

				$dataClientes['clientes'][$row->CIF] = array(
					'CIF' => $row->CIF,
					'nombreCliente' => $row->nombreCliente,
					'apellidoCliente' => $row->apellidoCliente,
					'fechaNacCliente' => $row->fechaNacCliente,
					'nacionalidad' => $row->nacionalidad,
					'email' => $row->email,
					'telefonoContacto' => $row->telefonoContacto,
					'imagenPerfil' => $archivo,
				);

				$archivo = NULL;
			}
		}
		$dataClientes['mensaje'] = $mensaje;
		$this->cargarNavSup();
		$this->load->view('listarClienteContent', $dataClientes);
		$this->cargarNavInf();
	}
	public function visualizarCliente($CIF) {
		if($CIF!=null){
			$query = $this->mod_clientes->seleccionarCliente($CIF);
			if ($query->num_rows() > 0 && $CIF!='0') {
				foreach ($query->result() as $row) {
					$dataCliente = array(
						'CIF' => $row->CIF,
						'tratamientoCliente' => $row->tratamientoCliente,
						'nombreCliente' => $row->nombreCliente,
						'apellidoCliente' => $row->apellidoCliente,
						'sexoCliente' => $row->sexoCliente,
						'fechaNacCliente' => $row->fechaNacCliente,
						'nacionalidad' => $row->nacionalidad,
						'email' => $row->email,
						'emailAlt' => $row->emailAlt,
						'telefonoContacto' => $row->telefonoContacto,
						'telefonoContactoAlt' => $row->telefonoContactoAlt,
						'skypeCliente' => $row->skypeCliente,
						'whatsappCliente' => $row->whatsappCliente,
						'profesion' => $row->profesion,
						'contacto' => $row->contacto,
						'tipoTarjetaCliente' => $row->tipoTarjetaCliente,
						'numTarjetaCliente' => $row->numTarjetaCliente
					);
				}
				$i = 0;
				foreach ($this->mod_tarjetas_credito->listarTarjetasCredito()->result() as $fila) {
					$dataCliente['tipoTarjetaCliente'] = $fila->nombreTarjeta;
				}
				if ($dataCliente['tipoTarjetaCliente'] == 0) {
					$dataCliente['tipoTarjetaCliente'] = 'Masculino';
				} else if ($dataCliente['tipoTarjetaCliente'] == 1) {
					$dataCliente['tipoTarjetaCliente'] = 'Femenino';
				}
				$dataCliente['tratamientoCliente'] = $this->mod_clientes->seleccionarTratamiento($dataCliente['tratamientoCliente']);
				foreach ($this->mod_clientes->seleccionarFotografiasCliente($dataCliente['CIF'])->result() as $fila) {
					$dataCliente['fotografias'][$fila->numFotografia] = $fila->nombreArchivo;
				}
				$this->cargarNavSup();
				$this->load->view('visualizarClienteContent', $dataCliente);
				$this->cargarNavInf();
			}
			else {
				$mensaje = '<div class="alert alert-warning alert-dismissable"><i class="fa fa-ban"></i>No se ha podido encontrar al cliente</div>';
				$this->listarClientes($mensaje);
			}
		}
		else {
			redirect(base_url('index.php/clientes/listarClientes'));
		}
	}
	private function eliminarFotografiasSesion() {
		return $this->session->set_userdata('fotografiasCliente', array());
	}
	private function eliminarFotografias($CIF, $fotografiasEliminadas = null) {
		if ($fotografiasEliminadas == null)
			$fotografiasEliminadas = $this->mod_clientes->eliminarFotografiasCliente($CIF, $fotografiasEliminadas);
		else
			$this->mod_clientes->eliminarFotografiasCliente($CIF, $fotografiasEliminadas);
		if (!empty($fotografiasEliminadas)) {
			foreach ($fotografiasEliminadas as $fotografia) {
				unlink($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/clientes/' . ($fotografia->nombreArchivo));
			}
		}
		return true;
	}
	function subirImagenesCliente() {
		if (!empty($_FILES) && is_array($_FILES['file']['tmp_name'])) {
			$this->session->set_userdata('fotografiasCliente', $_FILES['file']['name']);
			$i = 0;
			foreach ($_FILES['file']['tmp_name'] as $valor) {
				$string = preg_replace('/[^(\x20-\x7F)]*/', '', $_FILES['file']['name'][$i]); // Reemplazo de nombre de archivo
				$string = preg_replace('/\s+/', '', $string);
				move_uploaded_file($valor, (($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/temp/') . $string));
				$i++;
			}
		}
	}
    function eliminarCliente($CIF=null){
		if ($CIF!=null) {
			if ($this->buscarCliente($CIF)) {
				$this->eliminarFotografias($CIF);
				if ($this->mod_clientes->eliminarCliente($CIF)) {
					$mensaje = '<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i>Cliente eliminado correctamente</div>';
				}
				else {
					$mensaje = '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>No se ha podido eliminar el cliente</div>';
				}
			}
			else {
				$mensaje = '<div class="alert alert-warning alert-dismissable"><i class="fa fa-ban"></i>No se ha podido encontrar el departamento</div>';
			}

			$this->listarClientes($mensaje);
		}
		else {
			redirect(base_url('index.php/clientes/listarClientes'));
		}
	}
    private function buscarCliente($CIF=null){
		if ($CIF!=null) {
			$query = $this->mod_clientes->seleccionarCliente($CIF);
			if ($query->num_rows() > 0) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			redirect(base_url('index.php/clientes/listarClientes'));
		}
	}

	public function consultarDependencias(){
		$CIF = $this->input->post('CIF');
		$tipo = $this->input->post('tipo');
		$data= array();
		if($tipo=='Res'){
			$i=0;
			$query = $this->mod_reservas->listarReservasCliente($CIF);
			foreach($query->result() as $fila){
				$data[$i] = $fila;
				$i++;
			}
		}



		echo json_encode($data);
	}

}
