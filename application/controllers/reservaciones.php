<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reservaciones extends MY_vistaGeneral {

	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Santiago');
		$this->load->model('mod_clientes');
		$this->load->model('mod_reservas');
		$this->load->model('mod_edificios');
		$this->load->model('mod_departamentos');
		$this->load->model('mod_precios');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('mod_tarjetas_credito');
	}
	public function index()
	{
		redirect(base_url('index.php/reservaciones/listarReservas'));

	}

	private	function generarInput($dataInput = array(),$consultaEdicion=false){

		$query = $this->mod_reservas->listarEstados();
		foreach ($query->result() as $row) {
			$estados[$row->idEstado] = $row->nombre;
		}

		$query = $this->mod_departamentos->seleccionarCapacidadMaxima();
		foreach ($query->result() as $row) {
			$dataReserva['capacidades'][$row->idEdificio][$row->numDpto] = $row->capacidadPersonas;
		}

		// Inicio Datos de cliente
		$query = $this->mod_clientes->listarClientes();
		foreach ($query->result() as $row) {
			$dataReserva['listaClientes'][$row->CIF] = $row->nombreCliente." ".$row->apellidoCliente. " (".$row->CIF.")";
		}

		// Fin Datos de cliente
		$dataReserva['precios'] = array();
		foreach($this->mod_precios->listarPrecios()->result() as $precio){
			$dataReserva['precios'][$precio->idEdificio][$precio->numDpto][$precio->cantidadPersonas] = $precio->precio;
		}

		$query = $this->mod_tarjetas_credito->listarTarjetasCredito();
		foreach ($query->result() as $fila) {
			$dataReserva['tarjetasCredito'][$fila->idTarjeta] = $fila->nombreTarjeta;
		}

		$dataReserva['mesExpiracion'] = array('Enero','Febrero','Marzo','Abril','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
		$dataReserva['anioExpiracion'] = array();
		for($i=date("Y");$i<(date("Y")+10);$i++){
			$dataReserva['anioExpiracion'][$i]=$i;
		}

		$query = $this->mod_reservas->listarEmpresasVuelo();
		foreach ($query->result() as $fila) {
			$dataReserva['empresasVuelo'][$fila->idEmpresa] = $fila->nombreEmpresa;
		}

		if($consultaEdicion){
			$dataReserva['idReserva'] = $dataInput['idReserva'];
			$totalOcupacion=isset($dataInput['valorReserva'])?$dataInput['valorReserva']:0;
			$totalDescuento=$dataInput['descuento']!=''?$dataInput['descuento']:0;
			$totalReserva=$totalOcupacion - ($totalOcupacion*$totalDescuento/100);
		}
		else{
			$totalOcupacion=$this->input->post('totalOcupacion')!=''?$this->input->post('totalOcupacion'):0;
			$totalDescuento=$this->input->post('totalDescuento')!=''?$this->input->post('totalDescuento'):0;
			$totalReserva=$this->input->post('totalReserva')!=''?$this->input->post('totalReserva'):0;
		}




		$dataReserva['codigoCVC'] = array(
			'name' => 'codigoCVC',
			'id' => 'codigoCVC',
			'value' => set_value('codigoCVC', isset($dataInput['codigoCVC'])?$dataInput['codigoCVC']:'') ,
			'maxlength' => '4',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el código CVC'
		);
		$dataReserva['fechaExpiracion'] = array(
			'name' => 'fechaExpiracion',
			'id' => 'fechaExpiracion',
			'value' => set_value('fechaExpiracion', isset($dataInput['fechaExpiracion'])?$dataInput['fechaExpiracion']:'') ,
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese la fecha de expiración'
		);
		$dataReserva['motivoViaje'] = array(
			'name' => 'motivoViaje',
			'id' => 'motivoViaje',
			'value' => set_value('motivoViaje', isset($dataInput['motivoViaje'])?$dataInput['motivoViaje']:'') ,
			'maxlength' => '100',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el motivo del viaje'
		);
		$dataReserva['reservaEstacionamientoInicio'] = array(
			'name' => 'reservaEstacionamientoInicio',
			'id' => 'reservaEstacionamientoInicio',
			'value' => set_value('reservaEstacionamientoInicio', isset($dataInput['estacInicio'])?date('d/m/Y', strtotime(str_replace('-', '/', $dataInput['estacInicio']))):'') ,
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el inicio de la reserva de estacionamiento'
		);
		$dataReserva['reservaEstacionamientoFin'] = array(
			'name' => 'reservaEstacionamientoFin',
			'id' => 'reservaEstacionamientoFin',
			'value' => set_value('reservaEstacionamientoFin', isset($dataInput['estacFin'])?date('d/m/Y', strtotime(str_replace('-', '/', $dataInput['estacFin']))):'') ,
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el fin de la reserva de estacionamiento'
		);
		$dataReserva['modoLlegada'] = array(
			'name' => 'modoLlegada',
			'id' => 'modoLlegada',
			'value' => set_value('modoLlegada', isset($dataInput['modoLlegada'])?$dataInput['modoLlegada']:'') ,
			'maxlength' => '50',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el modo de llegada'
		);
		$dataReserva['horaLlegada'] = array(
			'name' => 'horaLlegada',
			'id' => 'horaLlegada',
			'value' => set_value('horaLlegada', isset($dataInput['horaLlegada'])?$dataInput['horaLlegada']:'') ,
			'maxlength' => '20',
			'class' => 'form-control timepicker',
			'placeholder' => 'Ingrese la hora de llegada'
		);
		$dataReserva['peticionesAdicionales'] = '<textarea class="form-control" '.
		'name="peticionesAdicionales" '.
		'id="peticionesAdicionales" '.
		'maxlength="1000" '.
		'class="form-control" '.
		'placeholder="Ingrese las peticiones adicionales" '.
		'rows="3">'.(isset($dataInput['peticionesAdicionales'])?$dataInput['peticionesAdicionales']:'').'</textarea>';

		$dataReserva['cantidadAcompaniantes'] = array(
			'name' => 'cantidadAcompaniantes',
			'id' => 'cantidadAcompaniantes',
			'value' => set_value('cantidadAcompaniantes', isset($dataInput['cantidadAcompaniantes'])?$dataInput['cantidadAcompaniantes']:'') ,
			'maxlength' => '2',
			'class' => 'form-control numericOnly',
			'placeholder' => 'Ingrese el número de acompañantes'
		);


		$dataReserva['numeroVuelo'] = array(
			'name' => 'numeroVuelo',
			'id' => 'numeroVuelo',
			'value' => set_value('numeroVuelo', isset($dataInput['numeroVuelo'])?$dataInput['numeroVuelo']:'') ,
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el número de vuevo'
		);

		$dataReserva['fechaLlegada'] = array(
			'name' => 'fechaLlegada',
			'id' => 'fechaLlegada',
			'value' => set_value('fechaLlegada', isset($dataInput['fechaLlegada'])?date('d/m/Y', strtotime(str_replace('-', '/', $dataInput['fechaLlegada']))):'') ,
			'maxlength' => '40',
			'class' => 'form-control',
			'placeholder' => 'Ingrese la fecha de llegada'
		);

		$dataReserva['reservaFechaInicio'] = array(
			'name' => 'reservaFechaInicio',
			'id' => 'reservaFechaInicio',
			'value' => set_value('reservaFechaInicio', isset($dataInput['reservaFechaInicio'])?$dataInput['reservaFechaInicio']:'') ,
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese la fecha de inicio de la reserva'
		);
		$dataReserva['reservaFechaTermino'] = array(
			'name' => 'reservaFechaTermino',
			'id' => 'reservaFechaTermino',
			'value' => set_value('reservaFechaTermino', isset($dataInput['reservaFechaTermino'])?$dataInput['reservaFechaTermino']:'') ,
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese la fecha de tórmino de la reserva'
		);

		$dataReserva['totalOcupacion'] = array(
			'name' => 'totalOcupacion',
			'id' => 'totalOcupacion',
			'value' => set_value('totalOcupacion', isset($totalOcupacion)?$totalOcupacion:0) ,
			'maxlength' => '12',
			'class' => 'form-control',
			'readonly' => 'readonly'
		);
		$dataReserva['totalDescuento'] = array(
			'name' => 'totalDescuento',
			'id' => 'totalDescuento',
			'value' => set_value('totalDescuento', isset($totalDescuento)?$totalDescuento:0) ,
			'maxlength' => '12',
			'class' => 'form-control percent'
		);
		$dataReserva['totalReserva'] = array(
			'name' => 'totalReserva',
			'id' => 'totalReserva',
			'value' => set_value('totalReserva', isset($totalReserva)?$totalReserva:0) ,
			'maxlength' => '12',
			'class' => 'form-control',
			'readonly' => 'readonly'
		);

	if(!$consultaEdicion){
		$dataReserva['cantidadReservas'] = $this->input->post('cantidadReservas');
		$total = 0; // Total de la reserva ingresada (considerando las ocupaciones)
		if(isset($dataReserva['cantidadReservas'])){
			$dataReserva['preciosDepartamentos'] = "";
			$dataReserva['departamentosReservados'] = "";
			$objetoPrecio="";
			for ($i=1; $i <= $dataReserva['cantidadReservas']; $i++) {
				$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . "<tr>" ;
				$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . "<td> ". $this->input->post($i.'_idEdificio') . "</td>";
				$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . "<td> ". $this->input->post($i.'_numDpto') . "</td>";
				$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . "<td class='formatoFechaHora'> ". $this->input->post($i.'_fechaInicio') . "</td>";
				$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . "<td class='formatoFechaHora'> ". $this->input->post($i.'_fechaTermino') . "</td>";
				$capacidadMaxima= $dataReserva['capacidades'][$this->input->post($i.'_idEdificio')][$this->input->post($i.'_numDpto')];
				$dropdown = '<select name="'.$i.'_capacidadSeleccionada" id="'.$i.'_capacidadSeleccionada" class="form-control capacidad" >';

				$preciosDepartamentoRePost = "[0,";
				for ($j=1; $j <= $capacidadMaxima; $j++) {

					if($j==$this->input->post($i.'_capacidadSeleccionada')){
						$dropdown = $dropdown . '<option value="' . $j . '" selected>' . $j . '</option>';
					}
					else{
						$dropdown = $dropdown . '<option value="' . $j . '">' . $j . '</option>';
					}

					if($j==$capacidadMaxima){
						$preciosDepartamentoRePost = $preciosDepartamentoRePost.($dataReserva['precios'][$this->input->post($i.'_idEdificio')][$this->input->post($i.'_numDpto')][$j]);
					}
					else{
						$preciosDepartamentoRePost = $preciosDepartamentoRePost.($dataReserva['precios'][$this->input->post($i.'_idEdificio')][$this->input->post($i.'_numDpto')][$j]).",";
					}


				}

				$preciosDepartamentoRePost = $preciosDepartamentoRePost."]";

				$objetoPrecio = "
				{
					idEdificio: ".$this->input->post($i.'_idEdificio').",
					numDpto: ".$this->input->post($i.'_numDpto').",
					precios: ".$preciosDepartamentoRePost."
				};
				";

				$dropdown = $dropdown.'</select>';
				if(isset($dataReserva['precios'][$this->input->post($i.'_idEdificio')][$this->input->post($i.'_numDpto')][$this->input->post($i.'_capacidadSeleccionada')])){
					$precio = $dataReserva['precios'][$this->input->post($i.'_idEdificio')][$this->input->post($i.'_numDpto')][$this->input->post($i.'_capacidadSeleccionada')];
				}
				else{
					$precio = 0;
				}
				$fechaInicio = $this->input->post($i.'_fechaInicio');
				$fechaTermino = $this->input->post($i.'_fechaTermino');


				$diferenciaTiempo = abs(strtotime($fechaTermino) - strtotime($fechaInicio));
				$dias = ceil($diferenciaTiempo/3600/24);
				$total = $precio * $dias;
				/*print "Before";
				print "<meta charset='UTF-8'>";
				print "INICIO: ".$fechaInicio;
				print "FIN: ".$fechaTermino;
				print "DIF: ".$diferenciaTiempo;
				print "DIAS: ".$dias;
				print "PRECIO: ".$precio;
				print "TOTAL: ".$total;
				exit();*/
				$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . "<td> " . $dropdown . "</td>";
				$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . '<td><input type="text" id="precio-fila' .($i).'" value="'.$total.' "class="form-control"  disabled></td>';
				$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . '<td><button class="botonEliminar btn btn-default btn-responsive">Eliminar</button></td>';
				$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . "</tr>" ;
				$dataReserva['preciosDepartamentos'] = $dataReserva['preciosDepartamentos']."listaPrecios['ed".$this->input->post($i.'_idEdificio')."dpto".$this->input->post($i.'_numDpto')."']=".$objetoPrecio;
			}

		}
		else{
			$dataReserva['cantidadReservas'] = 0;
		}
}
else{

	$query = $this->mod_reservas->seleccionarOcupacionesReserva($dataInput['idReserva']);
	$fechaActual = date("Y-m-d H:i:s");
	$menorEstado = 3; // Dotará el estado final de la reserva en base al de las ocupaciones

	$dataReserva['cantidadReservas'] = $query->num_rows();
	$total = 0; // Total de la reserva ingresada (considerando las ocupaciones)
	if(isset($dataReserva['cantidadReservas'])){
		$dataReserva['preciosDepartamentos'] = "";
		$dataReserva['departamentosReservados'] = "";
		$objetoPrecio="";
		$i=0;
		foreach($query->result() as $departamentoReservado){
			$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . "<tr>" ;
			$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . "<td> ". $departamentoReservado->idEdificio . "</td>";
			$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . "<td> ". $departamentoReservado->numDpto . "</td>";
			$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . "<td class='formatoFechaHora'> ". $departamentoReservado->fechaLlegada . "</td>";
			$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . "<td class='formatoFechaHora'> ". $departamentoReservado->fechaPartida . "</td>";


			$capacidadMaxima= $dataReserva['capacidades'][$departamentoReservado->idEdificio][$departamentoReservado->numDpto];
			$dropdown = '<select name="'.$i.'_capacidadSeleccionada" id="'.$i.'_capacidadSeleccionada" class="form-control capacidad" >';
			$preciosDepartamentoRePost = "[0,";
			for ($j=1; $j <= $capacidadMaxima; $j++) {
				if($j==$departamentoReservado->capacidad){
					$dropdown = $dropdown . '<option value="' . $j . '" selected>' . $j . '</option>';
				}
				else{
					$dropdown = $dropdown . '<option value="' . $j . '">' . $j . '</option>';
				}
				if($j==$capacidadMaxima){
					$preciosDepartamentoRePost = $preciosDepartamentoRePost.($dataReserva['precios'][$departamentoReservado->idEdificio][$departamentoReservado->numDpto][$j]);
				}
				else{
					$preciosDepartamentoRePost = $preciosDepartamentoRePost.($dataReserva['precios'][$departamentoReservado->idEdificio][$departamentoReservado->numDpto][$j]).",";
				}


			}
			$preciosDepartamentoRePost = $preciosDepartamentoRePost."]";

			$objetoPrecio = "
			{
				idEdificio: ".$departamentoReservado->idEdificio.",
				numDpto: ".$departamentoReservado->numDpto.",
				precios: ".$preciosDepartamentoRePost."
			};
			";

			$dropdown = $dropdown.'</select>';

			$precio = $dataReserva['precios'][$departamentoReservado->idEdificio][$departamentoReservado->numDpto][1]; // 1: Capacidad seleccionada por defecto
			$fechaInicio = $departamentoReservado->fechaLlegada;
			$fechaTermino = $departamentoReservado->fechaPartida;

			$diferenciaTiempo = abs(strtotime($fechaTermino) - strtotime($fechaInicio));
			$dias = ceil($diferenciaTiempo/3600/24);
			$total = $precio * $dias;
			$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . "<td> " . $dropdown . "</td>";
			$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . '<td><input type="text" id="precio-fila' .($i).'" value="'.$total.' "class="form-control"  disabled></td>';
			$estado = $this->consultarEstadoPorFecha($fechaInicio,$fechaTermino,$fechaActual);
			$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . '<td class="'.$this->determinarEstiloEstado($estado).'">'.$estados[$estado].'</td>';
			$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . '<td><button class="botonEditar btn btn-default">Editar</button></td>';
			$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . '<td><button class="botonEliminar btn btn-default">Eliminar</button></td>';
			$dataReserva['departamentosReservados'] = $dataReserva['departamentosReservados'] . "</tr>" ;
			$dataReserva['preciosDepartamentos'] = $dataReserva['preciosDepartamentos']."listaPrecios['ed".$departamentoReservado->idEdificio."dpto".$departamentoReservado->numDpto."']=".$objetoPrecio;

		}

	}
	else{
		$dataReserva['cantidadReservas'] = 0;
	}

	$query = $this->mod_reservas->seleccionarFotografiasReserva($dataReserva['idReserva']);
	foreach($query->result() as $row) {
		$dataReserva['fotografias'][$row->numFotografia]['idReserva']=$row->idReserva;
		$dataReserva['fotografias'][$row->numFotografia]['numFotografia']=$row->numFotografia;
		$dataReserva['fotografias'][$row->numFotografia]['nombreArchivo']=$row->nombreArchivo;
	}

}
date_default_timezone_set('America/Santiago');

$dataReserva['fechaActualServidor'] = date('Y-m-d');
$dataReserva['idEmpresaVueloSeleccionado'] = isset($dataInput['seleccionEmpresaVuelo'])?$dataInput['seleccionEmpresaVuelo']:''; // Selección de empresa de vuelo dropdown
$dataReserva['idClienteSeleccionado'] = isset($dataInput['seleccionCliente'])?$dataInput['seleccionCliente']:''; // Selección de cliente dropdown
$dataReserva['idTarjetaCreditoSeleccionado'] = isset($dataInput['seleccionTarjetaCredito'])?$dataInput['seleccionTarjetaCredito']:''; // Selección de tarjetaCredito dropdown
$dataReserva['vencMesSeleccionado'] = isset($dataInput['seleccionMesVenc'])?$dataInput['seleccionMesVenc']:''; // Selección de venc. mes dropdown
$dataReserva['vencAnioSeleccionado'] = isset($dataInput['seleccionAnioVenc'])?$dataInput['seleccionAnioVenc']:''; // Selección de venc. anio dropdown
return $dataReserva;

}

public function editarReserva($idReserva){
	$this->form_validation->set_error_delimiters('', '<br />');
	$this->form_validation->set_message('esFechaValida','El campo %s contiene una fecha no válida (El formato debe ser DD/MM/YYYY).');
	$this->form_validation->set_message('esValidoRangoFecha','El campo %s contiene una fecha menos reciente que la de inicio.');
	$this->form_validation->set_message('verificarDisponibilidad','El campo %s contiene una fecha que para ese departamento ya está siendo utilizada.');
	$this->form_validation->set_rules('cantidadReservas', 'Cantidad de reservas', 'required|is_natural_no_zero');
	$this->form_validation->set_rules('seleccionTarjetaCredito', 'Tarjeta de crédito','required');
	$this->form_validation->set_rules('seleccionCliente', 'Cliente', 'required');
	$this->form_validation->set_rules('seleccionMesVenc', 'Mes de vencimiento de tarjeta', 'required|is_natural');
	$this->form_validation->set_rules('seleccionAnioVenc', 'Mes de vencimiento de tarjeta', 'required|is_natural');
	$this->form_validation->set_rules('motivoViaje', 'Motivo de viaje', 'min_length[3]|max_length[100]');
	$this->form_validation->set_rules('modoLlegada', 'Modo de llegada', 'min_length[3]|max_length[50]');
	$this->form_validation->set_rules('fechaLlegada', 'Fecha de llegada del cliente', 'required|min_length[3]|max_length[10]|callback_esFechaValida');
	$this->form_validation->set_rules('reservaEstacionamientoInicio', 'Fecha de inicio de la reserva de estacionamiento', 'min_length[3]|max_length[10]|callback_esFechaValida');
	$this->form_validation->set_rules('reservaEstacionamientoFin', 'Fecha de fin de la reserva de estacionamiento', 'min_length[3]|max_length[10]|callback_esFechaValida|callback_esValidoRangoFecha');
	$this->form_validation->set_rules('horaLlegada', 'Hora de llegada', 'required|min_length[3]|max_length[10]');
	$this->form_validation->set_rules('peticionesAdicionales', 'Peticiones adicionales', 'min_length[3]|max_length[1000]');
	$this->form_validation->set_rules('seleccionEmpresaVuelo', 'Empresa de vuelo', '');
	$this->form_validation->set_rules('numeroVuelo', 'Número de vuelo', 'required|is_natural');
	$this->form_validation->set_rules('cantidadAcompaniantes', 'Cantidad de acompañantes', 'required|is_natural');
	$this->form_validation->set_rules('codigoCVC', 'Código CVC', 'required|numeric');
	$this->form_validation->set_rules('departamentosReservados', 'Departamentos Reservados', '');
	$this->form_validation->set_rules('totalOcupacion', 'Total ocupación', '');
	$this->form_validation->set_rules('totalDescuento', 'Total descuento', '');
	$this->form_validation->set_rules('totalReserva', 'Total reserva', '');


	if($this->input->post('cantidadReservas')){
		for ($i=1; $i <= $this->input->post('cantidadReservas'); $i++) {
			$argumentos = $this->input->post($i.'_fechaTermino').'.'.$this->input->post($i.'_idEdificio').'.'.$this->input->post($i.'_numDpto');
			//exit($argumentos);
			$this->form_validation->set_rules($i.'_fechaInicio', 'Fecha de inicio (fila='.$i.')','');
			$this->form_validation->set_rules($i.'_idEdificio', 'Identificador de edificio (fila ='.$i.')', '');
			$this->form_validation->set_rules($i.'_numDpto', 'Número de departamento (fila ='.$i.')', '');
			$this->form_validation->set_rules($i.'_fechaTermino', 'Fecha de término (fila='.$i.')', '');
			$this->form_validation->set_rules($i.'_capacidadSeleccionada', 'Capacidad seleccionada (fila='.$i.')', '');
			$this->form_validation->set_rules($i.'_capacidadMaxima', 'Capacidad máxima (fila='.$i.')', '');
		}
	}

	if ($idReserva != null) {
		$query = $this->mod_reservas->seleccionarReserva($idReserva);
		if ($query->num_rows() > 0) {
			$row=$query->first_row();

			$dataObtenida['idReserva'] = $idReserva;
			$dataObtenida['seleccionCliente'] = $row->idCliente;
			$dataObtenida['seleccionTarjetaCredito'] = $row->idTarjeta;
			$dataObtenida['codigoCVC'] = $row->codigoCVC;
			$dataObtenida['seleccionMesVenc'] = $row->mesExpiracion;
			$dataObtenida['seleccionAnioVenc'] = $row->anioExpiracion;
			$dataObtenida['motivoViaje'] = $row->motivoViaje;
			$dataObtenida['estacInicio'] = $row->estacInicio;
			$dataObtenida['estacFin'] = $row->estacFin;
			$dataObtenida['modoLlegada'] = $row->modoLlegada;
			$dataObtenida['fechaLlegada'] = $row->fechaLlegada;
			$dataObtenida['horaLlegada'] = $row->horaLlegada;
			$dataObtenida['peticionesAdicionales'] = $row->peticionAdicional;
			$dataObtenida['numeroVuelo'] = $row->numeroVuelo;
			$dataObtenida['seleccionEmpresaVuelo'] = $row->idEmpresa;
			$dataObtenida['cantidadAcompaniantes'] = $row->cantidadAcompaniantes;

			$dataObtenida['descuento'] = $row->descuento;


			if ($this->form_validation->run() != FALSE) {
				$dataInput['idReserva'] = $idReserva;
				$dataInput['seleccionCliente'] = $this->input->post('seleccionCliente');
				$dataInput['seleccionTarjetaCredito'] = $this->input->post('seleccionTarjetaCredito');
				$dataInput['codigoCVC'] = $this->input->post('codigoCVC');
				$dataInput['seleccionMesVenc'] = $this->input->post('seleccionMesVenc');
				$dataInput['seleccionAnioVenc'] = $this->input->post('seleccionAnioVenc');
				$dataInput['motivoViaje'] = $this->input->post('motivoViaje');
				$dataInput['estacInicio'] = $this->input->post('reservaEstacionamientoInicio');
				$dataInput['estacFin'] = $this->input->post('reservaEstacionamientoFin');
				$dataInput['modoLlegada'] = $this->input->post('modoLlegada');
				$dataInput['fechaLlegada'] = $this->input->post('fechaLlegada');
				$dataInput['horaLlegada'] = $this->input->post('horaLlegada');
				$dataInput['peticionesAdicionales'] = $this->input->post('peticionesAdicionales');
				$dataInput['numeroVuelo'] = $this->input->post('numeroVuelo');
				$dataInput['seleccionEmpresaVuelo'] = $this->input->post('seleccionEmpresaVuelo');
				$dataInput['cantidadAcompaniantes'] = $this->input->post('cantidadAcompaniantes');

				$dataInput['descuento'] = $this->input->post('totalDescuento');

				$fotografiasReserva = $this->session->userdata('fotografiasReserva');
				if (is_array($fotografiasReserva)) {
					$fotografiasActuales = $this->mod_reservas->seleccionarFotografiasReserva($idReserva)->result();
					$j = 0;
					for ($i = 0; $i < 15; $i++) {
						$banderaDisponibilidad = true;
						foreach($fotografiasActuales as $recuperada) {
							if (($recuperada->numFotografia) == $i) $banderaDisponibilidad = false;
						}
						if ($banderaDisponibilidad == true) {
							$fotografiasDisponibles[$j] = $i;
							$j++;
						}
					}

					$i = 0;
					if (!empty($fotografiasDisponibles)) {
						foreach($fotografiasReserva as $recuperada) {
							$recuperada = preg_replace('/[^(\x20-\x7F)]*/', '', $recuperada); // Reemplazo de nombre de archivo
							$recuperada = preg_replace('/\s+/', '', $recuperada);
							$llegada = $idReserva . '_' . $fotografiasDisponibles[$i] . '.' . substr(strrchr($recuperada, '.') , 1);
							rename((($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/temp/') . $recuperada) , (($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/reservas/') . $llegada));
							$archivosReserva[$i] = array(
								'idReserva' => $idReserva,
								'numFotografia' => $fotografiasDisponibles[$i],
								'nombreArchivo' => $llegada
							);
							$i++;
							if ($i == $j) break;
						}
					}

				}

				$dataObtenida['fechaLlegada']=date('d/m/Y', strtotime(str_replace('-', '/', $dataObtenida['fechaLlegada'])));
				$dataObtenida['estacInicio']=date('d/m/Y', strtotime(str_replace('-', '/', $dataObtenida['estacInicio'])));
				$dataObtenida['estacFin']=date('d/m/Y', strtotime(str_replace('-', '/', $dataObtenida['estacFin'])));
				$dataInput['horaLlegada']=date('g:i a',strtotime(str_replace('-', '/', $dataObtenida['horaLlegada'])));
				$dataObtenida['horaLlegada']=date('g:i a',strtotime(str_replace('-', '/', $dataObtenida['horaLlegada'])));

				foreach($this->mod_reservas->seleccionarFotografiasReserva($idReserva)->result() as $fila) {
					if ($this->input->post(str_replace(".", "_", "$fila->nombreArchivo"))) $fotografiasEliminadas[$fila->nombreArchivo] = $fila;
				}

				if ($dataInput == $dataObtenida && empty($fotografiasEliminadas) && empty($fotografiasReserva)) {
					$dataReserva = $this->generarInput($dataInput,true);
					$dataReserva['datosIdenticos'] = TRUE;

				}
				else {



					if (!empty($fotografiasEliminadas)) {
						//exit(print_r($fotografiasEliminadas));
						$this->eliminarFotografias($idReserva, $fotografiasEliminadas);
						$dataReserva = $this->generarInput($dataInput,true);
						$dataReserva['datosIdenticos'] = FALSE;
					}

					if (!empty($fotografiasReserva)) {
						$this->mod_reservas->ingresarImagenes($archivosReserva);
						$this->eliminarFotografiasSesion();
						$dataReserva = $this->generarInput($dataInput,true);
						$dataReserva['datosIdenticos'] = FALSE;
					}

					$dataReserva = $this->generarInput($dataInput,true);
					$dataInput = array();
					$dataInput['idCliente'] = $this->input->post('seleccionCliente');
					$dataInput['idEmpresa'] = $this->input->post('seleccionEmpresaVuelo');
					$dataInput['idTarjeta'] = $this->input->post('seleccionTarjetaCredito');

					$dataInput['codigoCVC'] = $this->input->post('codigoCVC');
					$dataInput['mesExpiracion'] = $this->input->post('seleccionMesVenc');
					$dataInput['anioExpiracion'] = $this->input->post('seleccionAnioVenc');
					$dataInput['motivoViaje'] = $this->input->post('motivoViaje');
					$dataInput['estacInicio'] = $this->input->post('reservaEstacionamientoInicio');
					$dataInput['estacFin'] = $this->input->post('reservaEstacionamientoFin');
					$dataInput['modoLlegada'] = $this->input->post('modoLlegada');
					$dataInput['fechaLlegada'] = $this->input->post('fechaLlegada');
					$dataInput['horaLlegada'] = $this->input->post('horaLlegada');
					$dataInput['peticionAdicional'] = $this->input->post('peticionesAdicionales');
					$dataInput['numeroVuelo'] = $this->input->post('numeroVuelo');
					$dataInput['cantidadAcompaniantes'] = $this->input->post('cantidadAcompaniantes');
					$dataInput['descuento'] = $this->input->post('totalDescuento');
					$dataInput['fechaLlegada']=date('Y-m-d', strtotime(str_replace('/', '-', $dataInput['fechaLlegada'])));
					$dataInput['estacInicio']=date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('reservaEstacionamientoInicio'))));
					$dataInput['estacFin']=date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('reservaEstacionamientoFin'))));
					$dataReserva['ingresoCorreto'] = $this->mod_reservas->editarReserva($idReserva, $dataInput);
					$dataReserva['datosIdenticos'] = FALSE;

				}


			}

			else{
				$dataReserva['idReserva'] = $row->idReserva;
				$dataReserva['seleccionCliente'] = $row->idCliente;
				$dataReserva['seleccionTarjetaCredito'] = $row->idTarjeta;
				$dataReserva['codigoCVC'] = $row->codigoCVC;
				$dataReserva['seleccionMesVenc'] = $row->mesExpiracion;
				$dataReserva['seleccionAnioVenc'] = $row->anioExpiracion;
				$dataReserva['motivoViaje'] = $row->motivoViaje;
				$dataReserva['estacInicio'] = $row->estacInicio;
				$dataReserva['estacFin'] = $row->estacFin;
				$dataReserva['modoLlegada'] = $row->modoLlegada;
				$dataReserva['fechaLlegada'] = $row->fechaLlegada;
				$dataReserva['horaLlegada'] = $row->horaLlegada;
				$dataReserva['peticionesAdicionales'] = $row->peticionAdicional;
				$dataReserva['numeroVuelo'] = $row->numeroVuelo;
				$dataReserva['seleccionEmpresaVuelo'] = $row->idEmpresa;
				$dataReserva['cantidadAcompaniantes'] = $row->cantidadAcompaniantes;
				$dataReserva['valorReserva'] = $row->valorReserva;
				$dataReserva['descuento'] = $row->descuento;
				$dataReserva['totalReserva'] = $row->valorReserva - ($row->valorReserva*$row->descuento/100);
				$dataReserva['fechaCreacion'] = $row->fechaCreacion;

				$dataReserva = $this->generarInput($dataReserva,true);
				$dataReserva['datosIdenticos'] = FALSE;
			}
		}
		else{
			$mensaje = '<div class="alert alert-warning alert-dismissable"><i class="fa fa-ban"></i>No se ha podido encontrar la reserva</div>';
			$this->listarReservas($mensaje);
			return true;
		}

		$query = $this->mod_departamentos->listarDepartamentos();
		foreach($query->result() as $row) {
			$dataReserva['departamentos'][$row->idEdificio][$row->numDpto] = array(
				'idEdificio' => $row->idEdificio,
				'numDpto' => $row->numDpto,
				'cantDormitorios' => $row->cantDormitorios,
				'metrosConstruidos' => $row->metrosConstruidos,
				'capacidadPersonas' => $row->capacidadPersonas
			);
		}
		foreach($this->mod_edificios->listarEdificiosDepartamento()->result() as $fila) {
			$dataReserva['edificios'][$fila->idEdificio] = $fila->nombreEdificio;
		}
		$this->cargarNavSup();
		$this->load->view('editarReservaContent',$dataReserva);
		$this->cargarNavInf();
	}
	else{
		redirect(base_url('index.php/reservaciones/listarReservas'));
	}
}

public function agregarReserva(){
	$this->form_validation->set_error_delimiters('', '<br />');
	$this->form_validation->set_message('esFechaValida','El campo %s contiene una fecha no válida (El formato debe ser DD/MM/YYYY).');
	$this->form_validation->set_message('esValidoRangoFecha','El campo %s contiene una fecha menos reciente que la de inicio.');
	$this->form_validation->set_message('verificarDisponibilidad','El campo %s contiene una fecha que para ese departamento ya está siendo utilizada.');
	$this->form_validation->set_rules('cantidadReservas', 'Cantidad de reservas', 'required|is_natural_no_zero');
	$this->form_validation->set_rules('seleccionTarjetaCredito', 'Tarjeta de crédito','required');
	$this->form_validation->set_rules('seleccionCliente', 'Cliente', 'required');
	$this->form_validation->set_rules('seleccionMesVenc', 'Mes de vencimiento de tarjeta', 'required|is_natural');
	$this->form_validation->set_rules('seleccionAnioVenc', 'Mes de vencimiento de tarjeta', 'required|is_natural');
	$this->form_validation->set_rules('motivoViaje', 'Motivo de viaje', 'min_length[3]|max_length[100]');
	$this->form_validation->set_rules('modoLlegada', 'Modo de llegada', 'min_length[3]|max_length[50]');
	$this->form_validation->set_rules('fechaLlegada', 'Fecha de llegada del cliente', 'required|min_length[3]|max_length[10]|callback_esFechaValida');
	$this->form_validation->set_rules('reservaEstacionamientoInicio', 'Fecha de inicio de la reserva de estacionamiento', 'min_length[3]|max_length[10]|callback_esFechaValida');
	$this->form_validation->set_rules('reservaEstacionamientoFin', 'Fecha de fin de la reserva de estacionamiento', 'min_length[3]|max_length[10]|callback_esFechaValida|callback_esValidoRangoFecha');
	$this->form_validation->set_rules('horaLlegada', 'Hora de llegada', 'required|min_length[3]|max_length[10]');
	$this->form_validation->set_rules('peticionesAdicionales', 'Peticiones adicionales', 'min_length[3]|max_length[1000]');
	$this->form_validation->set_rules('seleccionEmpresaVuelo', 'Empresa de vuelo', '');
	$this->form_validation->set_rules('numeroVuelo', 'Número de vuelo', 'required|is_natural');
	$this->form_validation->set_rules('cantidadAcompaniantes', 'Cantidad de acompañantes', 'required|is_natural');
	$this->form_validation->set_rules('codigoCVC', 'Código CVC', 'required|numeric');
	$this->form_validation->set_rules('departamentosReservados', 'Departamentos Reservados', '');
	$this->form_validation->set_rules('totalOcupacion', 'Total ocupación', '');
	$this->form_validation->set_rules('totalDescuento', 'Total descuento', '');
	$this->form_validation->set_rules('totalReserva', 'Total reserva', '');


	if($this->input->post('cantidadReservas')){
		for ($i=1; $i <= $this->input->post('cantidadReservas'); $i++) {
			$argumentos = $this->input->post($i.'_fechaTermino').'.'.$this->input->post($i.'_idEdificio').'.'.$this->input->post($i.'_numDpto');
			$this->form_validation->set_rules($i.'_fechaInicio', 'Fecha de inicio (fila='.$i.')', 'callback_verificarDisponibilidad[' .$argumentos.']');
			$this->form_validation->set_rules($i.'_idEdificio', 'Identificador de edificio (fila ='.$i.')', '');
			$this->form_validation->set_rules($i.'_numDpto', 'Número de departamento (fila ='.$i.')', '');
			$this->form_validation->set_rules($i.'_fechaTermino', 'Fecha de término (fila='.$i.')', '');
			$this->form_validation->set_rules($i.'_capacidadSeleccionada', 'Capacidad seleccionada (fila='.$i.')', '');
			$this->form_validation->set_rules($i.'_capacidadMaxima', 'Capacidad máxima (fila='.$i.')', '');

		}
	}

	if ($this->form_validation->run() != FALSE) {
		$dataReservaInput = array(
			'codigoCVC'         		=> $this->input->post('codigoCVC') ,
			'motivoViaje'       		=> $this->input->post('motivoViaje') ,
			'modoLlegada'       		=> $this->input->post('modoLlegada') ,
			'fechaLlegada'      		=> $this->input->post('fechaLlegada'),
			'horaLlegada'       		=> $this->input->post('horaLlegada') ,
			'peticionesAdicionales' 	=> $this->input->post('peticionesAdicionales') ,
			'cantidadPersonas'  		=> $this->input->post('cantidadPersonas') ,
			'numeroVuelo'       		=> $this->input->post('numeroVuelo') ,
			'idEmpresa'         		=> $this->input->post('seleccionEmpresaVuelo'),
			'valorReserva'      		=> $this->input->post('valorReserva') ,
			'descuento'         		=> $this->input->post('descuento'),
			'estacInicio'  => $this->input->post('reservaEstacionamientoInicio'),
			'estacFin'     => $this->input->post('reservaEstacionamientoFin'),
			'seleccionTarjetaCredito'   => $this->input->post('seleccionTarjetaCredito'),
			'seleccionEmpresaVuelo'     => $this->input->post('seleccionEmpresaVuelo'),
			'seleccionCliente'          => $this->input->post('seleccionCliente'),
			'seleccionMesVenc'          => $this->input->post('seleccionMesVenc'),
			'seleccionAnioVenc'         => $this->input->post('seleccionAnioVenc'),
			'cantidadReservas'			=> $this->input->post('cantidadReservas'),
			'totalOcupacion'			=> $this->input->post('totalOcupacion'),
			'totalDescuento'			=> $this->input->post('totalDescuento'),
			'totalReserva'				=> $this->input->post('totalReserva')
		);




		$dataReserva = $this->generarInput($dataReservaInput);

	$idMaxima = ($this->mod_reservas->seleccionarIdMaxima()->row()->idReserva)+1;


	$fotografiasReserva = $this->session->userdata('fotografiasReserva');
	$i = 0;
	if (is_array($fotografiasReserva)) {
		foreach($fotografiasReserva as $recuperada) {
			$recuperada = preg_replace('/[^(\x20-\x7F)]*/', '', $recuperada); // Reemplazo de nombre de archivo
			$recuperada = preg_replace('/\s+/', '', $recuperada);
			$llegada = $idMaxima. '_' . $i . '.' . substr(strrchr($recuperada, '.') , 1);
			rename((($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/temp/') . $recuperada) , (($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/reservas/') . $llegada));
			$archivosReserva[$i] = array(
				'idReserva' => $idMaxima,
				'numFotografia' => $i,
				'nombreArchivo' => $llegada
			);
			$i++;
		}
	}

	$this->eliminarFotografiasSesion();
	$suma=0;
	for ($i=1; $i <= $dataReserva['cantidadReservas']; $i++) {
		$OcupacionesInput[$i]=array(
			'idReserva'     => $idMaxima,
			'idEdificio'	=> $this->input->post($i.'_idEdificio'),
			'numDpto'		=> $this->input->post($i.'_numDpto'),
			'fechaLlegada'	=> $this->input->post($i.'_fechaInicio'),
			'fechaPartida'	=> $this->input->post($i.'_fechaTermino'),
			'capacidad'		=> $this->input->post($i.'_capacidadSeleccionada'),
			'precioTotal'   => $dataReserva['precios'][$this->input->post($i.'_idEdificio')][$this->input->post($i.'_numDpto')][$this->input->post($i.'_capacidadSeleccionada')]
			*$this->calcularDiasDiferenciaFechas($this->input->post($i.'_fechaTermino'),$this->input->post($i.'_fechaInicio'))
		);
		$suma+=$OcupacionesInput[$i]['precioTotal'];
	}

	$suma-=($suma*$this->input->post('totalDescuento')/100);

	$ReservaInput = array(
		'idReserva'					=> $idMaxima,
		'idCliente'          => $this->input->post('seleccionCliente'),
		'idTarjeta'   => $this->input->post('seleccionTarjetaCredito'),
		'codigoCVC'         		=> $this->input->post('codigoCVC') ,
		'mesExpiracion'          => $this->input->post('seleccionMesVenc'),
		'anioExpiracion'         => $this->input->post('seleccionAnioVenc'),
		'motivoViaje'       		=> $this->input->post('motivoViaje') ,
		'estacInicio'  => $this->input->post('reservaEstacionamientoInicio'),
		'estacFin'     => $this->input->post('reservaEstacionamientoFin'),
		'modoLlegada'       		=> $this->input->post('modoLlegada') ,
		'fechaLlegada'      		=> $this->input->post('fechaLlegada'),
		'horaLlegada'       		=> $this->input->post('horaLlegada') ,
		'peticionAdicional' 	    => $this->input->post('peticionesAdicionales') ,
		'cantidadPersonas'  		=> $this->input->post('cantidadAcompaniantes') ,
		'numeroVuelo'       		=> $this->input->post('numeroVuelo') ,
		'idEmpresa'     		    => $this->input->post('seleccionEmpresaVuelo'),
		'cantidadAcompaniantes'     => $this->input->post('cantidadAcompaniantes'),
		'valorReserva'      		=> $suma ,
		'descuento'         		=> $this->input->post('totalDescuento'),
		'estado'					=> 0,
		'fechaCreacion'			    => date("Y-m-d H:i:s")
	);

	$ReservaInput['fechaLlegada']=date('Y-m-d', strtotime(str_replace('/', '-', $ReservaInput['fechaLlegada'])));
	$ReservaInput['estacInicio']=date('Y-m-d', strtotime(str_replace('/', '-', $ReservaInput['estacInicio'])));
	$ReservaInput['estacFin']=date('Y-m-d', strtotime(str_replace('/', '-', $ReservaInput['estacFin'])));

	if(isset($OcupacionesInput)){
		if (empty($archivosReserva)) {
			$dataReserva['ingresoCorreto'] = $this->mod_reservas->ingresarReserva($ReservaInput)
			&& $this->mod_reservas->ingresarOcupaciones($OcupacionesInput);
		}
		else {
			$dataReserva['ingresoCorreto'] = $this->mod_reservas->ingresarReserva($ReservaInput)
			&& $this->mod_reservas->ingresarImagenes($archivosReserva)
			&& $this->mod_reservas->ingresarOcupaciones($OcupacionesInput);
		}
		$this->session->set_userdata('ocupacionesIngresadas',$dataReserva['cantidadReservas']); // Para no verificar la disponibilidad de los departamentos en el reingreso
	}
	else{
		if (empty($archivosReserva)) {
			$dataReserva['ingresoCorreto'] = $this->mod_reservas->ingresarReserva($ReservaInput);
		}
		else {
			$dataReserva['ingresoCorreto'] = $this->mod_reservas->ingresarReserva($ReservaInput)
			&& $this->mod_reservas->ingresarImagenes($archivosReserva);
		}
	}

}
else{
	$dataReservaInput = array(
		'codigoCVC'         		=> '4545' ,
		'motivoViaje'       		=> 'Negocios' ,
		'modoLlegada'       		=> 'Avion' ,
		'fechaLlegada'      		=> '25/11/2014' ,
		'horaLlegada'       		=> '04:22 PM' ,
		'peticionesAdicionales' 	=> 'Almuerzo al llegar' ,
		'cantidadPersonas'  		=> '5' ,
		'numeroVuelo'       		=> '3321' ,
		'idEmpresa'         		=> '0' ,
		'valorReserva'      		=> $this->input->post('valorReserva') ,
		'descuento'         		=> $this->input->post('descuento'),
		'estacInicio'         		=> '25/11/2014',
		'estacFin'         		=> '26/11/2014',
		'seleccionTarjetaCredito'   => '0',
		'seleccionEmpresaVuelo'     => '0',
		'seleccionCliente'          => '',
		'seleccionMesVenc'          => '5',
		'seleccionAnioVenc'         => '2018',
		'cantidadReservas'			=> $this->input->post('cantidadReservas'),
		'totalOcupacion'			=> $this->input->post('totalOcupacion'),
		'totalDescuento'			=> $this->input->post('totalDescuento'),
		'totalReserva'				=> $this->input->post('totalReserva')
	);
	$dataReserva = $this->generarInput($dataReservaInput);
}

$query = $this->mod_departamentos->listarDepartamentos();
foreach($query->result() as $row) {
	$dataReserva['departamentos'][$row->idEdificio][$row->numDpto] = array(
		'idEdificio' => $row->idEdificio,
		'numDpto' => $row->numDpto,
		'cantDormitorios' => $row->cantDormitorios,
		'metrosConstruidos' => $row->metrosConstruidos,
		'capacidadPersonas' => $row->capacidadPersonas
	);
}
foreach($this->mod_edificios->listarEdificiosDepartamento()->result() as $fila) {
	$dataReserva['edificios'][$fila->idEdificio] = $fila->nombreEdificio;
}
$this->cargarNavSup();
$this->load->view('agregarReservaContent',$dataReserva);
$this->cargarNavInf();

}

public function consultaDisponibilidadDepartamento(){
	$fechaInicio = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('fechaInicio'))));
	$fechaTermino = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('fechaTermino'))));
	$dataEdificio = array();
	foreach($this->mod_edificios->listarEdificiosDepartamento()->result() as $fila) {
		$dataEdificio[$fila->idEdificio] = $fila->nombreEdificio;
	}

	$query = $this->mod_reservas->seleccionarDepartamentosFecha($fechaInicio,$fechaTermino);
	$i=0;

	foreach($query->result() as $row){
		$row->nombreEdificio = $dataEdificio[$row->idEdificio];
		$data[$i] = $row;
		$i++;
	}
	echo json_encode($data);
}

public function eliminarOcupacion(){
	$idEdificio = $this->input->post('idEdificio');
	$numDpto = $this->input->post('numDpto');
	$fechaInicio = date('Y-m-d h:i:s', strtotime(str_replace('/', '-', $this->input->post('fechaLlegada'))));
	$fechaTermino = date('Y-m-d h:i:s', strtotime(str_replace('/', '-', $this->input->post('fechaPartida'))));
	$data = array();
	$data[0] = (object) array('valor'=>$this->mod_reservas->eliminarOcupacion($idEdificio,$numDpto,$fechaInicio,$fechaTermino)) ;//

	echo json_encode($data);
}

public function modificarOcupaciones(){
	$idReserva=$this->input->post('idReserva');
	$idEdificio=$this->input->post('idEdificio');
	$numDpto=$this->input->post('numDpto');
	$capacidad=$this->input->post('ocupantes');
	$fechaInicioNew = str_replace('/', '-', $this->input->post('fechaInicioNew'));
	$fechaTerminoNew = str_replace('/', '-', $this->input->post('fechaTerminoNew'));
	$fechaInicioOld = str_replace('/', '-', $this->input->post('fechaInicioOld'));
	$fechaTerminoOld = str_replace('/', '-', $this->input->post('fechaTerminoOld'));
	$diferenciaDias = $this->calcularDiasDiferenciaFechas($fechaTerminoNew,$fechaInicioNew);
	$precioTotal = 0;
	$precio = 0;



	$precioCapacidad = $this->mod_precios->seleccionarPrecioCapacidad($idEdificio,$numDpto,$capacidad);

	if ($precioCapacidad->num_rows() > 0)
	{
		foreach ($precioCapacidad->result() as $row)
		{
			$precio = intval($row->precio);
		}
	}

	$precioTotal = $diferenciaDias * $precio;
	$i=0;
	$data=array();
	
	foreach($this->mod_reservas->listarOcupacionesDepartamentoFecha($idEdificio,$numDpto,$fechaInicioNew,$fechaTerminoNew)->result() as $fila) {

		$data[$i] = $fila;
		$i++;
	}

	if($i==0)
	{
		$query = $this->mod_reservas->editarOcupacion($idReserva,$idEdificio,$numDpto,$fechaInicioNew,$fechaTerminoNew,$fechaInicioOld,$fechaTerminoOld,$capacidad,$precioTotal);
	}

	echo json_encode($data);
}

public function consultaReservasDepartamento(){
	$idEdificio = $this->input->post('idEdificio');
	$numDpto = $this->input->post('numDpto');
	$nombreEdificio ='';
	$query = $this->mod_edificios->seleccionarEdificio($idEdificio);
	foreach($query->result() as $row){
		$nombreEdificio = $row->nombreEdificio;
	}
	$data = array();

	$i=0;
	foreach($this->mod_reservas->seleccionarOcupacionesDepartamento($idEdificio,$numDpto)->result() as $fila) {
		$fila->nombreEdificio = $nombreEdificio;
		$data[$i] = $fila;
		$i++;
	}
	echo json_encode($data);
}

public function consultaPreciosDepartamento(){
	$idEdificio = $this->input->post('idEdificio');
	$numDpto = $this->input->post('numDpto');
	$query = $this->mod_precios->seleccionarPrecios($idEdificio,$numDpto);
	$data = array();
	$i=0;
	foreach($query->result() as $fila) {
		$data[$i] = $fila;
		$i++;
	}
	echo json_encode($data);
}

public function consultarEstadoPorReserva($idReserva){
	$query = $this->mod_reservas->seleccionarReserva($idReserva);
	if ($query->num_rows() > 0) {
		foreach($query->result() as $row) {
			$estado = $row->estado;
		}
	}
	$cancelada = ($estado==3)?true:false; // Reserva cancelada
	$menorEstado = 3;
	if(!$cancelada){
		$query = $this->mod_reservas->seleccionarOcupacionesReserva($idReserva);
		$fechaActual = date("Y-m-d H:i:s");
		foreach($query->result() as $row) {
			$consultaEstado = $this->consultarEstadoPorFecha($row->fechaLlegada,$row->fechaPartida,$fechaActual);
			if($menorEstado>$consultaEstado){
				$menorEstado=$consultaEstado; // Se empieza con todo abortado. Si existe una ocupación en algún estado menor, la reserva lo tendrá.
			}
		}
	}
	return $menorEstado;
}

public function consultarEstadoPorFecha($fechaInicio,$fechaTermino,$fechaActual){
	//exit($fechaInicio.' '.$fechaTermino.' '.$fechaActual);
	if($fechaActual<$fechaInicio){
		return 0; // Anticipada
	}
	else{
		if($fechaActual<$fechaTermino){
			return 1; // En curso
		}
		else{
			return 2; // Vencida
		}
	}
}

public function determinarEstiloEstado($estado){ // Clase de fila de bootstrap
	switch($estado){
		case 0:
		return 'warning'; // Estado prematuro
		case 1:
		return 'success'; // Estado en curso
		case 2:
		return 'danger'; // Estado vencido
		default:
		return 'warning'; // Estado abortado
	}
}

private function eliminarFotografiasSesion() {
	return $this->session->set_userdata('fotografiasReserva', array());
}

function subirImagenesReserva() {
	if (!empty($_FILES) && is_array($_FILES['file']['tmp_name'])) {
		$this->session->set_userdata('fotografiasReserva', $_FILES['file']['name']);
		$i = 0;
		foreach ($_FILES['file']['tmp_name'] as $valor) {
			$string = preg_replace('/[^(\x20-\x7F)]*/', '', $_FILES['file']['name'][$i]); // Reemplazo de nombre de archivo
			$string = preg_replace('/\s+/', '', $string);
			move_uploaded_file($valor, (($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/temp/') . $string));
			$i++;
		}
	}
}

public function esFechaValida($fecha){
	$fecha = str_replace("/","-",$fecha);
	return (strtotime($fecha))?true:false;
}

function esValidoRangoFecha($fechaInicio,$fechaTermino){
	$estacInicio=$this->input->post('reservaEstacionamientoInicio');
	$estacFin=$this->input->post('reservaEstacionamientoFin');
	if(isset($estacInicio) && isset($estacFin)){
		$fechaInicio = str_replace("/","-",$estacInicio);
		$fechaTermino = str_replace("/","-",$estacFin);
	}

	return (strtotime($fechaInicio) <= strtotime($fechaTermino))?true:false;
}

public function verificarDisponibilidad($fechaInicio,$argumentos){

	if($this->session->userdata('ocupacionesIngresadas')==0){ // Esta condición sirve para evitar que el verificador de formulario
		// llame nuevamente a la verificación de los departamentos recién in-
		// gresados (si no lo tuviera, verificaría el mismo departamento que
		// se ingresó recién).
		list($fechaTermino, $idEdificio, $numDpto) = explode('.', $argumentos);

		$fechaInicio = date('Y-m-d H:i:s', strtotime(str_replace('-', '/',  $fechaInicio)));
		$fechaTermino = date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $fechaTermino)));

		$query = $this->mod_reservas->verificarDisponibilidadFecha($fechaInicio,$fechaTermino,$idEdificio,$numDpto);

		return $query;
	}
	else{
		$this->session->set_userdata('ocupacionesIngresadas',$this->session->userdata('ocupacionesIngresadas')-1);
		return true;
	}

}

public function visualizarReserva($idReserva=null){
	if ($idReserva!=null) {
		$query = $this->mod_reservas->listarEstados();
		foreach ($query->result() as $row) {
			$estados[$row->idEstado] = $row->nombre;
		}

		$query = $this->mod_reservas->listarEmpresasVuelo();
		foreach ($query->result() as $fila) {
			$empresaVuelo[$fila->idEmpresa] = $fila->nombreEmpresa;
		}

		$query = $this->mod_reservas->seleccionarReserva($idReserva);
		if ($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$dataReserva['idReserva'] = $row->idReserva;
				$dataReserva['idCliente'] = $row->idCliente;
				$nombre = $this->mod_clientes->seleccionarCliente($row->idCliente)->first_row();
				$nombre = $nombre->nombreCliente.' '.$nombre->apellidoCliente;
				$dataReserva['nombreCliente'] = $nombre;
				$dataReserva['idTarjeta'] = $this->mod_tarjetas_credito->seleccionarTarjetasCredito($row->idTarjeta)->first_row()->nombreTarjeta;
				$dataReserva['codigoCVC'] = $row->codigoCVC;
				$dataReserva['mesExpiracion'] = $row->mesExpiracion;
				$dataReserva['anioExpiracion'] = $row->anioExpiracion;
				$dataReserva['motivoViaje'] = $row->motivoViaje;
				$dataReserva['estacInicio'] = $row->estacInicio;
				$dataReserva['estacFin'] = $row->estacFin;
				$dataReserva['modoLlegada'] = $row->modoLlegada;
				$dataReserva['fechaLlegada'] = $row->fechaLlegada;
				$dataReserva['horaLlegada'] = $row->horaLlegada;
				$dataReserva['peticionAdicional'] = $row->peticionAdicional;
				$dataReserva['numeroVuelo'] = $row->numeroVuelo;
				$dataReserva['idEmpresa'] = $empresaVuelo[$row->idEmpresa];
				$dataReserva['cantidadAcompaniantes'] = $row->cantidadAcompaniantes;
				$dataReserva['valorReserva'] = $this->calcularTotalReserva($row->idReserva);
				$dataReserva['descuento'] = $row->descuento;
				$dataReserva['totalReserva'] = $dataReserva['valorReserva'] - ($dataReserva['valorReserva']*$row->descuento/100);
				$dataReserva['fechaCreacion'] = $row->fechaCreacion;
				$vencimiento = (($row->estado)==3)?true:false;
			}

			$query = $this->mod_reservas->seleccionarOcupacionesReserva($idReserva);
			$i=0;

			$fechaActual = date("Y-m-d H:i:s");
			$menorEstado = 3; // Dotará el estado final de la reserva en base al de las ocupaciones

			foreach($query->result() as $row) {
				$dataReserva['ocupaciones'][$i]['nombreEdificio']=$this->mod_edificios->seleccionarEdificio($row->idEdificio)->first_row()->nombreEdificio;
				$dataReserva['ocupaciones'][$i]['idEdificio']=$row->idEdificio;
				$dataReserva['ocupaciones'][$i]['numDpto']=$row->numDpto;
				$dataReserva['ocupaciones'][$i]['fechaLlegada']=$row->fechaLlegada;
				$dataReserva['ocupaciones'][$i]['fechaPartida']=$row->fechaPartida;
				$dataReserva['ocupaciones'][$i]['capacidad']=$row->capacidad;
				$dataReserva['ocupaciones'][$i]['precioTotal']=$row->precioTotal;
				$consultaEstado = $this->consultarEstadoPorFecha($row->fechaLlegada,$row->fechaPartida,$fechaActual);

				if($vencimiento){
					$dataReserva['ocupaciones'][$i]['estado'] = $estados[3]; // Si se abortó la reserva, todas las ocupaciones también lo harán.
				}
				else{
					if($menorEstado>$consultaEstado){
						$menorEstado=$consultaEstado; // Se empieza con todo abortado. Si existe una ocupación en algún estado menor, la reserva lo tendrá.
					}
					$dataReserva['ocupaciones'][$i]['estado'] = $estados[$consultaEstado];
				}

				$dataReserva['ocupaciones'][$i]['estadoColor'] = $this->determinarEstiloEstado($consultaEstado);
				$i++;
			}

			$dataReserva['estado'] = $estados[$menorEstado]; // Se asigna el menor estado al de la reserva.
			$dataReserva['estadoColor'] = $this->determinarEstiloEstado($menorEstado);

			$query = $this->mod_reservas->seleccionarFotografiasReserva($idReserva);
			foreach($query->result() as $row) {
				$dataReserva['fotografias'][$row->numFotografia]['idReserva']=$row->idReserva;
				$dataReserva['fotografias'][$row->numFotografia]['numFotografia']=$row->numFotografia;
				$dataReserva['fotografias'][$row->numFotografia]['nombreArchivo']=$row->nombreArchivo;
			}

			$this->cargarNavSup();
			$this->load->view('visualizarReservaContent',$dataReserva);
			$this->cargarNavInf();
		}
		else{
			$mensaje = '<div class="alert alert-warning alert-dismissable"><i class="fa fa-ban"></i>No se ha podido encontrar el departamento</div>';
			$this->listarReservas($mensaje);
		}
	}
	else{
		redirect(base_url('index.php/reservaciones/listarReservas'));
	}
}

public function calcularTotalReserva($idReserva){
	$query = $this->mod_reservas->seleccionarOcupacionesReserva($idReserva);
	$suma=0;
	foreach($query->result() as $row) {
		$suma = $suma + $row->precioTotal;
	}
	return $suma;
}

public function listarReservas($mensaje=null){
	$dataReserva['mensaje'] = $mensaje;
	$query = $this->mod_reservas->listarEstados();
	foreach ($query->result() as $row) {
		$estados[$row->idEstado] = $row->nombre;
	}
	$query = $this->mod_clientes->listarClientes();
	foreach ($query->result() as $row) {
		$clientes[$row->CIF] =$row->nombreCliente." ".$row->apellidoCliente;
	}
	$query = $this->mod_reservas->listarReservas();
	foreach($query->result() as $row) {
		$estado = $this->consultarEstadoPorReserva($row->idReserva);
		$dataReserva['reservas'][$row->idReserva] = array(
			'idReserva' => $row->idReserva,
			'idCliente' => $row->idCliente,
			'nombreCliente' => $clientes[$row->idCliente],
			'fechaCreacion' => $row->fechaCreacion,
			'montoTotal' => $this->calcularTotalReserva($row->idReserva),
			'estado' => $estados[$estado],
			'estiloColor' => $this->determinarEstiloEstado($estado)
		);
	}

	$this->cargarNavSup();
	$this->load->view('listarReservasContent',$dataReserva);
	$this->cargarNavInf();
}

public function calcularDiasDiferenciaFechas($fechaFin,$fechaInicio){
	//http://stackoverflow.com/a/2040589
	$fechaFin = strtotime($fechaFin); // or your date as well
	$fechaInicio = strtotime($fechaInicio);
	$datediff = $fechaFin-$fechaInicio;
	return floor($datediff/(60*60*24));

}

private function eliminarFotografias($idReserva, $fotografiasEliminadas = null)
{
	if ($fotografiasEliminadas == null) $fotografiasEliminadas = $this->mod_reservas->eliminarFotografiasReserva($idReserva, $fotografiasEliminadas);
	else $this->mod_reservas->eliminarFotografiasReserva($idReserva, $fotografiasEliminadas);
	if (!empty($fotografiasEliminadas)) {
		foreach($fotografiasEliminadas as $fotografia) {
			unlink($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/reservas/' . ($fotografia->nombreArchivo));
		}
	}

	return true;
}

function eliminarReserva($idReserva = null)
{
	if ($idReserva!= null) {
		$query = $this->mod_reservas->seleccionarReserva($idReserva);
		if ($query->num_rows() > 0) {
			$this->eliminarFotografias($idReserva);
			if ($this->mod_reservas->eliminarReserva($idReserva)) {
				$mensaje = '<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i>Reserva eliminada correctamente</div>';
			}
			else {
				$mensaje = '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>No se ha podido eliminar la reserva</div>';
			}
		}
		else {
			$mensaje = '<div class="alert alert-warning alert-dismissable"><i class="fa fa-ban"></i>No se ha podido encontrar la reserva</div>';
		}

		$this->listarReservas($mensaje);
	}
	else {
		redirect(base_url('index.php/reservaciones/listarReservas'));
	}
}
}
