<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Impresion extends MY_vistaGeneral{
	function __construct(){
		parent::__construct();
        $this->load->model('mod_edificios');
		$this->load->model('mod_departamentos');
		$this->load->model('mod_precios');
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->load->library('pdf');
	}

	public function index(){
        $rutaFC = FCPATH;
        $rutaReportes = "/reportes/";
        $nombreArchivo = "reporte_precios.pdf";
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        $pdfFilePath = $rutaFC.$rutaReportes.$nombreArchivo;
        unlink($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/'.$rutaReportes.$nombreArchivo);
        $data['page_title'] = 'Reporte de precios'; // pass data to the view

        if (file_exists($pdfFilePath) == FALSE){
            
            $listaPrecios = array();
              $i=0;
              foreach($this->mod_edificios->listarEdificiosDepartamento()->result() as $fila) {
                    $departamentos[$fila->idEdificio] = $fila->nombreEdificio;
              }

              $listaPrecios['Departamentos'] = array();
              foreach($this->mod_precios->listarPrecios()->result() as $precio){
                  $listaPrecios['Departamentos'][$i] = array(
                      'idEdificio' => $departamentos[$precio->idEdificio],
                      'numDpto' => $precio->numDpto,
                      'cantidadPersonas' => $precio->cantidadPersonas,
                      'precio' => $precio->precio
                  ); 
                  $i++;
              }
            
            
            $html = $this->load->view('impresionPrecios', $listaPrecios, true); // render the view into HTML
            $pdf = $this->pdf->load();
            $pdf->SetFooter('Listado de precios'.'|{PAGENO}|'.date(DATE_RFC850)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'F'); // save to file because we can
        }

        redirect(base_url($rutaReportes.$nombreArchivo));
       
    }
    }