<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Dashboard extends MY_vistaGeneral
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('mod_cuenta');
        $this->load->model('mod_reservas');
        $this->load->model('mod_edificios');
        $this->load->model('mod_clientes');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
    public function index()
    {


        foreach ($this->mod_reservas->listarEstados()->result() as $row) {
            $estados[$row->idEstado] = $row->nombre;
        }

        foreach($this->mod_edificios->listarEdificiosDepartamento()->result() as $fila) {
            $edificios[$fila->idEdificio] = $fila->nombreEdificio;
        }

        foreach($this->mod_clientes->listarClientes()->result() as $fila){
            $clientes[$fila->CIF] = $fila->nombreCliente.' '.$fila->apellidoCliente;
        }

        $fechaActual = date("Y-m-d H:i:s");
        $i=0;

    foreach($this->mod_reservas->listarOcupaciones()->result() as $row) {
        $estadoNum = $this->consultarEstadoPorFecha($row->fechaLlegada,$row->fechaPartida,$fechaActual); // Estado en forma numérica para preguntar nombre de estado
        $color = $this->consultarColorEstado($estadoNum);
        $dataCalendario['ocupaciones'][$i] = array(
            'idReserva' => $row->idReserva,
            'nombreEdificio' => $edificios[$row->idEdificio],
            'numDpto' => $row->numDpto,
            'fechaLlegada' => $row->fechaLlegada,
            'fechaPartida' => $row->fechaPartida,
            'estado' => $estados[$estadoNum],
            'colorEstado' => $color,
            'enlace' => (base_url('index.php/reservaciones/visualizarReserva/') ).'/'.$row->idReserva
        );
        $i++;
    }

        $this->cargarNavSup();
        $this->cargarNavInf();
        $this->load->view('calendario',$dataCalendario);
    }


    public function consultarEstadoPorFecha($fechaInicio,$fechaTermino,$fechaActual){
        if($fechaActual<$fechaInicio){
            return 0; // Anticipada
        }
        else{
            if($fechaActual<$fechaTermino){
                return 1; // En curso
            }
            else{
                return 2; // Vencida
            }
        }
    }

    public function consultarColorEstado($estado){
        //exit($fechaInicio.' '.$fechaTermino.' '.$fechaActual);
        if($estado==0){
            return '#FFA500'; // Anticipada
        }
        else{
            if($estado==1){
                return '#6CC417'; // En curso
            }
            else{
                return '#B6B6B4'; // Vencida
            }
        }
    }


}
