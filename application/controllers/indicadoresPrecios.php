<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class IndicadoresPrecios extends MY_vistaGeneral {

	    function __construct()
    {
        parent::__construct();
        $this->load->model('mod_edificios');
		$this->load->model('mod_departamentos');
		$this->load->model('mod_precios');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
  public function index()
	{
    
	}
  public function indicadores()
  {
      $this->cargarNavSup();
      $this->load->view('indicadores');
      $this->cargarNavInf();  
      
  }
  public function listarPrecios()
  {
      $listaPrecios = array();
      $i=0;
      foreach($this->mod_edificios->listarEdificiosDepartamento()->result() as $fila) {
			$departamentos[$fila->idEdificio] = $fila->nombreEdificio;
	  }

      $listaPrecios['Departamentos'] = array();
      foreach($this->mod_precios->listarPrecios()->result() as $precio){
          $listaPrecios['Departamentos'][$i] = array(
              'idEdificio' => $departamentos[$precio->idEdificio],
              'numDpto' => $precio->numDpto,
              'cantidadPersonas' => $precio->cantidadPersonas,
              'precio' => $precio->precio
          ); 
          $i++;
      }
      $listaPrecios['enlaceImpresion'] = base_url('index.php/impresion');
      $this->cargarNavSup();
      $this->load->view('listarPreciosContent',$listaPrecios);
      $this->cargarNavInf();  
  }
  public function informes()
  {
      $this->cargarNavSup();
      $this->cargarNavInf(); 
  }
}
