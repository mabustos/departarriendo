<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientes extends CI_Controller {

	    function __construct()
    {
        parent::__construct();
        $this->load->model('mod_cuenta');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
  
  public function index()
	{
    
	}

  function agregarCliente()
  {
      $this->load->view('dash_header');
      $this->load->view('dash_footer');
      $this->load->view('dash_menuIzq');
      $this->load->view('agregarClienteContent');  
  }
}