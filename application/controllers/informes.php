<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Informes extends MY_vistaGeneral {
    function __construct(){
        parent::__construct();
        $this->load->model('mod_cuenta');
        $this->load->model('mod_informes');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
    
    public function index(){
        $query = $this->mod_informes->listarCiudades();
        foreach ($query->result() as $row) {
            $dataInformes['ciudades'][$row->id] = $row->nombre;
        }
        $dataInformes['numero'] = 3;
        $dataInformes['palabra'] = 'computador';
        $this->cargarNavSup();
		$this->load->view('listarInformes',$dataInformes);
		$this->cargarNavInf();
	}
}

?>