<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Edificios extends MY_vistaGeneral{
	function __construct(){
		parent::__construct();
		$this->load->model('mod_cuenta');
		$this->load->model('mod_ciudades');
		$this->load->model('mod_departamentos');
		$this->load->model('mod_edificios');
		$this->load->model('mod_reservas');
		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index(){
		redirect(base_url('index.php/edificios/listarEdificios'));
	}

	function generarInput($dataInput){
		$data['idEdificio'] = $dataInput['idEdificio'];
		$data['nombreEdificio'] = array(
			'name' => 'nombreEdificio',
			'id' => 'nombreEdificio',
			'value' => set_value('nombreEdificio', $dataInput['nombreEdificio']) ,
			'maxlength' => '40',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el nombre del edificio'
		);
		$data['nombreAdministrador'] = array(
			'name' => 'nombreAdministrador',
			'id' => 'nombreAdministrador',
			'value' => set_value('nombreAdministrador', $dataInput['nombreAdministrador']) ,
			'maxlength' => '40',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el nombre del administrador'
		);
		$data['emailAdministrador'] = array(
			'name' => 'emailAdministrador',
			'id' => 'emailAdministrador',
			'value' => set_value('emailAdministrador', $dataInput['emailAdministrador']) ,
			'maxlength' => '40',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el correo electrónico del administrador'
		);
		$data['telefonoAdministracion'] = array(
			'name' => 'telefonoAdministracion',
			'id' => 'telefonoAdministracion',
			'value' => set_value('telefonoAdministracion', $dataInput['telefonoAdministracion']) ,
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el n° de teléfono del administrador'
		);
		$data['telefonoConsejeria'] = array(
			'name' => 'telefonoConsejeria',
			'id' => 'telefonoConsejeria',
			'value' => set_value('telefonoConsejeria', $dataInput['telefonoConsejeria']) ,
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el n° de teléfono de la consejería'
		);
		$data['datosBancarios'] = array(
			'name' => 'datosBancarios',
			'id' => 'datosBancarios',
			'value' => set_value('datosBancarios', $dataInput['datosBancarios']) ,
			'maxlength' => '300',
			'class' => 'form-control',
			'placeholder' => 'Ingrese los datos bancarios del edificio'
		);
		$data['cantidadPisos'] = array(
			'name' => 'cantidadPisos',
			'id' => 'cantidadPisos',
			'value' => set_value('cantidadPisos', $dataInput['cantidadPisos']) ,
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese la cantidad de pisos del edificio'
		);
		$data['direccionEdificio'] = array(
			'name' => 'direccionEdificio',
			'id' => 'direccionEdificio',
			'value' => set_value('direccionEdificio', $dataInput['direccionEdificio']) ,
			'maxlength' => '40',
			'class' => 'form-control',
			'placeholder' => 'Ingrese la dirección del edificio'
		);
		$data['numeroEdificio'] = array(
			'name' => 'numeroEdificio',
			'id' => 'numeroEdificio',
			'value' => set_value('numeroEdificio', $dataInput['numeroEdificio']) ,
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el número del edificio'
		);
		$data['descripcionEdificio'] = array(
			'name' => 'descripcionEdificio',
			'id' => 'descripcionEdificio',
			'value' => set_value('descripcionEdificio', $dataInput['descripcionEdificio']) ,
			'maxlength' => '300',
			'class' => 'form-control',
			'placeholder' => 'Ingrese la descripción del edificio'
		);
		$data['ciudadSeleccionada'] = $dataInput['ciudadSeleccionada'];
		if (isset($dataInput['ingresoCorrecto'])) $data['ingresoCorrecto'] = $dataInput['ingresoCorrecto'];
		foreach($this->mod_ciudades->listarCiudades()->result() as $fila) {
			$data['ciudadEdificio'][$fila->id] = $fila->nombre;
		}

		return $data;
	}

	function visualizarEdificio($id=null){
        if($id!=null){
            $query = $this->mod_edificios->seleccionarEdificio($id);
            if ($query->num_rows() > 0) {
                foreach($query->result() as $row) {
                    $dataEdificio = array(
                        'idEdificio' => $row->idEdificio,
                        'nombreEdificio' => $row->nombreEdificio,
                        'nombreAdministrador' => $row->nombreAdministrador,
                        'emailAdministrador' => $row->emailAdministrador,
                        'telefonoAdministracion' => $row->telefonoAdministracion,
                        'telefonoConsejeria' => $row->telefonoConsejeria,
                        'datosBancarios' => $row->datosBancarios,
                        'cantidadPisos' => $row->cantidadPisos,
                        'direccionEdificio' => $row->direccionEdificio,
                        'numeroEdificio' => $row->numeroEdificio,
                        'ciudadEdificio' => $row->ciudadEdificio,
                        'descripcionEdificio' => $row->descripcionEdificio
                    );
                }

                $this->cargarNavSup();
                $this->load->view('visualizarEdificioContent', $dataEdificio);
                $this->cargarNavInf();
            }
            else {
                $mensaje = '<div class="alert alert-warning alert-dismissable"><i class="fa fa-ban"></i>No se ha podido encontrar el edificio</div>';
                $this->listarEdificios($mensaje);
            }
        }
        else{
            redirect(base_url('index.php/edificios/listarEdificios'));
        }

	}

	function editarEdificio($id=null){
		$this->form_validation->set_error_delimiters('', '<br />');
		$this->form_validation->set_rules('nombreEdificio', 'Nombre de edificio', 'required|min_length[5]|max_length[40]');
		$this->form_validation->set_rules('nombreAdministrador', 'Nombre del administrador', 'required|min_length[5]|max_length[40]');
		$this->form_validation->set_rules('emailAdministrador', 'Email del administrador', 'required|min_length[5]|max_length[40]|valid_email');
		$this->form_validation->set_rules('telefonoAdministracion', 'Teléfono del administrador', 'required|min_length[5]|max_length[11]|numeric');
		$this->form_validation->set_rules('telefonoConsejeria', 'Teléfono de consejería', 'required|min_length[5]|max_length[11]|numeric');
		$this->form_validation->set_rules('datosBancarios', 'Datos bancarios', 'required|min_length[5]|max_length[300]');
		$this->form_validation->set_rules('cantidadPisos', 'Cantidad de pisos', 'required|min_length[1]|max_length[11]|numeric');
		$this->form_validation->set_rules('direccionEdificio', 'Dirección del edificio', 'required|min_length[5]|max_length[40]');
		$this->form_validation->set_rules('numeroEdificio', 'Número del edificio', 'required|min_length[1]|max_length[11]|numeric');
		$this->form_validation->set_rules('ciudadEdificio', 'Ciudad del edificio', 'required|min_length[1]|max_length[11]|numeric');
		$this->form_validation->set_rules('descripcionEdificio', 'Descripción del edificio', 'required|min_length[5]|max_length[300]');
        if($id!=null){
            $query = $this->mod_edificios->seleccionarEdificio($id);
            if ($query->num_rows() > 0) {
                foreach($query->result() as $row) {
                    $dataObtenida = array(
                        'idEdificio' => $row->idEdificio,
                        'nombreEdificio' => $row->nombreEdificio,
                        'nombreAdministrador' => $row->nombreAdministrador,
                        'emailAdministrador' => $row->emailAdministrador,
                        'telefonoAdministracion' => $row->telefonoAdministracion,
                        'telefonoConsejeria' => $row->telefonoConsejeria,
                        'datosBancarios' => $row->datosBancarios,
                        'cantidadPisos' => $row->cantidadPisos,
                        'direccionEdificio' => $row->direccionEdificio,
                        'numeroEdificio' => $row->numeroEdificio,
                        'ciudadEdificio' => $row->ciudadEdificio,
                        'descripcionEdificio' => $row->descripcionEdificio,
                        'ciudadSeleccionada' => $row->ciudadEdificio
                    );
                }

                if ($this->form_validation->run() != FALSE) {
                    $dataEdificioInput = array(
                        'idEdificio' => $dataObtenida['idEdificio'],
                        'nombreEdificio' => $this->input->post('nombreEdificio') ,
                        'nombreAdministrador' => $this->input->post('nombreAdministrador') ,
                        'emailAdministrador' => $this->input->post('emailAdministrador') ,
                        'telefonoAdministracion' => $this->input->post('telefonoAdministracion') ,
                        'telefonoConsejeria' => $this->input->post('telefonoConsejeria') ,
                        'datosBancarios' => $this->input->post('datosBancarios') ,
                        'cantidadPisos' => $this->input->post('cantidadPisos') ,
                        'direccionEdificio' => $this->input->post('direccionEdificio') ,
                        'numeroEdificio' => $this->input->post('numeroEdificio') ,
                        'ciudadEdificio' => $this->input->post('ciudadEdificio') ,
                        'descripcionEdificio' => $this->input->post('descripcionEdificio')
                    );
                    if ($dataObtenida['nombreEdificio'] == $dataEdificioInput['nombreEdificio'] && $dataObtenida['nombreAdministrador'] == $dataEdificioInput['nombreAdministrador'] && $dataObtenida['emailAdministrador'] == $dataEdificioInput['emailAdministrador'] && $dataObtenida['telefonoAdministracion'] == $dataEdificioInput['telefonoAdministracion'] && $dataObtenida['telefonoConsejeria'] == $dataEdificioInput['telefonoConsejeria'] && $dataObtenida['datosBancarios'] == $dataEdificioInput['datosBancarios'] && $dataObtenida['cantidadPisos'] == $dataEdificioInput['cantidadPisos'] && $dataObtenida['direccionEdificio'] == $dataEdificioInput['direccionEdificio'] && $dataObtenida['numeroEdificio'] == $dataEdificioInput['numeroEdificio'] && $dataObtenida['ciudadEdificio'] == $dataEdificioInput['ciudadEdificio'] && $dataObtenida['descripcionEdificio'] == $dataEdificioInput['descripcionEdificio']) {
                        $dataEdificio['datosIdenticos'] = true;
                    }
                    else {
                        $dataEdificio['datosIdenticos'] = false;
                        $dataEdificio['ingresoCorreto'] = $this->mod_edificios->editarEdificio($id, $dataEdificioInput);
                    }

                    $dataEdificioInput['ciudadSeleccionada'] = $this->input->post('ciudadEdificio');
                    $dataEdificio = array_merge($dataEdificio, $this->generarInput($dataEdificioInput));
                }
                else {
                    $dataEdificio = $this->generarInput($dataObtenida);
                    $dataEdificio['datosIdenticos'] = false;
                }

                $this->cargarNavSup();
                $this->load->view('editarEdificioContent', $dataEdificio);
                $this->cargarNavInf();
            }
            else {
                $mensaje = '<div class="alert alert-warning alert-dismissable"><i class="fa fa-ban"></i>No se ha podido encontrar el edificio</div>';
                $this->listarEdificios($mensaje);
            }
        }
        else{
            redirect(base_url('index.php/edificios/listarEdificios'));
        }

	}

	function agregarEdificio(){
		$this->form_validation->set_error_delimiters('', '<br />');
		$this->form_validation->set_rules('nombreEdificio', 'Nombre de edificio', 'required|min_length[5]|max_length[40]');
		$this->form_validation->set_rules('nombreAdministrador', 'Nombre del administrador', 'required|min_length[5]|max_length[40]');
		$this->form_validation->set_rules('emailAdministrador', 'Email del administrador', 'required|min_length[5]|max_length[40]|valid_email');
		$this->form_validation->set_rules('telefonoAdministracion', 'Teléfono del administrador', 'required|min_length[5]|max_length[11]|numeric');
		$this->form_validation->set_rules('telefonoConsejeria', 'Teléfono de consejería', 'required|min_length[5]|max_length[11]|numeric');
		$this->form_validation->set_rules('datosBancarios', 'Datos bancarios', 'required|min_length[5]|max_length[300]');
		$this->form_validation->set_rules('cantidadPisos', 'Cantidad de pisos', 'required|min_length[1]|max_length[11]|numeric');
		$this->form_validation->set_rules('direccionEdificio', 'Dirección del edificio', 'required|min_length[5]|max_length[40]');
		$this->form_validation->set_rules('numeroEdificio', 'Número del edificio', 'required|min_length[1]|max_length[11]|numeric');
		$this->form_validation->set_rules('ciudadEdificio', 'Ciudad del edificio', 'required|min_length[1]|max_length[11]|numeric');
		$this->form_validation->set_rules('descripcionEdificio', 'Descripción del edificio', 'required|min_length[5]|max_length[300]');

        $dataEdificioInput = array(
				'nombreEdificio' => $this->input->post('nombreEdificio') ,
				'nombreAdministrador' => $this->input->post('nombreAdministrador') ,
				'emailAdministrador' => $this->input->post('emailAdministrador') ,
				'telefonoAdministracion' => $this->input->post('telefonoAdministracion') ,
				'telefonoConsejeria' => $this->input->post('telefonoConsejeria') ,
				'datosBancarios' => $this->input->post('datosBancarios') ,
				'cantidadPisos' => $this->input->post('cantidadPisos') ,
				'direccionEdificio' => $this->input->post('direccionEdificio') ,
				'numeroEdificio' => $this->input->post('numeroEdificio') ,
				'ciudadEdificio' => $this->input->post('ciudadEdificio') ,
				'descripcionEdificio' => $this->input->post('descripcionEdificio')
			);

		$mensaje = '';
		if ($this->form_validation->run() != FALSE){

			$query = $this->mod_edificios->seleccionarEdificioCaracteristico($dataEdificioInput);

			if($query->num_rows()==0){
				$insercionEdificio = $this->mod_edificios->ingresarEdificio($dataEdificioInput);
				$dataEdificioInput['idEdificio'] = '';
				$dataEdificioInput['ciudadSeleccionada'] = $this->input->post('ciudadEdificio');
				$dataEdificio = $this->generarInput($dataEdificioInput);
				if($insercionEdificio){
					$dataEdificio['mensaje'] = '<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i>Edificio ingresado correctamente</div>';
				}
				else{
					$dataEdificio['mensaje'] = '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>No se ha podido agregar el edificio</div>';
				}
			}
			else{
				$dataEdificioInput['idEdificio'] = '';
				$dataEdificioInput['ciudadSeleccionada'] = $this->input->post('ciudadEdificio');
				$dataEdificio = $this->generarInput($dataEdificioInput);
				$dataEdificio['mensaje'] = '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>No se ha podido agregar el edificio. Ya existe un edificio con el mismo nombre en esa ciudad con ese número.</div>';
			}


		}
        else{
            $dataEdificioInput['idEdificio'] = '';
            $dataEdificioInput['ciudadSeleccionada'] = $this->input->post('ciudadEdificio');
			$dataEdificio = $this->generarInput($dataEdificioInput);
        }

		$this->cargarNavSup();
		$this->load->view('agregarEdificioContent', $dataEdificio);
		$this->cargarNavInf();
	}

	function listarEdificios($mensaje = ''){
		$dataEdificio = array();
		$query = $this->mod_edificios->listarEdificios();
		foreach($query->result() as $row) {
			$dataEdificio['edificios'][$row->idEdificio] = array(
				'idEdificio' => $row->idEdificio,
				'nombreEdificio' => $row->nombreEdificio,
				'nombreAdministrador' => $row->nombreAdministrador,
				'cantidadPisos' => $row->cantidadPisos,
				'ciudad' => $row->ciudadEdificio
			);
		}
		foreach($this->mod_ciudades->listarCiudades()->result() as $fila) {
			$dataEdificio['ciudadEdificio'][$fila->id] = $fila->nombre;
		}

		$dataEdificio['mensaje'] = $mensaje;
		$this->cargarNavSup();
		$this->load->view('listarEdificioContent', $dataEdificio);
		$this->cargarNavInf();
	}

	function eliminarEdificio($id=null){
        if($id!=null){
            if ($this->buscarEdificio($id)) {
                if ($this->mod_edificios->eliminarEdificio($id)) {
                    $mensaje = '<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i>Edificio eliminado correctamente</div>';
                }
                else {
                    $mensaje = '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>No se ha podido eliminar el edificio</div>';
                }
            }
            else {
                $mensaje = '<div class="alert alert-warning alert-dismissable"><i class="fa fa-ban"></i>No se ha podido encontrar el edificio</div>';
            }

            $this->listarEdificios($mensaje);
        }
        else{
            redirect(base_url('index.php/edificios/listarEdificios'));
        }

	}

	private function buscarEdificio($id){
		$query = $this->mod_edificios->seleccionarEdificio($id);
		if ($query->num_rows() > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	public function consultarDependencias(){
		$idEdificio = $this->input->post('idEdificio');
		$tipo = $this->input->post('tipo');
		$data= array();
		if($tipo=='Dep'){
			$i=0;
			$query = $this->mod_departamentos->listarDepartamentosEdificio($idEdificio);
			foreach($query->result() as $fila){
				$data[$i] = $fila;
				$i++;
			}
		}
		else if($tipo=='Ocu'){
			$i=0;
			$query = $this->mod_reservas->listarOcupacionesEdificio($idEdificio);
			foreach($query->result() as $fila){
				$data[$i] = $fila;
				$i++;
			}
		}
		echo json_encode($data);
	}
}
