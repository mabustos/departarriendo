<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
class Departamentos extends MY_vistaGeneral

{
	function __construct()
	{
		parent::__construct();
		$this->load->model('mod_edificios');
		$this->load->model('mod_departamentos');
		$this->load->model('mod_ciudades');
		$this->load->model('mod_precios');
		$this->load->model('mod_reservas');
		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function index()
	{
		redirect(base_url('index.php/departamentos/listarDepartamentos'));
	}

	private	function generarInput($dataInput,$precios=array())
	{
		$query = $this->mod_edificios->listarEdificios();
		foreach($this->mod_departamentos->seleccionarFotografiasDepartamento($dataInput['idEdificio'], $dataInput['numDpto'])->result() as $fila) {
			$dataDepartamento['fotografias'][$fila->numFotografia] = $fila->nombreArchivo;
		}

		foreach($query->result() as $fila) {
			$dataDepartamento['idEdificio'][$fila->idEdificio] = $fila->idEdificio;
			$dataDepartamento['nombreEdificio'][$fila->idEdificio] = $fila->nombreEdificio;
			$dataDepartamento['pisosEdificio'][$fila->idEdificio] = $fila->cantidadPisos;
		}

		for($i=1;$i<=$dataInput['capacidadPersonas'];$i++){
			$dataDepartamento['precio'][($i)] = array(
				'name' => 'precio_'.($i),
				'id' => 'precio_'.($i),
				'value' => set_value('precio_'.($i), isset($precios[$i])?$precios[$i]:0) ,
				'maxlength' => '11',
				'class' => 'form-control',
				'placeholder' => 'Ingrese el precio'
			);
		}

		$dataDepartamento['numDpto'] = array(
			'name' => 'numDpto',
			'id' => 'numDpto',
			'value' => set_value('numDpto', $dataInput['numDpto']) ,
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese el número de departamento'
		);

		$dataDepartamento['orientacion'] = array(
			'N',
			'NO',
			'O',
			'SO',
			'S',
			'SE',
			'E',
			'NE'
		);
		$dataDepartamento['cantDormitorios'] = array(
			'name' => 'cantDormitorios',
			'id' => 'cantDormitorios',
			'value' => set_value('cantDormitorios', $dataInput['cantDormitorios']) ,
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese la cantidad de dormitorios'
		);
		$dataDepartamento['cantBanios'] = array(
			'name' => 'cantBanios',
			'id' => 'cantBanios',
			'value' => set_value('cantBanios', $dataInput['cantBanios']) ,
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese la cantidad de baños'
		);
		$dataDepartamento['metrosConstruidos'] = array(
			'name' => 'metrosConstruidos',
			'id' => 'metrosConstruidos',
			'value' => set_value('metrosConstruidos', $dataInput['metrosConstruidos']) ,
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese la cantidad de metros construidos'
		);
		$dataDepartamento['capacidadPersonas'] = array(
			'name' => 'capacidadPersonas',
			'id' => 'capacidadPersonas',
			'value' => set_value('capacidadPersonas', $dataInput['capacidadPersonas']) ,
			'maxlength' => '11',
			'class' => 'form-control',
			'placeholder' => 'Ingrese la capacidad de personas'
		);
		$dataDepartamento['descripcion'] = array(
			'name' => 'descripcion',
			'id' => 'descripcion',
			'value' => set_value('descripcion', $dataInput['descripcion']) ,
			'maxlength' => '1000',
			'class' => 'form-control',
			'placeholder' => 'Ingrese una breve descripción'
		);
		$dataDepartamento['dispWIFI'] = array(
			'name' => 'dispWIFI',
			'id' => 'dispWIFI',
			'value' => 'aceptado',
			'checked' => ($dataInput['dispWIFI']) ? TRUE : FALSE
		);
		$dataDepartamento['dispCocina'] = array(
			'name' => 'dispCocina',
			'id' => 'dispCocina',
			'value' => 'aceptado',
			'checked' => ($dataInput['dispCocina']) ? TRUE : FALSE
		);
		$dataDepartamento['dispCable'] = array(
			'name' => 'dispCable',
			'id' => 'dispCable',
			'value' => 'aceptado',
			'checked' => ($dataInput['dispCable']) ? TRUE : FALSE
		);
		$dataDepartamento['dispRefrigerador'] = array(
			'name' => 'dispRefrigerador',
			'id' => 'dispRefrigerador',
			'value' => 'aceptado',
			'checked' => ($dataInput['dispRefrigerador']) ? TRUE : FALSE
		);
		$dataDepartamento['dispCalefactor'] = array(
			'name' => 'dispCalefactor',
			'id' => 'dispCalefactor',
			'value' => 'aceptado',
			'checked' => ($dataInput['dispCalefactor']) ? TRUE : FALSE
		);
		$dataDepartamento['dispMicroondas'] = array(
			'name' => 'dispMicroondas',
			'id' => 'dispMicroondas',
			'value' => 'aceptado',
			'checked' => ($dataInput['dispMicroondas']) ? TRUE : FALSE
		);
		$dataDepartamento['idEdificioSeleccionado'] = $dataInput['idEdificio']; // Selección de edificio dropdown
		$dataDepartamento['numPisoSeleccionado'] = $dataInput['numPiso']; // Selección de nÂº de piso dropdown
		$dataDepartamento['orientacionSeleccionado'] = $dataInput['orientacion']; // Selección de orientación dropdown
		return $dataDepartamento;
	}

	function editarDepartamento($idEdificio = null, $numDpto = null)
	{
		date_default_timezone_set('America/Santiago');
		$this->form_validation->set_error_delimiters('', '<br />');
		$this->form_validation->set_rules('idEdificio', 'Edificio', 'required|min_length[1]|max_length[11]|is_natural');
		$this->form_validation->set_rules('numDpto', 'Número de departamento', 'required|min_length[1]|max_length[11]|is_natural');
		$this->form_validation->set_rules('numPiso', 'Número de piso', 'required|min_length[1]|max_length[11]|numeric');
		$this->form_validation->set_rules('orientacion', 'Orientación del edificio', 'required|min_length[1]|max_length[1]|is_batyrak');
		$this->form_validation->set_rules('cantDormitorios', 'Cantidad de dormitorios', 'required|min_length[1]|max_length[11]|is_natural_no_zero');
		$this->form_validation->set_rules('cantBanios', 'Cantidad de baños', 'required|min_length[1]|max_length[11]|is_natural_no_zero');
		$this->form_validation->set_rules('metrosConstruidos', 'Metros cuadrados construí­dos', 'required|min_length[1]|max_length[11]|is_natural_no_zero');
		$this->form_validation->set_rules('capacidadPersonas', 'Capacidad de personas', 'required|min_length[1]|max_length[11]|is_natural_no_zero');
		$this->form_validation->set_rules('descripcion', 'Descripción de departamento', 'required|min_length[5]|max_length[1000]');
		if ($idEdificio != null && $numDpto != null) {
			$query = $this->mod_departamentos->seleccionarDepartamento($idEdificio, $numDpto);
			if ($query->num_rows() > 0) {
				foreach($query->result() as $row) {
					$dataObtenida = array(
						'idEdificio' => $row->idEdificio,
						'numDpto' => $row->numDpto,
						'numPiso' => $row->numPiso,
						'orientacion' => $row->orientacion,
						'cantDormitorios' => $row->cantDormitorios,
						'cantBanios' => $row->cantBanios,
						'metrosConstruidos' => $row->metrosConstruidos,
						'capacidadPersonas' => $row->capacidadPersonas,
						'descripcion' => $row->descripcion,
						'dispWIFI' => ($row->dispWIFI ? 1 : 0) ,
						'dispCocina' => ($row->dispCocina ? 1 : 0) ,
						'dispCable' => ($row->dispCable ? 1 : 0) ,
						'dispRefrigerador' => ($row->dispRefrigerador ? 1 : 0) ,
						'dispCalefactor' => ($row->dispCalefactor ? 1 : 0) ,
						'dispMicroondas' => ($row->dispMicroondas ? 1 : 0)
					);
				}


				if($this->input->post('capacidadPersonas')){
					for ($i=1; $i <= $this->input->post('capacidadPersonas'); $i++) {
						$this->form_validation->set_rules('precio_'.$i, 'Precio (capacidad='.$i.')', 'is_natural');
					}
				}
				else{
					for ($i=1; $i <= $dataObtenida['capacidadPersonas']; $i++) {
						$this->form_validation->set_rules('precio_'.$i, 'Precio (capacidad='.$i.')', 'is_natural');
					}
				}

				if ($this->form_validation->run() != FALSE) {


					$dataInput = array(
						'idEdificio' => $this->input->post('idEdificio') ,
						'numDpto' => $this->input->post('numDpto') ,
						'numPiso' => $this->input->post('numPiso') ,
						'orientacion' => $this->input->post('orientacion') ,
						'cantDormitorios' => $this->input->post('cantDormitorios') ,
						'cantBanios' => $this->input->post('cantBanios') ,
						'metrosConstruidos' => $this->input->post('metrosConstruidos') ,
						'capacidadPersonas' => $this->input->post('capacidadPersonas') ,
						'descripcion' => $this->input->post('descripcion') ,
						'dispWIFI' => ($this->input->post('dispWIFI') ? 1 : 0) ,
						'dispCocina' => ($this->input->post('dispCocina') ? 1 : 0) ,
						'dispCable' => ($this->input->post('dispCable') ? 1 : 0) ,
						'dispRefrigerador' => ($this->input->post('dispRefrigerador') ? 1 : 0) ,
						'dispCalefactor' => ($this->input->post('dispCalefactor') ? 1 : 0) ,
						'dispMicroondas' => ($this->input->post('dispMicroondas') ? 1 : 0)
					);
					$fotografiasDepartamento = $this->session->userdata('fotografiasDepartamento');
					if (is_array($fotografiasDepartamento)) {
						$fotografiasActuales = $this->mod_departamentos->seleccionarFotografiasDepartamento($dataObtenida['idEdificio'], $dataObtenida['numDpto'])->result();
						$j = 0;
						for ($i = 0; $i < 15; $i++) {
							$banderaDisponibilidad = true;
							foreach($fotografiasActuales as $recuperada) {
								if (($recuperada->numFotografia) == $i) $banderaDisponibilidad = false;
							}
							if ($banderaDisponibilidad == true) {
								$fotografiasDisponibles[$j] = $i;
								$j++;
							}
						}

						$i = 0;
						if (!empty($fotografiasDisponibles)) {
							foreach($fotografiasDepartamento as $recuperada) {
								$recuperada = preg_replace('/[^(\x20-\x7F)]*/', '', $recuperada); // Reemplazo de nombre de archivo
								$recuperada = preg_replace('/\s+/', '', $recuperada);
								$llegada = $dataObtenida['idEdificio'] . '_' . $dataObtenida['numDpto'] . '_' . $fotografiasDisponibles[$i] . '.' . substr(strrchr($recuperada, '.') , 1);
								rename((($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/temp/') . $recuperada) , (($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/departamentos/') . $llegada));
								$archivosDepartamento[$i] = array(
									'idEdificio' => $this->input->post('idEdificio') ,
									'numDpto' => $this->input->post('numDpto') ,
									'numFotografia' => $fotografiasDisponibles[$i],
									'nombreArchivo' => $llegada
								);
								$i++;
								if ($i == $j) break;
							}
						}
					}

					$fecha = date('Y-m-d H:i:s');

					$query = $this->mod_precios->seleccionarPrecios($dataInput['idEdificio'], $dataInput['numDpto']);
					$query = $query->result();
					$dataObtenidaPrecios = array();
					$dataInputPrecios = array();
					$dataMezclaPrecios = array();
					for($i=1;$i<=$dataInput['capacidadPersonas'];$i++){
						/*Escenarios:
							- Se modificó la cantidad de habitaciones disponibles
								- Si la cantidad es mayor, entonces se inicializan
								a 0 los nuevos precios
								- Si la cantidad es menor, se pierden los precios
								predispuestos.
						*/

						if(isset($query[$i-1])){
							$dataObtenidaPrecios[$i] = $query[$i-1]->precio;
							$dataMezclaPrecios[$i] = $dataObtenidaPrecios[$i];
						}
						else{
							$dataObtenidaPrecios[$i]=0;
							$dataMezclaPrecios[$i] = 0;
						}
						if($this->input->post('precio_'.($i))){
							$dataInputPrecios[$i] = $this->input->post('precio_'.($i));
							$dataMezclaPrecios[$i] = $this->input->post('precio_'.($i));
						}
						else{
							$dataInputPrecios[$i] = 0;
						}
						// La query comienza de 0 a diferencia del ciclo sobre el que reside.
					}



					$dataDepartamento = $this->generarInput($dataInput,$dataMezclaPrecios);

					$preciosIguales = TRUE;
					for($i=1;$i<=$dataInput	['capacidadPersonas'];$i++){
                        if($this->input->post('precio_'.($i))){
                            if($dataObtenidaPrecios[$i] != $dataDepartamento['precio'][$i]['value']){
                                $preciosIguales = FALSE;
                                break;
                            }
                        }
					}

					foreach($this->mod_departamentos->seleccionarFotografiasDepartamento($dataObtenida['idEdificio'], $dataObtenida['numDpto'])->result() as $fila) {
						if ($this->input->post(str_replace(".", "_", "$fila->nombreArchivo"))) $fotografiasEliminadas[$fila->nombreArchivo] = $fila;
					}

					if ($dataInput == $dataObtenida && empty($fotografiasEliminadas) && empty($fotografiasDepartamento) && $preciosIguales==TRUE) {
						$dataDepartamento['datosIdenticos'] = TRUE;
					}
					else {
						$dataDepartamento['datosIdenticos'] = FALSE;

						if (!empty($fotografiasEliminadas)) {
							$this->eliminarFotografias($dataInput['idEdificio'], $dataInput['numDpto'], $fotografiasEliminadas);
							$dataDepartamento = $this->generarInput($dataInput,$dataMezclaPrecios);
							$dataDepartamento['datosIdenticos'] = FALSE;
						}

						if (!empty($fotografiasDepartamento)) {
							$this->mod_departamentos->ingresarImagenes($archivosDepartamento);
							$this->eliminarFotografiasSesion();
							$dataDepartamento = $this->generarInput($dataInput,$dataMezclaPrecios);
							$dataDepartamento['datosIdenticos'] = FALSE;
						}

						if ($dataInput['idEdificio'] != $dataObtenida['idEdificio'] || $dataInput['numDpto'] != $dataObtenida['numDpto']) {
							$dataDepartamento['ingresoCorreto'] = $this->mod_departamentos->ingresarDepartamento($dataInput);
							if ($dataDepartamento['ingresoCorreto']) {
								$this->mod_precios->editarPreciosDepartamento($dataObtenida['idEdificio'], $dataObtenida['numDpto'], $dataInput['idEdificio'], $dataInput['numDpto']);
								$this->mod_departamentos->editarFotografiasDepartamento($dataObtenida['idEdificio'], $dataObtenida['numDpto'], $dataInput['idEdificio'], $dataInput['numDpto']);
								$this->mod_reservas->editarDepartamentoReserva($dataObtenida['idEdificio'], $dataObtenida['numDpto'], $dataInput['idEdificio'], $dataInput['numDpto']);
								$this->mod_departamentos->eliminarDepartamento($dataObtenida['idEdificio'], $dataObtenida['numDpto']);
							}
						}
						else {
							$this->mod_departamentos->editarFotografiasDepartamento($dataObtenida['idEdificio'], $dataObtenida['numDpto'], $dataInput['idEdificio'], $dataInput['numDpto']);
							$dataDepartamento['ingresoCorreto'] = $this->mod_departamentos->editarDepartamento($idEdificio, $numDpto, $dataInput);
						}

						if($dataObtenida['capacidadPersonas'] > $dataInput['capacidadPersonas']){
							$this->mod_precios->eliminarPreciosDepartamento($dataInput['idEdificio'], $dataInput['numDpto'],$dataInput['capacidadPersonas']);
						}

						else if($dataObtenida['capacidadPersonas'] < $dataInput['capacidadPersonas']){
							$this->mod_precios->rellenarPreciosDepartamento($dataInput['idEdificio'], $dataInput['numDpto'],$dataObtenida['capacidadPersonas'],$dataInput['capacidadPersonas'],$fecha);
						}

						if(!$preciosIguales && $dataDepartamento['ingresoCorreto']){
							$fecha = date('Y-m-d H:i:s');

							$dataPrecios = array();
							$j=0;

							for($i=1;$i<=$dataInput['capacidadPersonas'];$i++){
								if($this->input->post('precio_'.($i))){
									if($dataObtenidaPrecios[$i] != $dataDepartamento['precio'][$i]['value'])
										$dataPrecios[$i] = array(
											'fecha'=>$fecha,
											'precio'=>$dataInputPrecios[$i]
										);

								}
							}

							$this->mod_precios->editarPrecios($dataInput['idEdificio'], $dataInput['numDpto'],$dataPrecios);
						}
					}
				}
				else {
					$query = $this->mod_precios->seleccionarPrecios($dataObtenida['idEdificio'], $dataObtenida['numDpto']);
					$query = $query->result();

					for($i=1;$i<=$dataObtenida['capacidadPersonas'];$i++){
						/*Escenarios:
							- Se modificó la cantidad de habitaciones disponibles
								- Si la cantidad es mayor, entonces se inicializan
								a 0 los nuevos precios
								- Si la cantidad es menor, se pierden los precios
								predispuestos.
						*/
						$dataInputPrecios[$i] = isset($query[$i-1])?($query[$i-1]->precio):0;
					}

					$dataDepartamento = $this->generarInput($dataObtenida,$dataInputPrecios);
					$dataDepartamento['datosIdenticos'] = FALSE;
				}

				$this->cargarNavSup();
				$this->load->view('editarDepartamentoContent', $dataDepartamento);
				$this->cargarNavInf();
			}
			else {
				$mensaje = '<div class="alert alert-warning alert-dismissable"><i class="fa fa-ban"></i>No se ha podido encontrar el departamento</div>';
				$this->listarDepartamentos($mensaje);
			}
		}
		else {
			redirect(base_url('index.php/departamentos/listarDepartamentos'));
		}
	}

	public function agregarDepartamento()
	{
		date_default_timezone_set('America/Santiago');
		$this->form_validation->set_error_delimiters('', '<br />');
		$this->form_validation->set_rules('idEdificio', 'Edificio', 'required|min_length[1]|max_length[11]|is_natural');
		$this->form_validation->set_rules('numDpto', 'Número de departamento', 'required|min_length[1]|max_length[11]|is_natural');
		$this->form_validation->set_rules('numPiso', 'Número de piso', 'required|min_length[1]|max_length[11]|integer');
		$this->form_validation->set_rules('orientacion', 'Orientación del edificio', 'required|min_length[1]|max_length[1]|is_natural');
		$this->form_validation->set_rules('cantDormitorios', 'Cantidad de dormitorios', 'required|min_length[1]|max_length[11]|is_natural_no_zero');
		$this->form_validation->set_rules('cantBanios', 'Cantidad de baños', 'required|min_length[1]|max_length[11]|is_natural_no_zero');
		$this->form_validation->set_rules('metrosConstruidos', 'Metros cuadrados construÃ­dos', 'required|min_length[1]|max_length[11]|is_natural_no_zero');
		$this->form_validation->set_rules('capacidadPersonas', 'Capacidad de personas', 'required|min_length[1]|max_length[11]|is_natural_no_zero');
		$this->form_validation->set_rules('descripcion', 'Descripción de departamento', 'required|min_length[5]|max_length[1000]');
		$dataInput = array(
			'idEdificio' => $this->input->post('idEdificio') ,
			'numDpto' => $this->input->post('numDpto') ,
			'numPiso' => $this->input->post('numPiso') ,
			'orientacion' => $this->input->post('orientacion') ,
			'cantDormitorios' => $this->input->post('cantDormitorios') ,
			'cantBanios' => $this->input->post('cantBanios') ,
			'metrosConstruidos' => $this->input->post('metrosConstruidos') ,
			'capacidadPersonas' => $this->input->post('capacidadPersonas') ,
			'descripcion' => $this->input->post('descripcion') ,
			'dispWIFI' => ($this->input->post('dispWIFI') ? 1 : 0) ,
			'dispCocina' => ($this->input->post('dispCocina') ? 1 : 0) ,
			'dispCable' => ($this->input->post('dispCable') ? 1 : 0) ,
			'dispRefrigerador' => ($this->input->post('dispRefrigerador') ? 1 : 0) ,
			'dispCalefactor' => ($this->input->post('dispCalefactor') ? 1 : 0) ,
			'dispMicroondas' => ($this->input->post('dispMicroondas') ? 1 : 0)
		);
		if ($this->form_validation->run() != FALSE) {
			$fotografiasDepartamento = $this->session->userdata('fotografiasDepartamento');
			$i = 0;
			if (is_array($fotografiasDepartamento)) {
				foreach($fotografiasDepartamento as $recuperada) {
					$recuperada = preg_replace('/[^(\x20-\x7F)]*/', '', $recuperada); // Reemplazo de nombre de archivo
					$recuperada = preg_replace('/\s+/', '', $recuperada);
					$llegada = $dataInput['idEdificio'] . '_' . $dataInput['numDpto'] . '_' . $i . '.' . substr(strrchr($recuperada, '.') , 1);
					rename((($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/temp/') . $recuperada) , (($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/departamentos/') . $llegada));
					$archivosDepartamento[$i] = array(
						'idEdificio' => $this->input->post('idEdificio') ,
						'numDpto' => $this->input->post('numDpto') ,
						'numFotografia' => $i,
						'nombreArchivo' => $llegada
					);
					$i++;
				}
			}

			$this->eliminarFotografiasSesion();
			$dataDepartamento = $this->generarInput($dataInput);
			if (empty($archivosDepartamento)) {
				$dataDepartamento['ingresoCorrecto'] = $this->mod_departamentos->ingresarDepartamento($dataInput);
				$dataDepartamento['ingresoCorrecto'] = $dataDepartamento['ingresoCorrecto'] && $this->mod_precios->definirPlantilla(
															$dataInput['idEdificio'],
															$dataInput['numDpto'],
															$dataInput['capacidadPersonas'],
															date('Y-m-d H:i:s')
															) ;

			}
			else {
				$dataDepartamento['ingresoCorrecto'] = $this->mod_departamentos->ingresarDepartamento($dataInput)
													  && $this->mod_departamentos->ingresarImagenes($archivosDepartamento)
													  && $this->mod_precios->definirPlantilla(
																$dataInput['idEdificio'],
																$dataInput['numDpto'],
																$dataInput['capacidadPersonas'],
																date('Y-m-d H:i:s')
																);
			}
		}
		else {
			$dataDepartamento = $this->generarInput($dataInput);
		}

		$this->cargarNavSup();
		$this->load->view('agregarDepartamentoContent', $dataDepartamento);
		$this->cargarNavInf();
	}

	function subirImagenesDepartamento()
	{
		if (!empty($_FILES) && is_array($_FILES['file']['tmp_name'])) {
			$this->session->set_userdata('fotografiasDepartamento', $_FILES['file']['name']);
			$i = 0;
			foreach($_FILES['file']['tmp_name'] as $valor) {
				$string = preg_replace('/[^(\x20-\x7F)]*/', '', $_FILES['file']['name'][$i]); // Reemplazo de nombre de archivo
				$string = preg_replace('/\s+/', '', $string);;
				move_uploaded_file($valor, (($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/temp/') . $string));
				$i++;
			}
		}
	}

	public

	function visualizarDepartamento($idEdificio = null, $numDpto = null)
	{
		if ($idEdificio != null && $numDpto != null) {
			$query = $this->mod_departamentos->seleccionarDepartamento($idEdificio, $numDpto);
			if ($query->num_rows() > 0) {
				foreach($query->result() as $row) {
					$dataDepartamento = array(
						'idEdificio' => $row->idEdificio,
						'numDpto' => $row->numDpto,
						'numPiso' => $row->numPiso,
						'orientacion' => $row->orientacion,
						'cantDormitorios' => $row->cantDormitorios,
						'cantBanios' => $row->cantBanios,
						'metrosConstruidos' => $row->metrosConstruidos,
						'capacidadPersonas' => $row->capacidadPersonas,
						'descripcion' => $row->descripcion,
						'dispWIFI' => $row->dispWIFI,
						'dispCocina' => $row->dispCocina,
						'dispCable' => $row->dispCable,
						'dispRefrigerador' => $row->dispRefrigerador,
						'dispCalefactor' => $row->dispCalefactor,
						'dispMicroondas' => $row->dispMicroondas
					);
				}

				$i = 0;
				foreach($this->mod_edificios->listarEdificiosDepartamento()->result() as $fila) {
					$dataDepartamento['edificios'][$fila->idEdificio] = $fila->nombreEdificio;
				}

				foreach($this->mod_departamentos->seleccionarFotografiasDepartamento($idEdificio, $numDpto)->result() as $fila) {
					$dataDepartamento['fotografias'][$fila->numFotografia] = $fila->nombreArchivo;
				}

				foreach($this->mod_precios->seleccionarPrecios($idEdificio, $numDpto)->result() as $fila) {
					$dataDepartamento['precios'][$fila->cantidadPersonas] = array('cantidadPersonas'=>$fila->cantidadPersonas,
																			  'precio' => $fila->precio,
																			  'fecha' => $fila->fecha
																			);
				}

				switch ($dataDepartamento['orientacion']) {
				case 0:
					$dataDepartamento['orientacion'] = 'Norte';
					break;

				case 1:
					$dataDepartamento['orientacion'] = 'Noroeste';
					break;

				case 2:
					$dataDepartamento['orientacion'] = 'Oeste';
					break;

				case 3:
					$dataDepartamento['orientacion'] = 'Suroeste';
					break;

				case 4:
					$dataDepartamento['orientacion'] = 'Sur';
					break;

				case 5:
					$dataDepartamento['orientacion'] = 'Sureste';
					break;

				case 6:
					$dataDepartamento['orientacion'] = 'Este';
					break;

				case 7:
					$dataDepartamento['orientacion'] = 'Noreste';
					break;
				}

                //$dataDepartamento[cantPersonas] = null;

				$this->cargarNavSup();
				$this->load->view('visualizarDepartamentoContent', $dataDepartamento);
				$this->cargarNavInf();
			}
			else {
				$mensaje = '<div class="alert alert-warning alert-dismissable"><i class="fa fa-ban"></i>No se ha podido encontrar el departamento</div>';
				$this->listarDepartamentos($mensaje);
			}
		}
		else {
			redirect(base_url('index.php/departamentos/listarDepartamentos'));
		}
	}

	public

	function listarDepartamentos($mensaje = '')
	{
		$dataDepartamento = array();
		$query = $this->mod_departamentos->listarDepartamentos();
		foreach($query->result() as $row) {
			$dataDepartamento['departamentos'][$row->idEdificio][$row->numDpto] = array(
				'idEdificio' => $row->idEdificio,
				'numDpto' => $row->numDpto,
				'cantDormitorios' => $row->cantDormitorios,
				'metrosConstruidos' => $row->metrosConstruidos,
				'capacidadPersonas' => $row->capacidadPersonas
			);
		}

		foreach($this->mod_ciudades->listarCiudades()->result() as $fila) {
			$ciudades[$fila->id] = $fila->nombre;
		}

		foreach($this->mod_edificios->listarEdificiosDepartamento()->result() as $fila) {
			$dataDepartamento['edificios'][$fila->idEdificio] = array('nombreEdificio' => $fila->nombreEdificio,
																	  'ciudad' => $ciudades[$fila->ciudadEdificio]
																	);
		}

		$dataDepartamento['mensaje'] = $mensaje;
		$this->cargarNavSup();
		$this->load->view('listarDepartamentosContent', $dataDepartamento);
		$this->cargarNavInf();
	}

	function eliminarDepartamento($idEdificio = null, $numDpto = null)
	{
		if ($idEdificio != null && $numDpto != null) {
			if ($this->buscarDepartamento($idEdificio, $numDpto)) {
				$this->eliminarFotografias($idEdificio, $numDpto);
				if ($this->mod_departamentos->eliminarDepartamento($idEdificio, $numDpto)) {
					$mensaje = '<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i>Departamento eliminado correctamente</div>';
				}
				else {
					$mensaje = '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>No se ha podido eliminar el departamento</div>';
				}
			}
			else {
				$mensaje = '<div class="alert alert-warning alert-dismissable"><i class="fa fa-ban"></i>No se ha podido encontrar el departamento</div>';
			}

			$this->listarDepartamentos($mensaje);
		}
		else {
			redirect(base_url('index.php/departamentos/listarDepartamentos'));
		}
	}

	private
	function eliminarFotografiasSesion()
	{
		return $this->session->set_userdata('fotografiasDepartamento', array());
	}

	private
	function eliminarFotografias($idEdificio, $numDpto, $fotografiasEliminadas = null)
	{
		if ($fotografiasEliminadas == null) $fotografiasEliminadas = $this->mod_departamentos->eliminarFotografiasDepartamento($idEdificio, $numDpto, $fotografiasEliminadas);
		else $this->mod_departamentos->eliminarFotografiasDepartamento($idEdificio, $numDpto, $fotografiasEliminadas);
		if (!empty($fotografiasEliminadas)) {
			foreach($fotografiasEliminadas as $fotografia) {
				unlink($_SERVER['DOCUMENT_ROOT'] . '/departarriendo/uploads/departamentos/' . ($fotografia->nombreArchivo));
			}
		}

		return true;
	}

	private
	function buscarDepartamento($idEdificio = null, $numDpto = null)
	{
		if ($idEdificio != null && $numDpto != null) {
			$query = $this->mod_departamentos->seleccionarDepartamento($idEdificio, $numDpto);
			if ($query->num_rows() > 0) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			redirect(base_url('index.php/departamentos/listarDepartamentos'));
		}
	}

    public function inicializar(){
        date_default_timezone_set('America/Santiago');
        foreach($this->mod_departamentos->listarDepartamentos()->result() as $fila){
            $this->mod_precios->definirPlantilla(
															$fila->idEdificio,
															$fila->numDpto,
															$fila->capacidadPersonas,
															date('Y-m-d H:i:s')
															) ;
        }

    }

	public function consultarDependencias(){
		$idEdificio = $this->input->post('idEdificio');
		$numDpto = $this->input->post('numDpto');
		$tipo = $this->input->post('tipo');
		$data= array();
		if($tipo=='Ocu'){
			$i=0;
			$query = $this->mod_reservas->seleccionarOcupacionesDepartamento($idEdificio,$numDpto);
			foreach($query->result() as $fila){
				$data[$i] = $fila;
				$i++;
			}
		}



		echo json_encode($data);
	}
}