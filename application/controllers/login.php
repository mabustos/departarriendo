<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
  private $caso = 0;

  function __construct()
	{
   	  parent::__construct();
   	  $this->load->model('mod_login');
      $this->load->helper('form');
      $this->load->library('form_validation');

 	}

	public function index(){
    if($this->input->post('email')!=NULL || $this->input->post('password')!=NULL){
      if($this->validar_ingreso()){
        $this->set_session();
        $this->mod_login->registrarIngreso($this->session->all_userdata());
        redirect(base_url('index.php/dashboard'));

      }
      else{
        $this->cargar_login(TRUE);
      }
    }
    else{
      $this->cargar_login();
    }
	}

  function validar_ingreso(){
    $usuario     = $this->input->post('email');
    $contrasenia = hash('sha512',$this->input->post('password'));
    $pass = $this->mod_login->selectUser($usuario);

    if($pass->num_rows()>0){
      $data = $pass->result_array();
      if($data[0]['contrasenia']==$contrasenia){
        return true;
      }
      else{
        return false;
      }

    }
    else{
      return false;
    }
  }


  function cerrar_sesion(){
      $this->session->sess_destroy();
      $this->session->set_userdata( array(
            'isLoggedIn'=>false
        )
    );
      redirect(base_url('index.php/login'));
  }

  function cargar_login($error=FALSE){
      $this->load->helper('form');
      $this->load->view('login_header');
      if ($error) $this->load->view('login_error');
      $this->load->view('login_form');
      $this->load->view('login_footer');
  }

  public function restablecerPassword($codigo){
      $original = MD5('sdjfhasdufaeufu32u24up423p12u23pu12eb1');
      if($original==$codigo){
          $nuevaPass = $original.'sad'.$this->caso;
          $this->mod_login->cambiarContrasenia(hash('sha512',$nuevaPass));
          echo 'Su nueva contraseña de acceso es :'.$nuevaPass;
      }
      else{
          echo 'Lo sentimos. Su código de restauración no es válido';
      }
  }

  public function recuperar_contrasenia() {
   $this->load->library("email");

        //configuracion para gmail
        $configGmail = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_port' => 465,
            'smtp_user' => 'sistema.departarriendo@gmail.com',
            'smtp_pass' => 'departarriendo',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        );


        $this->caso=$this->caso + 1;
        $codificacion = MD5('sdjfhasdufaeufu32u24up423p12u23pu12eb1')/*.$this->caso*/;
        $enlace = base_url('index.php/login/restablecerPassword').'/'.$codificacion;
        //cargamos la configuración para enviar con gmail
        $this->email->initialize($configGmail);
        $correoDestino = $this->mod_login->seleccionarCorreo()->result()[0]->correoElectronico1;

        $this->email->from('sistema.departarriendo@gmail.com');
        $this->email->to("whitedogwar@gmail.com");
        $this->email->subject('SAD - Recuperación de contraseña');
        $mensaje = "<h2>Sistema de arriendo de departamentos (SAD)</h2></br>";
        $mensaje = $mensaje."Se ha realizado una solicitud de restablecimiento de contraseña.</br>";
        $mensaje = $mensaje."Si Ud. no ha solicitado esta acción, por favor, ignore este mensaje.</br>";
        $mensaje = $mensaje."Si Ud. pincha el siguiente enlace, le enviaremos una nueva contraseña.</br>";
        $mensaje = $mensaje."<a href=".$enlace.">Enlace de restauración</a>";
        $this->email->message($mensaje);
        $this->email->send();
        //con esto podemos ver el resultado


}

  function set_session() {
    // session->set_userdata is a CodeIgniter function that
    // stores data in a cookie in the user's browser.  Some of the values are built in
    // to CodeIgniter, others are added (like the IP address).  See CodeIgniter's documentation for details.
    date_default_timezone_set('America/Santiago');
    $this->session->set_userdata( array(
            'isLoggedIn'=>true,
            'horaFecha'=>date('Y-m-d H:i:s'),
            'fotografiasDepartamento'=>array(),
            'fotografiasDepartamentoCant'=>0
        )
    );
  }

}
