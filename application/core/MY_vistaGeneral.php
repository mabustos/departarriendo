<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_vistaGeneral extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('mod_login');
        date_default_timezone_set('America/Santiago');
        if (!$this->session->userdata('isLoggedIn')) {
            redirect(base_url('index.php/login/cargar_login'));
        }
    }
    public function confirmacion_contrasenia($str)
    {
        $str = hash('sha512',$str);
        if (!$this->mod_cuenta->comprobarContrasenia($str)) {
            $this->form_validation->set_message('confirmacion_contrasenia', 'La contraseña ingresada es incorrecta.');
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    public function cargarNavSup()
    {
        $query = $this->mod_login->listarIngresos();
        $i=0;
        foreach ($query->result() as $row) {
            $data['registroAccesos'][$i] = array(
                'sessionId' => $row->sessionId,
                'fecha' => $row->fecha,
                'ip' => $row->ip
            );
            $i++;
        }
        
        if ($this->input->post('submitDatosContacto')) {
            $this->form_validation->set_error_delimiters('', '<br />');
            $this->form_validation->set_rules('nombreUsuario', 'Nombre de usuario', 'required|min_length[5]|max_length[40]');
            $this->form_validation->set_rules('correoElectronico', 'Correo electrónico', 'required|min_length[5]|max_length[100]|valid_email');
            $this->form_validation->set_rules('correoElectronicoAlt', 'Correo electrónico alternativo', 'required|min_length[5]|max_length[100]|valid_email');
            $this->form_validation->set_rules('password', 'Contraseña', 'required|min_length[5]|max_length[100]|callback_confirmacion_contrasenia');
        } else if ($this->input->post('submitContrasenia')) {
            $this->form_validation->set_rules('cambioContrasenia_actual', 'Contraseña actual', 'required|min_length[5]|max_length[100]|callback_confirmacion_contrasenia');
            $this->form_validation->set_rules('cambioContrasenia_nuevaContrasenia', 'Nueva contraseña', 'required|min_length[5]|max_length[100]|matches[cambioContrasenia_nuevaContraseniaRep]');
        }
        
        $data['nombreUsuario']                         = array(
            'name' => 'nombreUsuario',
            'id' => 'nombreUsuario',
            'value' => set_value('nombreUsuario', ''),
            'maxlength' => '40',
            'class' => 'form-control',
            'placeholder' => 'Ingrese un nombre de usuario'
        );
        $data['correoElectronico']                     = array(
            'name' => 'correoElectronico',
            'id' => 'correoElectronico',
            'value' => set_value('correoElectronico', ''),
            'maxlength' => '100',
            'class' => 'form-control',
            'placeholder' => 'Ingrese un correo electrónico válido',
            'type' => 'email'
        );
        $data['correoElectronicoAlt']                  = array(
            'name' => 'correoElectronicoAlt',
            'id' => 'correoElectronicoAlt',
            'value' => set_value('correoElectronicoAlt', ''),
            'maxlength' => '100',
            'class' => 'form-control',
            'placeholder' => 'Ingrese un correo electrónico válido',
            'type' => 'email'
        );
        $data['password']                              = array(
            'name' => 'password',
            'id' => 'password',
            'value' => set_value('password', ''),
            'maxlength' => '40',
            'class' => 'form-control',
            'placeholder' => 'Ingrese su contraseña',
            'type' => 'password'
        );
        $data['cambioContrasenia_actual']              = array(
            'name' => 'cambioContrasenia_actual',
            'id' => 'cambioContrasenia_actual',
            'value' => set_value('cambioContrasenia_actual', ''),
            'maxlength' => '40',
            'class' => 'form-control',
            'placeholder' => 'Ingrese su contraseña',
            'type' => 'password'
        );
        $data['cambioContrasenia_nuevaContrasenia']    = array(
            'name' => 'cambioContrasenia_nuevaContrasenia',
            'id' => 'cambioContrasenia_nuevaContrasenia',
            'value' => set_value('cambioContrasenia_nuevaContrasenia', ''),
            'maxlength' => '40',
            'class' => 'form-control',
            'placeholder' => 'Ingrese su nueva contraseña',
            'type' => 'password'
        );
        $data['cambioContrasenia_nuevaContraseniaRep'] = array(
            'name' => 'cambioContrasenia_nuevaContraseniaRep',
            'id' => 'cambioContrasenia_nuevaContraseniaRep',
            'value' => set_value('cambioContrasenia_nuevaContraseniaRep', ''),
            'maxlength' => '40',
            'class' => 'form-control',
            'placeholder' => 'Repita su nueva contraseña',
            'type' => 'password'
        );
        if ($this->form_validation->run() != FALSE) {
            if ($this->input->post('submitDatosContacto')) {
                $dataInput = array(
                    'usuario' => $this->input->post('nombreUsuario'),
                    'contrasenia' => hash('sha512',$this->input->post('password')),
                    'correoElectronico1' => $this->input->post('correoElectronico'),
                    'correoElectronico2' => $this->input->post('correoElectronicoAlt')
                );
                if ($this->mod_cuenta->modificarCuenta($dataInput, $this->mod_cuenta->obtenerCuenta())) {
                    redirect(base_url('index.php/login/cerrar_sesion'));
                } else {
                    
                }
            } else if ($this->input->post('submitContrasenia')) {
                $contraseniaActual = hash('sha512',$this->input->post('cambioContrasenia_actual'));
                $dataInput         = array(
                    'usuario' => $this->mod_cuenta->obtenerCuenta(),
                    'contrasenia' => hash('sha512',$this->input->post('cambioContrasenia_nuevaContrasenia'))
                );
                if ($this->mod_cuenta->modificarCuenta($dataInput, $this->mod_cuenta->obtenerCuenta())) {
                    redirect(base_url('index.php/login/cerrar_sesion'));
                } else {
                    
                }
            }
        }
        $this->load->view('dash_header', $data);
        $this->load->view('dash_menuIzq');
    }
    public function cargarNavInf()
    {
        $this->load->view('dash_footer');
    }
    
}