<aside class="right-side">
    <!-- Content Header (Page header) -->

    <section class="content-header">
        <h1>Agregar Departamento</h1>
    </section>

    <section class="content-header">
        <?php echo form_open( 'departamentos/agregarDepartamento', 'class="form" id="agregarDepartamentoForm"');?>
        <button type="submit" class="btn btn-primary" id="submitAgregarDepartamento">Ingresar</button>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->

                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Datos de departamento</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    <?php if(validation_errors()){
                                echo '<div class="alert alert-warning alert-dismissable">
                                        <i class="fa fa-warning"></i>'.validation_errors(). '</div>';
                            }
                            else{
                                if(isset($ingresoCorrecto)){
                                        if($ingresoCorrecto){
                                            echo '<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i>Departamento ingresado correctamente</div>';
                                        }
                                        else{
                                            echo '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>No se ha podido agregar el departamento</div>';
                                        }
                                }
                            } ?>


                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#departamentoPrincipal" data-toggle="tab">Datos principales</a>
                        </li>
                        <li><a href="#departamentoInterior" data-toggle="tab">Datos del interior</a>
                        </li>
                        <li><a href="#departamentoAdicionales" data-toggle="tab">Datos adicionales</a>
                        </li>
                        <li><a href="#departamentoMultimedia" data-toggle="tab">Multimedia</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="departamentoPrincipal">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Nombre del edificio</label>

                                    <?php echo form_dropdown( 'idEdificio', $nombreEdificio, $idEdificioSeleccionado, 'id="idEdificio" class="form-control"'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Nº de departamento</label>
                                    <?php echo form_input($numDpto); ?>
                                </div>
                                <div class="form-group">
                                    <label>Nº de piso</label>
                                    <?php echo form_dropdown( 'numPiso', $pisosEdificio, $numPisoSeleccionado, 'id="numPiso" class="form-control"'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Orientación del departamento</label>
                                    <?php echo form_dropdown( 'orientacion', $orientacion, $orientacionSeleccionado, 'class="form-control"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="departamentoInterior">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Nº de habitaciones</label>
                                    <?php echo form_input($cantDormitorios); ?>
                                </div>
                                <div class="form-group">
                                    <label>Nº de baños</label>
                                    <?php echo form_input($cantBanios); ?>
                                </div>
                                <div class="form-group">
                                    <label>M^2 Construidos</label>
                                    <?php echo form_input($metrosConstruidos); ?>
                                </div>
                                <div class="form-group">
                                    <label>Capacidad de personas</label>
                                    <?php echo form_input($capacidadPersonas); ?>
                                </div>
                                <div class="form-group">
                                    <label>Descripción</label>
                                    <?php echo form_input($descripcion); ?>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="departamentoAdicionales">
                            <div class="box-body">
                                <div class="form-group">
                                    <?php echo form_checkbox($dispWIFI); ?>
                                    <label for="dispWIFI">WIFI</label>
                                </div>
                                <div class="form-group">
                                    <?php echo form_checkbox($dispCocina); ?>
                                    <label for="dispCocina">Cocina</label>
                                </div>
                                <div class="form-group">
                                    <?php echo form_checkbox($dispCable); ?>
                                    <label for="dispCable">Cable</label>
                                </div>
                                <div class="form-group">
                                    <?php echo form_checkbox($dispRefrigerador); ?>
                                    <label for="dispRefrigerador">Refrigerador</label>
                                </div>
                                <div class="form-group">
                                    <?php echo form_checkbox($dispCalefactor); ?>
                                    <label for="dispCalefactor">Calefactor</label>
                                </div>
                                <div class="form-group">
                                    <?php echo form_checkbox($dispMicroondas); ?>
                                    <label for="dispMicroondas">Microondas</label>
                                </div>

                            </div>
                      </div>



                        <!-- FIN DE FORMULARIO (SIN IMAGENES)!-->
                        <div class="tab-pane" id="departamentoMultimedia">
                            <div class="box-body">
                                <div class="box-body">
                                    <div class="callout callout-info">
                                        <h4>Información</h4>
                                        <p>Como máximo se permiten <strong>15</strong> fotografías por departamento.</p>
                                    </div>
                                    <div id="imagenesDepartamento" class="dropzone">


                                    </div>



                            </div>
                        </div>
                </div> <!-- /.TAB CONTENT -->
</form>
                <!-- /.box-header -->
                <!-- textarea -->
                <!-- input states -->


            </div>
            <!-- /.box-body -->
            <!-- input states -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</aside>

<script>

$(document).ready(function() {
  var myDropzone = new Dropzone("div#imagenesDepartamento", { url: "<?php echo base_url('index.php/departamentos/subirImagenesDepartamento'); ?>"});
  var banderaSubida = false;
  var archivosDisponibles = false;
  jQuery("#agregarDepartamentoForm").submit(function(e) {
      if(archivosDisponibles){
          if(!banderaSubida){
             e.preventDefault();
             e.stopPropagation();
             myDropzone.processQueue();
             banderaSubida=true;
          }
          else{
              return true;
          }
      }
      else{
          return true;
      }
    });
  myDropzone.on("queuecomplete", function() {
      //return true;
      $("#agregarDepartamentoForm").submit();
    });

  myDropzone.on("addedfile", function(file) {
      archivosDisponibles = true;
  });

  // Primera carga de la página. Enumera cantidad de pisos del elemento por defecto.
  document.getElementById("numPiso").options.length = 0;
  var idEdificio = document.getElementById("idEdificio");
  var valorIdEdificio = idEdificio.options[idEdificio.selectedIndex].value;
  switch (valorIdEdificio) {
        <?php
        foreach (array_keys( $pisosEdificio ) as $index=>$key) {
            echo 'case "'.$key.'":'.PHP_EOL;
            for($i=1;$i<=$pisosEdificio[$key];$i=$i+1){
                echo 'var option = document.createElement("option");
    option.text = '.$i.';
    option.value = '.$i.';'.PHP_EOL;
                echo '
                try {
        numPiso.add(option, null); //Standard
    }catch(error) {
        numPiso.add(option); // IE only
    }
                ';
            }

            echo 'break;'.PHP_EOL;
        }
        ?>
    }
    $("select#idEdificio").val(<?php echo $idEdificioSeleccionado; ?>);
    $("select#numPiso").val(<?php echo $numPisoSeleccionado; ?>);

   // Accionado al momento de seleccionar el combobox idEdificio.
   //Enumera cantidad de pisos del elemento seleccionado.
  $("#idEdificio").change(function() {

    var idEdificio = document.getElementById("idEdificio");
    var numPiso = document.getElementById("numPiso");
    var valorIdEdificio = idEdificio.options[idEdificio.selectedIndex].value;
    numPiso.options.length = 0;

    switch (valorIdEdificio) {
        <?php
        foreach (array_keys( $pisosEdificio ) as $index=>$key) {
            echo 'case "'.$key.'":'.PHP_EOL;
            for($i=1;$i<=$pisosEdificio[$key];$i=$i+1){
                echo 'var option = document.createElement("option");
    option.text = '.$i.';
    option.value = '.$i.';'.PHP_EOL;
                echo '
                try {
        numPiso.add(option, null); //Standard
    }catch(error) {
        numPiso.add(option); // IE only
    }
                ';
            }

            echo 'break;'.PHP_EOL;
        }
        ?>
    }


  });

});
</script>
