<aside class="right-side">
    <!-- Content Header (Page header) -->

    <section class="content-header">
        <h1>Calendario</h1>
    </section>

    <section class="content-header">
      <div class="row">
                        <!-- /.col -->
                        <div class="col-md-12">
                            <a href="<?php echo base_url('index.php/reservaciones/agregarReserva'); ?>" target="_blank" ><button type="button" class="btn btn-primary btn-lg" style="width:100%;">Agregar reserva</button></a>
                            <div class="box box-primary">
                                <div class="box-body no-padding">

                                   <!-- INICIO CALENDARIO -->
                                   <div id='calendar'>


                                   </div>
                                   <!-- FIN CALENDARIO -->
                                </div><!-- /.box-body -->
                            </div><!-- /. box -->
                        </div><!-- /.col -->
                    </div>
    </section>

    <script type="text/javascript">
                $(function() {
                    $('#calendar').fullCalendar({
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,agendaWeek,agendaDay'
                        },
                        events: [
                            <?php
                                foreach($ocupaciones as $ocupacion){
                                    echo '{ ';
                                    echo 'title   : "'.$ocupacion['nombreEdificio'].' #'.$ocupacion['numDpto'].'",';
                                    echo 'start   : "'.$ocupacion['fechaLlegada'].'",';
                                    echo 'end     : "'.$ocupacion['fechaPartida'].'",';
                                    echo 'color   : "'.$ocupacion['colorEstado'].'",';
                                    echo 'url   : "'.$ocupacion['enlace'].'",';
                                    echo 'allDay: false';
                                    echo '},';
                                }
                            ?>
                        ],
                        eventClick: function(event) {
                            if (event.url) {
                                window.open(event.url);
                                return false;
                            }
                        },
                        defaultView: 'month',
                        selectable: true,
                        eventLimit: true // allow "more" link when too many events

                    });


                });
    </script>
