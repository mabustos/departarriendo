<aside class="right-side">
    <!-- Content Header (Page header) -->

    <section class="content-header">
        <h1>Editar cliente <?php echo $nombreCliente['value'].' '.$apellidoCliente['value']; ?></h1>
    </section>

    <section class="content-header">
        <?php echo form_open( 'clientes/editarCliente/'.$CIF['value'], 'id="editarClienteForm" class="form"');?>
        <button id="guardarCambios" type="submit" class="btn btn-primary">Guardar cambios</button>
    </section>
    <!-- Main content -->
   <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Datos Cliente</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?php
                        if (validation_errors()) {
                            echo '<div class="alert alert-warning alert-dismissable">
                                                                <i class="fa fa-warning"></i>' . validation_errors() . '</div>';
                        } else {
                            if ($datosIdenticos == TRUE) {
                                echo '<div class="alert alert-warning alert-dismissable">
                                                                <i class="fa fa-warning"></i>Los datos son idénticos</div>';
                            } else {
                                if (isset($ingresoCorreto)) { {
                                        if ($ingresoCorreto) {
                                            echo '<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i>Cliente editado  correctamente</div>';
                                        } else {
                                            echo '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>No se ha podido editar el cliente</div>';
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#clienteDatosPersonales" data-toggle="tab">Datos personales</a>
                        </li>
                        <li><a href="#clienteDatosContacto" data-toggle="tab">Datos de contacto</a>
                        </li>
                        <li><a href="#clienteMultimedia" data-toggle="tab">Multimedia</a>
                        </li>
                        <li><a href="#clienteAgregarMultimedia" data-toggle="tab">Agregar Multimedia</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <form role="form">
                            <div class="tab-pane active" id="clienteDatosPersonales">
                                <div class="box-body">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Rut o Pasaporte</label>
                                        <?php echo form_input($CIF); ?>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-2">

                                    <div class="form-group">
                                        <label>Tratamiento</label>
                                        <?php echo form_dropdown( 'tratamientoCliente', $tratamientoCliente, '', 'id="tratamientoCliente" class="form-control"'); ?>
                                    </div>

                                    </div>
                                    <div class="col-md-5">

                                    <div class="form-group">
                                        <label>Nombres</label>
                                        <?php echo form_input($nombreCliente); ?>
                                    </div>

                                    </div>
                                    <div class="col-md-5">

                                    <div class="form-group">
                                        <label>Apellidos</label>
                                        <?php echo form_input($apellidoCliente); ?>
                                    </div>

                                </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Sexo</label>
                                        <?php echo form_dropdown( 'sexoCliente', $sexoCliente, (isset($sexoClienteSeleccionado))?$sexoClienteSeleccionado:'', 'id="sexoCliente" class="form-control"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Fecha de nacimiento</label>
                                        <?php echo form_input($fechaNacCliente); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Nacionalidad</label>
                                        <?php echo form_dropdown( 'nacionalidad', $nacionalidad, (isset($nacionalidadSeleccionado))?$nacionalidadSeleccionado:'', 'id="nacionalidad" class="form-control"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Profesión</label>
                                        <?php echo form_input($profesion); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="clienteDatosContacto">
                                <div class="box-body">
                                    <div class="row">
                                    <div class="col-md-6">

                                    <div class="form-group">
                                        <label>Teléfono</label>
                                        <?php echo form_input($telefonoContacto); ?>
                                    </div>

                                    </div>

                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Teléfono alternativo</label>
                                        <?php echo form_input($telefonoContactoAlt); ?>
                                    </div>
                                    </div>

                                    </div>

                                    <div class="row">
                                    <div class="col-md-6">

                                    <div class="form-group">
                                        <label>E-mail</label>
                                        <?php echo form_input($email); ?>
                                    </div>

                                </div>
                                    <div class="col-md-6">

                                    <div class="form-group">
                                        <label>E-mail alternativo</label>
                                        <?php echo form_input($emailAlt); ?>
                                    </div>

                                </div>
                            </div>

<div class="row">
<div class="col-md-6">
                                    <div class="form-group">
                                        <label>Skype</label>
                                        <?php echo form_input($skypeCliente); ?>
                                    </div>
</div>
<div class="col-md-6">
                                    <div class="form-group">
                                        <label>Whatsapp</label>
                                        <?php echo form_input($whatsappCliente); ?>
                                    </div>
</div>
</div>
                                    <div class="form-group">
                                        <label>Contacto</label>
                                        <?php echo form_dropdown( 'contacto', $contacto, (isset($contactoSeleccionado))?$contactoSeleccionado:'', 'id="contacto" class="form-control"'); ?>
                                    </div>
<div class="row">
<div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tarjeta de crédito</label>
                                        <?php echo form_dropdown( 'tipoTarjetaCliente', $tipoTarjetaCliente, (isset($tipoTarjetaSeleccionado))?tipoTarjetaSeleccionado:'', 'id="tipoTarjetaCliente" class="form-control"'); ?>
                                    </div>
</div>
<div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nº Tarjeta de crédito</label>
                                        <?php echo form_input($numTarjetaCliente); ?>
                                    </div>

</div>
</div>
                                </div>
                            </div>
                            <!-- FIN DE FORMULARIO (SIN IMAGENES)!-->
                            <div class="tab-pane" id="clienteMultimedia">
                            <div class="box-body">
                                <!-- INICIO TAB 2 -->

                                <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <td>#</td>
                                            <td>Fotografía</td>
                                            <td>Estado</td>
                                        </tr>
                                    </thead>
                                    <?php
                                    if(!empty($fotografias)){
                                        $i=0;
                                        foreach($fotografias as $item){
                                            echo '<tr>';
                                            echo '<td>';
                                            echo ($i+1);
                                            echo '</td>';
                                            echo '<td>';
                                            echo '<a href="'.base_url('uploads/clientes/')."/".$item.'" data-lightbox="fotografiasCliente" data-title="Fotografía #'.($i+1).' del cliente" >';
            echo "<img src='".base_url('uploads/clientes/')."/".$item."' class='img-responsive-fix'></a>";
                                            echo '</td>';
                                            echo '<td>';
                                            echo '<input type="checkbox" value="aceptado" name="'.$item.'" id="'.$item.'" onclick="actualizarRestante(this);">';
                                            echo '<label for="'.$item.'">¿Eliminar?</label>';
                                            echo '</td>';
                                            echo '</tr>';
                                            $i++;
                                        }
                                    }

                                    ?>
                                    </form>
  </table>
</div>
                                <!-- FIN TAB 2 -->
                            </div>
                        </div>
                    <div class="tab-pane" id="clienteAgregarMultimedia">
                            <div class="box-body">
                                <div class="callout callout-info">
                                        <h4>Información</h4>
                                        <p>Como máximo se permiten <strong>15</strong> fotografías por cliente. <em>Restan (<span id="cantidadRestante"></span>) fotografías</em></p>
                                    </div>
                                    <div id="imagenesCliente" class="dropzone">

                                    </div>
                            </div>
                        </div>
                    </div>
                            </form>
                            <!-- /.box-header -->
                            <!-- textarea -->
                            <!-- input states -->

                    </div>
                    <!-- /.box-body -->
                    <!-- input states -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</aside>


<script>
    var maximoFotografias = 15;
  var cantidadRestante = <?php if(isset($fotografias)) echo (15-count($fotografias));
                               else echo 15?>; // Cantidad restante de fotografías
  $("#cantidadRestante").html(cantidadRestante);
$(document).ready(function() {

  var myDropzone = new Dropzone("div#imagenesCliente", { url: "<?php echo base_url('index.php/clientes/subirImagenesCliente'); ?>"});
  var banderaSubida = false;
  var archivosDisponibles = false;



  $("#tratamientoCliente").val(<?php echo $tratamientoClienteSeleccionado; ?>);
  $("#sexoCliente").val(<?php echo $sexoSeleccionado; ?>);
  $("#nacionalidad").val(<?php echo $nacionalidadSeleccionada; ?>);
  $("#contacto").val(<?php echo $contactoSeleccionado; ?>);
  $("#tipoTarjetaCliente").val(<?php echo $tarjetaCreditoSeleccionado; ?>);

  jQuery("#editarClienteForm").submit(function(e) {
      if(archivosDisponibles){
          if(!banderaSubida){
             $('guardarCambios').prop('disabled', true);
             e.preventDefault();
             e.stopPropagation();
             myDropzone.processQueue();
             banderaSubida=true;
          }
          else{
              return true;
          }
      }
      else{
          return true;
      }
    });
  myDropzone.on("queuecomplete", function() {
      $("#editarClienteForm").submit();
    });

  myDropzone.on("addedfile", function(file) {
      archivosDisponibles = true;
  });

});

    function actualizarRestante(cb) {
      if(cb.checked) cantidadRestante+=1;
      else cantidadRestante-=1;
      $("#cantidadRestante").html(cantidadRestante);
  };
</script>



<script>
            $(function() {
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                //Money Euro
                $("[data-mask]").inputmask();

                //Date range picker
                $('#reservation').daterangepicker();
                //Date range picker with time picker
                $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                        {
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                                'Last 7 Days': [moment().subtract('days', 6), moment()],
                                'Last 30 Days': [moment().subtract('days', 29), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                            },
                            startDate: moment().subtract('days', 29),
                            endDate: moment()
                        },
                function(start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
                );


                //Timepicker
                $(".timepicker").timepicker({
                    showInputs: false
                });
            });
        </script>
