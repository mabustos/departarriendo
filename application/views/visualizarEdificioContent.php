<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="row">
      </div>
        <h1>Edificio <?php echo $nombreEdificio; ?></h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Datos del edificio</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#edificioDatosEdificio" data-toggle="tab">Edificio</a>
                        </li>
                        <li><a href="#edificioDatosAdministracion" data-toggle="tab">Administración</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="edificioDatosEdificio">
                            <div class="box-body">
                                <!-- INICIO TAB 1 -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover">
                                        <tr>
                                            <td class="columna-valor">ID</td>
                                            <td><?php echo $idEdificio; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Nombre</td>
                                            <td><?php echo $nombreEdificio; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Cantidad de pisos</td>
                                            <td><?php echo $cantidadPisos; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Dirección</td>
                                            <td><?php echo $direccionEdificio; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Número</td>
                                            <td><?php echo $numeroEdificio; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Ciudad</td>
                                            <td><?php echo $ciudadEdificio; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Descripción</td>
                                            <td><?php echo $descripcionEdificio; ?></td>
                                        </tr>
                                    </table>
                                </div><!-- /.box-body -->
                                <!-- FIN TAB 1 -->
                            </div>
                        </div>
                        <div class="tab-pane" id="edificioDatosAdministracion">
                            <div class="box-body">
                                <!-- INICIO TAB 2 -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover">
                                        <tr>
                                            <td class="columna-valor">Administrador</td>
                                            <td><?php echo $nombreAdministrador; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Email administrador</td>
                                            <td><?php echo $emailAdministrador; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Teléfono administración</td>
                                            <td><?php echo $telefonoAdministracion; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Teléfono consejería</td>
                                            <td><?php echo $telefonoConsejeria; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Datos bancarios</td>
                                            <td><?php echo $datosBancarios; ?></td>
                                        </tr>
                                    </table>
                                </div><!-- /.box-body -->
                                <!-- FIN TAB 2 -->
                            </div>
                         </div>
                    </div>
                    
                    <!-- /.box-body -->
                    <!-- input states -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
        <!-- /.row -->
    </section>
    <!-- /.content -->
</aside>