
        <!-- jQuery UI 1.10.3 -->
        <script src="<?php echo base_url('template/js/jquery-ui-1.10.3.min.js'); ?>" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="<?php echo base_url('template/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <!-- Morris.js charts -->
        <script src="<?php echo base_url('template/js/plugins/morris/morris.min.js'); ?>" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="<?php echo base_url('template/js/plugins/sparkline/jquery.sparkline.min.js'); ?>" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="<?php echo base_url('template/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('template/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'); ?>" type="text/javascript"></script>
        <!-- fullCalendar -->
        <script src="<?php echo base_url('template/js/plugins/fullcalendar/fullcalendar.min.js'); ?>" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="<?php echo base_url('template/js/plugins/jqueryKnob/jquery.knob.js'); ?>" type="text/javascript"></script>
        <!-- jqueryui -->
        <script src="<?php echo base_url('template/js/plugins/jqueryui/jqueryui112.js'); ?>" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="<?php echo base_url('template/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'); ?>" type="text/javascript"></script>

        <!-- holder -->
        <script src="<?php echo base_url('template/js/holder.js'); ?>" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url('template/js/AdminLTE/app.js'); ?>" type="text/javascript"></script>

<!-- DATA TABES SCRIPT -->
        <script src="<?php echo base_url('template/js/plugins/datatables/jquery.dataTables.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('template/js/plugins/datatables/dataTables.bootstrap.js'); ?>" type="text/javascript"></script>

        <!-- Reloj -->
        <script src="<?php echo base_url('template/reloj/reloj.js'); ?>" type="text/javascript"></script>
        <!-- Image uploader -->
        <script src="<?php echo base_url('template/imageUploader/dropzone.js'); ?>" type="text/javascript"></script>
        <!-- InputMask -->
        <script src="<?php echo base_url('template/js/plugins/input-mask/jquery.inputmask.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('template/js/plugins/input-mask/jquery.inputmask.date.extensions.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('template/js/plugins/input-mask/jquery.inputmask.extensions.js'); ?>" type="text/javascript"></script>
        <!-- date-range-picker -->
        <script src="<?php echo base_url('template/js/plugins/daterangepicker/daterangepicker.js'); ?>" type="text/javascript"></script>
        <!-- bootstrap color picker -->
        <script src="<?php echo base_url('template/js/plugins/colorpicker/bootstrap-colorpicker.min.js'); ?>" type="text/javascript"></script>
        <!-- bootstrap time picker -->
        <script src="<?php echo base_url('template/js/plugins/timepicker/bootstrap-timepicker.min.js'); ?>" type="text/javascript"></script>
        <!--  bootstrap date picker -->
        <script src="<?php echo base_url('template/js/plugins/datepicker/bootstrap-datepicker.js'); ?>" type="text/javascript"></script>
        <!--  bootstrap-slider.js v2.0.0 -->
        <script src="<?php echo base_url('template/js/plugins/sliders/bootstrap-slider.js'); ?>" type="text/javascript"></script>
        <!--  jquery.mask.js -->
        <script src="<?php echo base_url('template/js/plugins/mask/jquery-mask.js'); ?>" type="text/javascript"></script>
        <!--  jquery.mask.js -->
        <script src="<?php echo base_url('template/js/plugins/moment/moment.js'); ?>" type="text/javascript"></script>

    </body>
</html>
