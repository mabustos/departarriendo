<aside class="right-side">
<!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Lista de reservas</h3>
                    </div><!-- /.box-header -->
                    <div class="box-header">
                        <?php
                            if(isset($mensaje)){
                                echo $mensaje;
                            }
                        ?>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="tablaListaReservas" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Identificador</th>
                                <th>Cliente</th>
                                <th>Fecha de creación</th>
                                <th>Monto total</th>
                                <th>Estado</th>
                                <th>Ver</th>
                                <th>Editar</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if(isset($reservas)){
                                    foreach ($reservas as $reserva){
                                            echo '<tr>';
                                            echo '<td>'.$reserva['idReserva'].'</td>';
                                            echo '<td><a href="'.base_url('index.php/clientes/visualizarCliente/'.$reserva['idCliente']).'">'.$reserva['nombreCliente'].'</a></td>';
                                            echo '<td>'.$reserva['fechaCreacion'].'</td>';
                                            echo '<td>'.$reserva['montoTotal'].'</td>';
                                            echo '<td class="'.$reserva['estiloColor'].'">'.$reserva['estado'].'</td>';
                                            echo '<td><a href ="'.(base_url('index.php/reservaciones/visualizarReserva/')).'/'.$reserva['idReserva'].'"/> <i class="fa fa-fw fa-eye"></i></td>';
                                            echo '<td><a href ="'.(base_url('index.php/reservaciones/editarReserva/')).'/'.$reserva['idReserva'].'"/> <i class="fa fa-fw fa-edit"></i></td>';
                                            echo '<td><a href ="'.(base_url('index.php/reservaciones/eliminarReserva/')).'/'.$reserva['idReserva'].'"/> <i class="fa fa-fw fa-edit"></i></td>';
                                            echo '</tr>';

                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->

<script type="text/javascript">
            $(function() {
                $("#tablaListaReservas").dataTable();
            });
</script>
