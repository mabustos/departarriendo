<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Sistema de arriendo de departamentos - Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- bootstrap 3.0.2 -->
    <link href="<?php echo base_url('template/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="<?php echo base_url('template/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- Original departarriendo -->
    <link href="<?php echo base_url('template/css/mataias.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="<?php echo base_url('template/css/ionicons.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
    <link href="<?php echo base_url('template/css/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="<?php echo base_url('template/css/morris/morris.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="<?php echo base_url('template/css/jvectormap/jquery-jvectormap-1.2.2.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- fullCalendar -->
    <link href="<?php echo base_url('template/css/fullcalendar/fullcalendar.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="<?php echo base_url('template/css/daterangepicker/daterangepicker-bs3.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="<?php echo base_url('template/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url('template/css/AdminLTE.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url('template/reloj/reloj.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url('template/imageUploader/css/dropzone.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- awesome-bootstrap-checkbox-->
    <link href="<?php echo base_url('template/checkbox/css/checkbox.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- fixImageResponsive-->
    <link href="<?php echo base_url('template/css/fixImageResponsive.css'); ?>" rel="stylesheet" type="text/css" />
     <!-- timePicker-->
    <link href="<?php echo base_url('template/css/timepicker/bootstrap-timepicker.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- datePicker-->
    <link href="<?php echo base_url('template/css/datepicker/datepicker.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- sliders-->
    <link href="<?php echo base_url('template/css/sliders/slider.css'); ?>" rel="stylesheet" type="text/css" />
    
  <!-- jQuery 2.0.2 -->
        <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->
    <script src="<?php echo base_url('template/js/jquery-2.1.1.min.js'); ?>"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>

<body class="skin-blue">

    <!-- header logo: style can be found in header.less -->
    <header class="header">
        <a href="<?php echo base_url(''); ?>" class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the margining -->
            Departarriendo
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="navbar-right">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-user"></i>
                            <span>Administrador <i class="caret"></i></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header bg-light-blue">
                                <img src="<?php echo base_url('template/img/administrator-icon.png'); ?>" class="img-circle" alt="User Image" />
                                <p>
                                    Administrador
                                    <small>Sistema de arriendo de departamentos</small>
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" data-toggle="modal" data-target="#modalCuenta" class="btn btn-default btn-flat">Configurar cuenta</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?php echo base_url('index.php/login/cerrar_sesion'); ?>" class="btn btn-default btn-flat">Cerrar sesión</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- Modal de cuenta -->

    <div class="modal fade" id="modalCuenta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Mi cuenta</h4>
                </div>
                <div class="modal-body">
                    <?php if(validation_errors()){ echo '<div class="alert alert-danger alert-dismissable"><p>'.validation_errors(). '</p></div>'; } ?>
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#cuentaDatosContacto" data-toggle="tab">Modificar datos de contacto</a>
                        </li>
                        <li><a href="#cuentaDatosContrasenia" data-toggle="tab">Modificar contraseña</a>
                        </li>
                        <li><a href="#cuentaAccesos" data-toggle="tab">Registro de accesos</a>
                        </li>
                    </ul>
                    <!-- Tabs -->
                    <div class="tab-content">

                        <div class="tab-pane active" id="cuentaDatosContacto">

                            <?php echo form_open( 'dashboard/', 'class="form-horizontal"');?>

                            <label for="inputCuentaUsuario">Nuevo nombre de usuario</label>
                            <?php echo form_input($nombreUsuario); ?>

                            <label for="inputCuentaCorreo1">Nuevo correo electrónico</label>
                            <?php echo form_input($correoElectronico); ?>

                            <label for="inputCuentaCorreo2">Nuevo correo electrónico alternativo</label>
                            <?php echo form_input($correoElectronicoAlt); ?>

                            <label for="exampleInputPassword1">Contraseña actual</label>
                            <?php echo form_input($password); ?>

                            <?php echo form_submit( 'submitDatosContacto', 'Ingresar'); ?>
                            </form>
                        </div>
                        <div class="tab-pane" id="cuentaDatosContrasenia">

                            <?php echo form_open( 'dashboard/', 'class="form-horizontal"');?>

                            <label for="inputContraseniaActual">Contraseña actual</label>
                            <?php echo form_input($cambioContrasenia_actual); ?>

                            <label for="inputNuevaContrasenia">Nueva contraseña</label>
                            <?php echo form_input($cambioContrasenia_nuevaContrasenia); ?>

                            <label for="inputNuevaContraseniaRep">Confirmación nueva contraseña</label>
                            <?php echo form_input($cambioContrasenia_nuevaContraseniaRep); ?>

                            <?php echo form_submit('submitContrasenia', 'Ingresar'); ?>
                            </form>

                        </div>
                        <div class="tab-pane" id="cuentaAccesos">
                            <!-- INICIO TABLA DE ACCESOS -->
                            <div class="box-body table-responsive">
                                    <table id="tablaCuentaAccesos" class="table table-bordered table-striped">
                                        <thead>

                                            <tr>
                                                <th>Id de sesión</th>
                                                <th>Fecha</th>
                                                <th>Dirección IP</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($registroAccesos)){
                                                    foreach ($registroAccesos as $row){
                                                            echo '<tr>';
                                                            echo '<td>'.$row['sessionId'].'</td>';
                                                            echo '<td>'.$row['fecha'].'</td>';
                                                            echo '<td>'.$row['ip'].'</td>';
                                                            echo '</tr>';

                                                    }
                                                }

                                            ?>
                                        </tbody>

                                    </table>
                                </div><!-- /.box-body -->
                            <!-- FIN TABLA DE ACCESOS -->
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.Modal de cuenta -->



<script type="text/javascript">
            $(function() {
                $('#tablaCuentaAccesos').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
</script>
