<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="row">
      </div>
        <h1>Cliente: <?php echo $nombreCliente.' '.$apellidoCliente; ?></h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="row">
                        <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div class="box-header">
                        <h3 class="box-title">Fotografía de perfil</h3>
                    </div>
                        <div class="center-block">
                <?php
                    if(!empty($fotografias)){
                        echo "<img src='".base_url('uploads/clientes/')."/".$fotografias[0]."' class='img-responsive'></a>";;
                    }
                    else{
                        echo '<img data-src="'.base_url('template/js/holder.js/350x300/text:Fotografía no disponible').'" class="img-responsive">';
                    }
                ?>
                        </div>
                    </div>
                        
                    <div class="col-md-8">
                        
                    <div class="box-header">
                        <h3 class="box-title">Datos del cliente</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!--                 'CIF' => $row->CIF,
                'tratamientoCliente' => $row->tratamientoCliente,
                'nombreCliente' => $row->nombreCliente,
                'apellidoCliente' => $row->apellidoCliente,
                'sexoCliente' => $row->sexoCliente,
                'fechaNacCliente' => $row->fechaNacCliente,
                'nacionalidad' => $row->nacionalidad,
                'email' => $row->email,
                'emailAlt' => $row->emailAlt,
                'telefonoContacto' => $row->telefonoContacto,
                'telefonoContactoAlt' => $row->telefonoContactoAlt,
                'skypeCliente' => $row->skypeCliente,
                'whatsappCliente' => $row->whatsappCliente,
                'profesion' => $row->profesion,
                'contacto' => $row->contacto,
                'tipoTarjetaCliente' => $row->tipoTarjetaCliente,
                'numTarjetaCliente' => $row->numTarjetaCliente, -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#clientePersonal" data-toggle="tab">Datos personales</a>
                        </li>
                        <li><a href="#clienteContacto" data-toggle="tab">Datos de contacto</a>
                        </li>
                        <li><a href="#clienteMultimedia" data-toggle="tab">Multimedia</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="clientePersonal">
                            <div class="box-body">
                                <!-- INICIO TAB 1 -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover">
                                        
                                        <tr>
                                            <td class="columna-valor">CIF</td>
                                            <td><?php echo $CIF; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Nombre completo</td>
                                            <td><?php echo $tratamientoCliente.' '.$nombreCliente.' '.$apellidoCliente; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Sexo</td>
                                            <td><?php echo $sexoCliente ; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Fecha de nacimiento</td>
                                            <td><?php echo $fechaNacCliente; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Nacionalidad</td>
                                            <td><?php echo $nacionalidad; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Profesión</td>
                                            <td><?php echo $profesion; ?></td>
                                        </tr>
                                    </table>
                                </div><!-- /.box-body -->
                                <!-- FIN TAB 1 -->
                            </div>
                        </div>
                        <div class="tab-pane" id="clienteContacto">
                            <div class="box-body">
                                <!-- INICIO TAB 2 -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover">
                                        
                                        <tr>
                                            <td class="columna-valor">Email</td>
                                            <td><?php echo $email; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Email Alternativo</td>
                                            <td><?php echo $emailAlt; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Teléfono de contacto</td>
                                            <td><?php echo $telefonoContacto; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Teléfono de contacto alternativo</td>
                                            <td><?php echo $telefonoContactoAlt; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Skype</td>
                                            <td><?php echo $skypeCliente; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Whatsapp</td>
                                            <td><?php echo $whatsappCliente; ?></td>
                                        </tr>
                                        
                                    </table>
                                </div><!-- /.box-body -->
                                <!-- FIN TAB 2 -->
                            </div>
                         </div>
                         <div class="tab-pane" id="clienteMultimedia">
                             <div class="table-responsive">
<table class="table">
    <thead>
        <tr>
            <td>#</td>
            <td>Fotografía</td>
        </tr>
    </thead>
<?php
                                    if(!empty($fotografias)){
                                        $i=0;
                                        foreach($fotografias as $item){
                                            echo '<tr>';
                                            echo '<td>';
                                            echo ($i+1);
                                            echo '</td>';
                                            echo '<td>';
                                            echo '<a href="'.base_url('uploads/clientes/')."/".$item.'" data-lightbox="fotografiasDepartamento" data-title="Fotografía #'.($i+1).' del cliente" >';
            echo "<img src='".base_url('uploads/clientes/')."/".$item."' class='img-responsive-fix'></a>";
                                            echo '</td>';
                                            echo '</tr>';
                                            $i++;
                                        }
                                    }
                                    
                                    ?>
  </table>
</div>
                        </div>
                    </div>
                    
                    <!-- /.box-body -->
                    <!-- input states -->
                    <div class="col-md-1"></div>
                </div>
                <!-- /.box-body -->
                </div>
                <!-- /.4col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
        <!-- /.row -->
    </section>
    <!-- /.content -->
</aside>