<aside class="right-side">

<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Listado de clientes</h3>
                                </div><!-- /.box-header -->
                                <div class="box-header">
                                    <?php
                                    if(isset($mensaje)){
                                        echo $mensaje;
                                    }
                                    ?>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>

                                            <tr>
                                                <th class="col-sm-2">Perfil</th>
                                                <th class="col-sm-1">CIF</th>
                                                <th class="col-sm-1">Nombre</th>

                                                <th class="col-sm-1">Fecha de Nac.</th>
                                                <th class="col-sm-1">Email</th>
                                                <th class="col-sm-1">Ver</th>
                                                <th class="col-sm-1">Editar</th>
                                                <th class="col-sm-1">Eliminar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($clientes)){
                                                    foreach ($clientes as $row){
                                                            echo '<tr>';
                                                            echo '<td>';
                                                            if(!empty($row['imagenPerfil'])){
                                                                echo "<img src='".base_url('uploads/clientes/')."/".$row['imagenPerfil']."' class='img-responsive' style='width:100%; height:auto;'></a>";;
                                                            }
                                                            else{
                                                                echo '<img data-src="'.base_url('template/js/holder.js/350x300/text:Fotografía no disponible').'" class="img-responsive">';
                                                            }
                                                            echo '</td>';
                                                            echo '<td>'.$row['CIF'].'</td>';
                                                            echo '<td>'.$row['nombreCliente'].' '.$row['apellidoCliente'].'</td>';
                                                            echo '<td>'.$row['fechaNacCliente'].'</td>';
                                                            echo '<td>'.$row['email'].'</td>';
                                                            echo '<td><a href ="'.(base_url('index.php/clientes/visualizarCliente/')).'/'.$row['CIF'].'" <i class="fa fa-fw fa-eye"></i></td>';
                                                            echo '<td><a href ="'.(base_url('index.php/clientes/editarCliente/')).'/'.$row['CIF'].'" <i class="fa fa-fw fa-edit"></i></td>';
                                                            echo '<td><a href ="#" class="botonEliminar"> <i class="fa fa-fw fa-minus"></i></td>';
                                                            echo '</tr>';

                                                    }
                                                }

                                            ?>
                                        </tbody>

                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

<!-- Modal de confirmación -->
<div class="modal fade" id="modalEliminacionCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div align="center">
                    <h2>Eliminación de cliente: <h2> <h4 class="modal-title" style="display: inline-block;" id="modalNombreCliente"></h4>
                    Id. (<h4 class="modal-title" style="display: inline-block;" id="modalCIF"></h4>)
                </div>
                </div>
            <div class="modal-body">
                <div class="alert alert-warning alert-dismissable">
                    <i class="fa fa-warning"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>!Atención!</b> Continuar con la eliminación de este cliente implica que las reservas a las que estaba asociado pasen a ser con cliente anónimo.
                </div>

                <h2 class="modal-title" style="display: inline-block;">Reservas</h2>

                <div class="table-responsive">
                    <table class="table" id="tablaConflictoReservas">
                        <thead>
                            <tr>
                                <th>N° res.</th>
                                <th>Fecha creación</th>
                                <th>Enlace</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>




            </div>

            <div class="modal-footer">
                <div align="center">
                <a href="" id="modalEnlace"><button type="button" class="btn btn-default registroCalendario">Confirmar eliminación</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.Modal de confirmación -->


<script type="text/javascript">

            $(function() {

                var enlaceEliminacion = '<?php echo (base_url('index.php/clientes/eliminarCliente/') ); ?>';
                var enlaceReserva = '<?php echo (base_url('index.php/reservaciones/visualizarReserva/') ); ?>';
                var CIF = '';
                var nombreCliente = '';


                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });

                $('.botonEliminar').on("click",function(){
                    CIF = $(this)
                        .closest('tr')
                        .find('td:eq(1)')
                        .text()
                        .trim();
                    nombreCliente = $(this)
                        .closest('tr')
                        .find('td:eq(2)')
                        .text();


                    $("#tablaConflictoReservas tr").remove();

                    $('#modalCIF').text(CIF);
                    $('#modalNombreCliente').text(nombreCliente);
                    $("#modalEnlace").attr("href", enlaceEliminacion+'/'+CIF);
                    consultarReservas();

                    $('#modalEliminacionCliente')
                        .modal('show');
                })




            function consultarReservas(){
                $.when($.ajax({
                        type: "POST",
                        url: '<?php echo base_url('index.php/') ;?>/clientes/consultarDependencias/',
                        data: {
                            CIF: CIF,
                            tipo: 'Res'
                        },
                        dataType: "json",
                        cache: false,
                        success: function (data) {
                            var listado = '';
                            $.each(data, function (linktext, link) {
                                var fila = '<tr>';
                                fila = fila + '<td> N° res. ' + link.idReserva + '</td>';
                                fila = fila + '<td> Fecha creación ' + link.fechaCreacion + '</td>';
                                fila = fila + '<td> <a href="' + enlaceReserva + '/' + link.idReserva + '">Enlace</a></td>';
                                fila = fila + '</tr>';
                                listado = listado + fila;
                            });
                            $('#tablaConflictoReservas > tbody') // AQUI
                                .append(listado);
                        },
                        error: function(xhr, textStatus, error){
                              console.log(xhr.statusText);
                              console.log(textStatus);
                              console.log(error);
                          }
                    }))
                    .then(function () {

                    });
            };



            });
        </script>
