<aside class="right-side">
	<section class="content-header">
		<h1>Editar reserva #<?php echo $idReserva; ?></h1> </section>
	<!-- Main content -->
	<section class="content-header">
		<?php echo form_open( 'reservaciones/editarReserva/'.$idReserva, 'class="form" id="editarReservaForm"');?>
		<button type="submit" class="btn btn-primary">Editar reserva</button>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header">
						<div class="col-md-4">
							<label>Total ocupación</label>
							<div class="input-group"> <span class="input-group-addon">$</span>
								<?php echo form_input($totalOcupacion); ?> <span class="input-group-addon">.00</span> </div>
						</div>
						<div class="col-md-4">
							<label>Total descuento</label>
							<div class="input-group"> <span class="input-group-addon">Desc</span>
								<?php echo form_input($totalDescuento); ?> <span class="input-group-addon">%</span> </div>
						</div>
						<div class="col-md-4">
							<label>Total reserva</label>
							<div class="input-group"> <span class="input-group-addon">$</span>
								<?php echo form_input($totalReserva); ?> <span class="input-group-addon">.00</span> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php
					if(validation_errors()){
						echo '<div class="alert alert-warning alert-dismissable">
									<i class="fa fa-warning"></i>'.validation_errors(). '</div>';
					}
					else{
						if ($datosIdenticos == TRUE) {
							echo '<div class="alert alert-warning alert-dismissable">
															<i class="fa fa-warning"></i>Los datos son idénticos</div>';
						} else {
							if(isset($ingresoCorreto)){
								{
								if($ingresoCorreto){
									echo '<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i>Reserva editada correctamente</div>';
								}
								else{
										echo '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>No se ha podido editar la reserva</div>';
								}
								}

							}
						}
					}
					?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Departamentos a reservar (no requiere pinchar Editar Reserva)</h3> </div>
					<div class="box-body table-responsive">
						<table id="tablaDepartamentosReservados" class="table table-bordered">
							<thead>

								<tr>
									<th>Edificio</th>
									<th>Nº</th>
									<th>Fecha de llegada</th>
									<th>Fecha de partida</th>
									<th>Ocupantes</th>
									<th>Monto</th>
									<th>Estado</th>
									<th>Editar</th>
									<th>Eliminar</th>
								</tr>
							</thead>
							<tbody>
							<?php
								if(isset($departamentosReservados)){
									echo $departamentosReservados;
								}
							?>
							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
			</div>
	</div>
	<di class="row">
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Datos de la reserva</h3> </div>

					<ul class="nav nav-tabs">
						<li class="active"><a href="#reservaViaje" data-toggle="tab">Datos de viaje</a> </li>
						<li><a href="#reservaAcompaniantes" data-toggle="tab">Datos de acompañantes</a> </li>
						<li><a href="#reservaCliente" data-toggle="tab">Datos del cliente</a> </li>
						<li><a href="#reservaMultimedia" data-toggle="tab">Multimedia de la reserva</a> </li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane" id="reservaCliente">
							<div class="box-body">
								<div class="form-group">
									<label>Cliente</label>
									<?php echo form_dropdown( 'seleccionCliente', $listaClientes, $idClienteSeleccionado, 'id="seleccionCliente" class="form-control"'); ?></div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<label>Tarjeta de crédito</label>
											<?php echo form_dropdown( 'seleccionTarjetaCredito', $tarjetasCredito, $idTarjetaCreditoSeleccionado, 'id="seleccionTarjetaCredito" class="form-control"'); ?> </div>
										<div class="col-md-6">
											<label>Código CVC</label>
											<?php echo form_input($codigoCVC); ?> </div>
									</div>
								</div>
								<div class="form-group">
									<label>Fecha de expiración</label>
									<div class="row">
										<div class="col-md-6">
											<label>Mes</label>
											<?php echo form_dropdown( 'seleccionMesVenc', $mesExpiracion, $vencMesSeleccionado, 'id="seleccionMesVenc" class="form-control"'); ?> </div>
										<div class="col-md-6">
											<label>Año</label>
											<?php echo form_dropdown( 'seleccionAnioVenc', $anioExpiracion, $vencAnioSeleccionado, 'id="seleccionAnioVenc" class="form-control"'); ?> </div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="reservaMonetario">
							<div class="box-body"> </div>
						</div>
						<div class="tab-pane active" id="reservaViaje">
							<div class="box-body">
								<div class="form-group">
									<label>Motivo del viaje</label>
									<?php echo form_input($motivoViaje); ?> </div>
								<div class="form-group">
									<label>Reserva de estacionamiento</label>
									<div class="row">
										<div class="col-md-6">
											<label>Inicio</label>
											<?php echo form_input($reservaEstacionamientoInicio); ?> </div>
										<div class="col-md-6">
											<label>Fin</label>
											<?php echo form_input($reservaEstacionamientoFin); ?> </div>
									</div>
								</div>
								<div class="form-group">
									<label>Modo de llegada</label>
									<?php echo form_input($modoLlegada); ?> </div>
								<div class="form-group">
									<label>Fecha de llegada</label>
									<?php echo form_input($fechaLlegada); ?> </div>
								<div class="form-group">
									<label>Hora de llegada</label>
									<!-- time Picker -->
									<div class="bootstrap-timepicker">
										<div class="form-group">
											<div class="input-group">
												<input type="text" id="horaLlegada" name="horaLlegada" class="form-control timepicker" />
												<div class="input-group-addon"> <i class="fa fa-clock-o"></i> </div>
											</div>
											<!-- /.input group -->
										</div>
										<!-- /.form group -->
									</div>
								</div>
								<div class="form-group">
									<label>Peticiones adicionales</label>
									<?php echo $peticionesAdicionales; ?> </div>
								<div class="form-group">
									<label>Datos de vuelo</label>
									<div class="row">
										<div class="col-md-6">
											<label>Empresa</label>
											<?php echo form_dropdown( 'seleccionEmpresaVuelo', $empresasVuelo,$idEmpresaVueloSeleccionado, 'id="seleccionEmpresaVuelo" class="form-control"'); ?> </div>
										<div class="col-md-6">
											<label>Número de vuelo</label>
											<?php echo form_input($numeroVuelo); ?> </div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="reservaMultimedia">
							<div class="box-body">
							<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<td>#</td>
										<td>Fotografía</td>
										<td>¿Eliminar?</td>
									</tr>
								</thead>
								<tbody>
								<?php
								if(!empty($fotografias)){
									foreach($fotografias as $fotografia){
										echo '<tr>';
										echo '<td>';
										echo $fotografia['numFotografia'];
										echo '</td>';
										echo '<td>';
										echo '<a href="'.base_url('uploads/reservas/')."/".$fotografia['nombreArchivo'].'" data-lightbox="fotografiasReserva" data-title="Fotografía #'.$fotografia['numFotografia'].' de la reserva" >';
										echo "<img src='".base_url('uploads/reservas/')."/".$fotografia['nombreArchivo']."' class='img-responsive-fix'></a>";
										echo '</td>';
										echo '<td>';
										echo '<input type="checkbox" value="aceptado" name="'.$fotografia['nombreArchivo'].'" id="'.$fotografia['nombreArchivo'].'" onclick="actualizarRestante(this);">';
										echo '<label for="'.$fotografia['nombreArchivo'].'">¿Eliminar?</label>';
										echo '</td>';
										echo '</tr>';
									}
								}
								?>
								</tbody>
							</table>
							</div>

							</div>
						</div>
						<div class="tab-pane" id="reservaAcompaniantes">
							<div class="box-body">
								<div class="form-group">
									<div class="callout callout-info">
											<h4>Información</h4>
											<p>Como máximo se permiten <strong>15</strong> fotografías por reserva. <em>Restan (<span id="cantidadRestante"></span>) fotografías</em></p>
										</div>
									<label>Cantidad de acompañantes</label>

									<?php echo form_input($cantidadAcompaniantes); ?> <span id="errmsg"></span> </div>
									<div class="hidden">
										<input type="text" id="cantidadReservas" name="cantidadReservas" class="form-control" value="<?php echo $cantidadReservas; ?>">
									</div>
								</form>
								<!-- FIN DE FORMULARIO (SIN IMAGENES)!-->
								<div id="imagenesAcompaniantes" class="dropzone"> </div>
							</div>
						</div>

					</div>
				</div>
			</div>
			</form>
		</div>
	</section>
</aside>
<!-- Modal de cuenta -->
<div class="modal fade" id="modalReserva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="display: inline-block;" id="myModalLabel">Reserva</h4> (
				<h4 class="modal-title" style="display: inline-block;" id="modalIdEdificio"></h4>)
				<h4 class="modal-title" style="display: inline-block;" id="modalNombreEdificio"></h4> #
				<h4 class="modal-title" style="display: inline-block;" id="modalNumDpto"></h4> Cap
				<h4 class="modal-title" style="display: inline-block;" id="modalOcupantes"></h4>
				</div>
			<div class="modal-body">
				<div id='calendar'></div>
				<label>Fecha de inicio</label>
				<input type="text" id="modalFechaInicio" value maxlength='11' class='form-control datepicker' placeholder='Ingrese la fecha de llegada'>
				<label>Hora de inicio</label>
				<!-- time Picker -->
				<div class="bootstrap-timepicker">
					<div class="form-group">
						<div class="input-group">
							<input type="text" class="form-control timepicker" id="modalTiempoInicio" />
							<div class="input-group-addon"> <i class="fa fa-clock-o"></i> </div>
						</div>
						<!-- /.input group -->
					</div>
					<!-- /.form group -->
				</div>
				<label>Fecha de termino</label>
				<input type="text" id="modalFechaTermino" value maxlength='11' class='form-control datepicker' placeholder='Ingrese la fecha de partida'>
				<label>Hora de termino</label>
				<!-- time Picker -->
				<div class="bootstrap-timepicker">
					<div class="form-group">
						<div class="input-group">
							<input type="text" class="form-control timepicker" id="modalTiempoTermino" />
							<div class="input-group-addon"> <i class="fa fa-clock-o"></i> </div>
						</div>
						<!-- /.input group -->
					</div>
					<!-- /.form group -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<button type="button" class="btn btn-default registroCalendario btn-block">Modificar fechas</button>
				</div>
			</div>
			<div id="errorDisponibilidad"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<!-- /.Modal de cuenta -->
<script type="text/javascript">
	var listaPrecios = [];
	var maximoFotografias = 15;
	var cantidadRestante = <?php if(isset($fotografias)) echo (15-count($fotografias));
								else echo 15?>; // Cantidad restante de fotografías
	var idReserva = <?php echo "'".$idReserva."'"; ?>;
	var filaSeleccionada;
	var fechaInicioFila;
	var fechaTerminoFila;
	$("#cantidadRestante").html(cantidadRestante);


	$(document)
		.ready(function () {
			$("#tablaConsultaDepartamento")
				.dataTable();
			$('.percent')
				.mask('00');
			$('#totalDescuento')
				.val(<?php echo $totalDescuento['value']; ?>);
			$('#totalReserva')
				.val(<?php echo $totalReserva['value']; ?>);
			$('#totalOcupacion')
				.val(<?php echo $totalOcupacion['value']; ?>);
			$('#fechaLlegada') /* Fecha de llegada del cliente al terminal */
				.datepicker({
					format: 'dd/mm/yyyy',
					startDate: obtenerFechaAhora()
				});
			$('#reservaEstacionamientoInicio')
				.datepicker({
					format: 'dd/mm/yyyy',
					startDate: obtenerFechaAhora()
				});
			$('#reservaEstacionamientoFin')
				.datepicker({
					format: 'dd/mm/yyyy',
					startDate: obtenerFechaAhora()
				});
			$('#reservaFechaInicio')
				.datepicker({
					format: 'dd/mm/yyyy',
					startDate: obtenerFechaAhora()
				});
			$('#reservaFechaTermino')
				.datepicker({
					format: 'dd/mm/yyyy',
					startDate: obtenerFechaAhora()
				});
			$(".timepicker")
				.timepicker({
					showInputs: false
				});
			$(".datepicker")
				.datepicker({
					format: 'dd/mm/yyyy',
				});
			$(".formatoFechaHora").mask('0000/00/00 00:00:00');
		});

	$(document)
		.ready(function () {

			//ACTUALIZACION DE PRECIOS LUEGO DE POST
			<?php
				if(isset($preciosDepartamentos)){
					echo $preciosDepartamentos;
				}
			?>

			$(".capacidad")
				.bind("change paste keyup", function () {
					var suma = 0;
					var fila = 1;
					$('.capacidad')
						.each(function () {
							var idEdificio = $(this)
								.closest('tr')
								.find('td:eq(0)')
								.text()
								.trim();
							var numDpto = $(this)
								.closest('tr')
								.find('td:eq(1)')
								.text()
								.trim();
							var fechaInicio  = new Date($(this)
								.closest('tr')
								.find('td:eq(2)')
								.text());
							var fechaTermino = new Date($(this)
								.closest('tr')
								.find('td:eq(3)')
								.text());

							var diferenciaFecha = Math.ceil((fechaTermino - fechaInicio) / 1000 / 3600 / 24);
							var precio = parseInt(listaPrecios['ed' + idEdificio + 'dpto' + numDpto].precios[$(this)
								.val()])*diferenciaFecha;
							$('#precio-fila' + (fila-1)).val(precio);
							fila++;
							suma += precio;
						});
					$('#totalOcupacion')
						.val(suma);
					if ($("#totalDescuento")
						.val()) {
						var descuento = ($("#totalDescuento")
							.val());
					} else {
						var descuento = 0;
					}
					$("#totalReserva")
						.val(suma - suma * descuento / 100);
				});
				$(".capacidad")
					.trigger("change");

			$('#calendar')
				.fullCalendar({
					header: {
						left: 'prev,next today',
						center: 'title',
						right: 'month,agendaWeek,agendaDay'
					},
					defaultView: 'agendaWeek',
					defaultDate: '2014-11-12',
					selectable: true,
					eventLimit: true, // allow "more" link when too many events
					eventRender: function (event, element) {}
				});

			var myDropzone = new Dropzone("#imagenesAcompaniantes", {
				url: "<?php echo base_url('index.php/reservaciones/subirImagenesReserva'); ?>"
			});
			Dropzone.options.myDropzone = false;
			var banderaSubida = false;
			var archivosDisponibles = false;
			jQuery("#editarReservaForm")
				.submit(function (e) {
					if (archivosDisponibles) {
						if (!banderaSubida) {
							e.preventDefault();
							e.stopPropagation();
							myDropzone.processQueue();
							banderaSubida = true;
						} else {
							return true;
						}
					} else {
						return true;
					}
				});
			myDropzone.on("queuecomplete", function () {
				//return true;
				$("#editarReservaForm")
					.submit();
			});
			myDropzone.on("addedfile", function (file) {
				archivosDisponibles = true;
			});
			$('#cantidadAcompaniantes')
				.on('input', function (e) {

					return true;
				});
			$(".numericOnly")
				.bind('keypress', function (e) {
					if (e.keyCode == '9' || e.keyCode == '16') {
						return;
					}
					var code;
					if (e.keyCode) code = e.keyCode;
					else if (e.which) code = e.which;
					if (e.which == 46) return false;
					if (code == 8 || code == 46) return true;
					if (code < 48 || code > 57) return false;
				});
			$(".numericOnly")
				.bind("paste", function (e) {
					e.preventDefault();
				});
			$(".numericOnly")
				.bind('mouseenter', function (e) {
					var val = $(this)
						.val();
					if (val != '0') {
						val = val.replace(/[^0-9]+/g, "")
						$(this)
							.val(val);
					}
				});
		});

	$("#totalDescuento")
		.bind("change paste keyup", function () {
			var suma = parseInt($("#totalOcupacion")
				.val());
			var descuento = parseInt($("#totalDescuento")
				.val());
			if (!isInt(descuento)) descuento = 0;
			$("#totalReserva")
				.val(suma - suma * descuento / 100);
		});
	$(document)
		.on('click', 'button.registroCalendario', function () { // <-- changes
			var datosConflicto = '';
			var FechaInicioModal = $('#modalFechaInicio')
				.datepicker('getDate');
			var FechaTerminoModal = $('#modalFechaTermino')
				.datepicker('getDate');

			var FechaInicioModalBack = $('#modalFechaInicio')
				.datepicker('getDate');
			var FechaTerminoModalBack = $('#modalFechaTermino')
				.datepicker('getDate');

			var HoraInicioModal = $('#modalTiempoInicio')
				.data('timepicker');
			var HoraTerminoModal = $('#modalTiempoTermino')
				.data('timepicker');
			var HoraInicio = parseTime(HoraInicioModal.getTime());
			var HoraFinal = parseTime(HoraTerminoModal.getTime());
			var ocupantes = $('#modalOcupantes').text().trim();
			FechaInicioModalBack.setHours(HoraInicio.getHours());
			FechaTerminoModalBack.setHours(HoraFinal.getHours());
			FechaInicioModalBack.setMinutes(HoraInicio.getMinutes());
			FechaTerminoModalBack.setMinutes(HoraFinal.getMinutes());
			if (comprobacionCamposFecha(FechaInicioModalBack, FechaTerminoModalBack)) {
				var FechaInicio = FechaInicioModal.getFullYear()+'-'+ (FechaInicioModal.getMonth()+1)+'-'+ FechaInicioModal.getDate()+' '+ HoraInicio.getHours()+':'+ HoraInicio.getMinutes()+':'+ HoraInicio.getSeconds();
				var FechaTermino = FechaTerminoModal.getFullYear()+'-'+ (FechaTerminoModal.getMonth()+1)+'-'+ FechaTerminoModal.getDate()+' '+ HoraFinal.getHours()+':'+ HoraFinal.getMinutes()+':'+ HoraFinal.getSeconds();
				var disponibilidad = true;
				var idEdificio = $('#modalIdEdificio').text().trim();
				var numDpto = $('#modalNumDpto').text().trim();
				var argumentos= "{'idReserva': '"+idReserva+"','idEdificio':'"+idEdificio+"','numDpto':'"+numDpto+"','fechaInicioNew':'"+FechaInicio+"','fechaTerminoNew':'"+FechaTermino+"','fechaInicioOld':'"+fechaInicioFila+"','fechaTerminoOld':'"+fechaTerminoFila+"'}";
				var i = 0;
				var elemento = $('#calendar').fullCalendar('clientEvents');
				var dateFechaInicio = new Date(FechaInicio);
				var dateFechaTermino = new Date(FechaTermino);
				for (i = 0; i < $('#calendar').fullCalendar('clientEvents').length; i++) {
					if((dateFechaInicio != elemento[i].start && dateFechaTermino != elemento[i].end)){
						console.log('1A'+dateFechaInicio+'1B'+dateFechaInicio+'2A'+dateFechaTermino+'2B'+elemento[i].end);
						if ((dateFechaInicio >= elemento[i].start && dateFechaTermino <= elemento[i].end) || (dateFechaInicio >= elemento[i].start && dateFechaInicio <= elemento[i].end) || (dateFechaTermino >= elemento[i].start && dateFechaTermino <= elemento[i].end)) {
							var datosConflicto = '<br /><strong>Departamento</strong>: ' + elemento[i].title + '<br /><strong>Fecha de inicio</strong>: ' + elemento[i].start + '<br /><strong>Fecha de termino</strong>: ' + elemento[i].end;
							$('#errorDisponibilidad')
								.html('<div id="errorDisponibilidad" class="alert alert-warning alert-dismissable"><i class="fa fa-ban"></i>Error: Ya existe una reserva entre el intervalo indicado' + datosConflicto + '</div>');
							disponibilidad = false;
						}
					}
				}
				if(disponibilidad){
				$.when($.ajax({
						type: "POST",
						url: "<?php echo base_url('index.php/') ;?>/reservaciones/modificarOcupaciones",
						dataType: "json",
						data: {
							idReserva: idReserva,
							idEdificio: idEdificio,
							numDpto: numDpto,
							fechaInicioNew: FechaInicio,
							fechaTerminoNew: FechaTermino,
							fechaInicioOld: fechaInicioFila,
							fechaTerminoOld: fechaTerminoFila,
							ocupantes: ocupantes
						},
						cache: false,
						success: function (data) {

							$.each(data, function (linktext, link) {
								//alert(linktext+' : '+link);
								datosConflicto = datosConflicto+'<br /><strong>Reserva</strong>: ' + link.idReserva +
								'<br />Fecha de inicio: ' + link.fechaLlegada +
								'<br />Fecha de termino: ' + link.fechaPartida +
								'<br />Ocupantes: ' + link.capacidad;
							});

						}
					}))
					.then(function () {
						if(datosConflicto==''){
							var numFila = filaSeleccionada.index();
							$('#tablaDepartamentosReservados tbody tr').eq(numFila).find('td').eq(2).text(moment(dateFechaInicio).format('YYYY/MM/DD hh:mm:ss'));
							$('#tablaDepartamentosReservados tbody tr').eq(numFila).find('td').eq(3).text(moment(dateFechaTermino).format('YYYY/MM/DD hh:mm:ss'));
							actualizarTabla();

						}
						else{
							$('#errorDisponibilidad')
									.html('<div id="errorDisponibilidad" class="alert alert-warning alert-dismissable"><i class="fa fa-ban"></i>Error: Ya existen ocupaciones para este departamento en la fecha ingresada.' + datosConflicto + '</div>');
						}
					});
				}
				/*var i = 0;
				var elemento = $('#calendar').fullCalendar('clientEvents');
				for (i = 0; i < $('#calendar').fullCalendar('clientEvents').length; i++) {
					if ((FechaInicio >= elemento[i].start && FechaTermino <= elemento[i].end) || (FechaInicio >= elemento[i].start && FechaInicio <= elemento[i].end) || (FechaTermino >= elemento[i].start && FechaTermino <= elemento[i].end)) {
						var datosConflicto = '<br /><strong>Departamento</strong>: ' + elemento[i].title + '<br /><strong>Fecha de inicio</strong>: ' + elemento[i].start + '<br /><strong>Fecha de termino</strong>: ' + elemento[i].end;
						$('#errorDisponibilidad')
							.html('<div id="errorDisponibilidad" class="alert alert-warning alert-dismissable"><i class="fa fa-ban"></i>Error: Ya existe una reserva entre el intervalo indicado' + datosConflicto + '</div>');
						disponibilidad = false;
					}
					if ((FechaInicio == elemento[i].start && FechaTermino == elemento[i].end)) {
						var datosConflicto = '<br /><strong>Departamento</strong>: ' + elemento[i].title + '<br /><strong>Fecha de inicio</strong>: ' + elemento[i].start + '<br /><strong>Fecha de termino</strong>: ' + elemento[i].end;
						$('#errorDisponibilidad')
							.html('<div id="errorDisponibilidad" class="alert alert-warning alert-dismissable"><i class="fa fa-ban"></i>Error: Ya existe una reserva entre el intervalo indicado' + datosConflicto + '</div>');
						disponibilidad = false;
					}
				}
				if (disponibilidad) {
					var idEdificioValor = $('#modalIdEdificio')
						.text()
						.trim();
					var numDptoValor = $('#modalNumDpto')
						.text()
						.trim();
					var nombreDepartamento = $('#modalNombreEdificio')
						.text() + ' #' + numDptoValor;
					var dropdown = '';

					$('#calendar').fullCalendar('renderEvent', {
										title: nombreDepartamento,
										start: FechaInicio,
										end: FechaTermino,
										idEdificio: idEdificioValor,
										numDpto: numDptoValor,
										allDay: false
									}, true);
					var idEdificio = $('#modalIdEdificio').text().trim();
					var numDpto = $('#modalNumDpto').text().trim();
					var numFila = filaSeleccionada.index();

					$('#tablaDepartamentosReservados tbody tr').eq(numFila).find('td').eq(2).text(moment(FechaInicio).format('YYYY-mm-DD hh:mm:ss'));
					$('#tablaDepartamentosReservados tbody tr').eq(numFila).find('td').eq(3).text(moment(FechaTermino).format('YYYY-mm-DD hh:mm:ss'));
					$(".capacidad").trigger("change");;
					} else {
						alert('Este departamento ya se ha ingresado durante el período indicado');
					}*/
			} else {
				alert('Hay un error con las fechas ingresadas. Compruebe que la fecha de inicio sea igual o anterior a la de término');
			}
			$('#0_capacidadSeleccionada').trigger('change');

		});
	$(document)
		.on('click', 'button.botonEliminar', function () { // <-- changes
			var idEdificio = $(this)
				.closest('tr')
				.find('td:eq(0)')
				.text()
				.trim();
			var numDpto = $(this)
				.closest('tr')
				.find('td:eq(1)')
				.text()
				.trim();
			var fechaLlegada = $(this)
				.closest('tr')
				.find('td:eq(2)')
				.text();
			var fechaPartida = $(this)
				.closest('tr')
				.find('td:eq(3)')
				.text();
			var fila = $(this)
				.closest('tr');
			
			var exito;
			$.when($.ajax({
					type: "POST",
					url: '<?php echo base_url('index.php/') ;?>/reservaciones/eliminarOcupacion',
					data: {
						idEdificio: idEdificio,
						numDpto: numDpto,
						fechaLlegada: fechaLlegada,
						fechaPartida: fechaPartida
					},
					dataType: "json",
					cache: false,
					success: function (data) {
						$.each(data, function (linktext, link) {
							exito=link.valor;
							if(link.valor==true){
								alert('Ocupación eliminada exitósamente');
							}
							else{
								alert('No se ha podido eliminar la ocupación');
							}
						});
					}
				}))
				.then(function () {
					if(exito){
						fila.remove();
						var cantDepartamentosAgregados = $('#tablaDepartamentosReservados tr').length-1; // La resta por desfase
						$("#cantidadReservas").val(cantDepartamentosAgregados);
					}

				});




			return false;
		});
	$(document)
		.on('click', 'button.botonEditar', function () { // <-- changes
			filaSeleccionada = $(this).closest('tr');
			var idEdificio = $(this)
				.closest('tr')
				.find('td:eq(0)')
				.text()
				.trim();
			var numDpto = $(this)
				.closest('tr')
				.find('td:eq(1)')
				.text()
				.trim();

			var fechaInicio = $(this)
				.closest('tr')
				.find('td:eq(2)')
				.text();
			var fechaTermino = $(this)
				.closest('tr')
				.find('td:eq(3)')
				.text()
				.trim();
			var ocupantes = $(this)
				.closest('tr')
				.find('td:eq(4)').find('select').val();

			fechaInicioFila=fechaInicio;
			fechaTerminoFila=fechaTermino;

			insertarFechaModal();
			$('#modalIdEdificio')
				.text(idEdificio);
			$('#modalNumDpto')
				.text(numDpto);
			$('#modalOcupantes')
				.text(ocupantes);
			$('#modalFechaInicio')
				.val(moment(fechaInicio).format('DD/MM/YYYY'));
			$('#modalFechaTermino')
				.val(moment(fechaTermino).format('DD/MM/YYYY'));
			$('#modalFechaInicio')
				.datepicker('setDate',moment(fechaInicio).format('DD/MM/YYYY'));
			$('#modalFechaTermino')
				.datepicker('setDate',moment(fechaTermino).format('DD/MM/YYYY'));

			$('#errorDisponibilidad')
				.text('');
			$('#calendar')
				.fullCalendar('removeEvents');
			$('.botonEditar')
				.attr('disabled', 'disabled');
			$.when($.ajax({
					type: "POST",
					url: '<?php echo base_url('index.php/') ;?>/reservaciones/consultaReservasDepartamento',
					data: {
						idEdificio: $('#modalIdEdificio')
							.text()
							.trim(),
						numDpto: $('#modalNumDpto')
							.text()
							.trim()
					},
					dataType: "json",
					cache: false,
					success: function (data) {
						var nombreEdificio = '';
						$.each(data, function (linktext, link) {
							FechaInicio = link.fechaLlegada;
							FechaTermino = link.fechaPartida;
							nombreEdificio = link.nombreEdificio;
							var Titulo = link.nombreEdificio + ' #' + link.numDpto;
							$('#calendar')
								.fullCalendar('renderEvent', {
									title: Titulo,
									start: FechaInicio,
									end: FechaTermino,
									idEdificio: link.idEdificio,
									numDpto: link.numDpto,
									allDay: false
								}, true);
						});
						$('#modalNombreEdificio').text(nombreEdificio);
					}
				}))
				.then(function () {
					$('.botonEditar')
						.removeAttr('disabled');
					$('#tablaDepartamentosReservados > tbody  > tr')
						.each(function () { // Reingreso de eventos
							if (($(this)
									.find('td:eq(0)')
									.text() == idEdificio) && ($(this)
									.find('td:eq(1)')
									.text() == numDpto)) {
								var titulo = nombreEdificio + ' #' + numDpto;
								var fechaInicioTabla = new Date($(this)
									.find('td:eq(2)')
									.text());
								var fechaTerminoTabla = new Date($(this)
									.find('td:eq(3)')
									.text());
								$('#calendar')
									.fullCalendar('renderEvent', {
										title: titulo,
										start: fechaInicioTabla,
										end: fechaTerminoTabla,
										idEdificio: idEdificio,
										numDpto: numDpto,
										allDay: false
									}, true);
							}
						});
					$('#modalReserva')
						.modal('show');


				});
			return false;
		});
	$('#modalReserva')
		.on('shown.bs.modal', function () {
			$("#calendar")
				.fullCalendar('render');
		});

	function actualizarRestante(cb) {
		if (cb.checked) cantidadRestante += 1;
		else cantidadRestante -= 1;
		$("#cantidadRestante")
			.html(cantidadRestante);
		Dropzone.maxFiles=cantidadRestante;
	};

	function comprobarExistenciaDepartamento(idEdificio, numDpto, fechaInicio, fechaTermino) { // Comprobación departamento
		var similaridad = false;
		$('#tablaDepartamentosReservados > tbody  > tr')
			.each(function () {
				if (($(this)
						.find('td:eq(0)')
						.text() == idEdificio) && ($(this)
						.find('td:eq(1)')
						.text() == numDpto)) {
					var fechaInicioTabla = new Date($(this)
						.find('td:eq(2)')
						.text());
					var fechaTerminoTabla = new Date($(this)
						.find('td:eq(3)')
						.text());
					if (fechaInicio >= fechaInicioTabla && fechaTermino <= fechaTerminoTabla) {
						similaridad = true;
						return false;
					} else if (fechaInicio >= fechaInicioTabla && fechaInicio <= fechaTerminoTabla) {
						similaridad = true;
						return false;
					} else if (fechaTermino >= fechaInicioTabla && fechaTermino <= fechaTerminoTabla) {
						similaridad = true;
						return false;
					}
				}
			});
		return !similaridad;
	};

	function parseTime(timeStr, dt) {
		if (!dt) {
			dt = new Date();
		}
		var time = timeStr.match(/(\d+)(?::(\d\d))?\s*(p?)/i);
		if (!time) {
			return NaN;
		}
		var hours = parseInt(time[1], 10);
		if (hours == 12 && !time[3]) {
			hours = 0;
		} else {
			hours += (hours < 12 && time[3]) ? 12 : 0;
		}
		dt.setHours(hours);
		dt.setMinutes(parseInt(time[2], 10) || 0);
		dt.setSeconds(0, 0);
		return dt;
	}

	function isInt(n) {
		return n % 1 === 0;
	}

	function obtenerFechaAhora() {
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1; //January is 0!
		var yyyy = today.getFullYear();
		if (dd < 10) {
			dd = '0' + dd
		}
		if (mm < 10) {
			mm = '0' + mm
		}
		var today = dd + '/' + mm + '/' + yyyy;
		return today;
	}

	function comprobacionCamposFecha(campo1, campo2) {
		var fechaCampo1 = new Date(campo1);
		var fechaCampo2 = new Date(campo2);
		if (fechaCampo1 < new Date(obtenerFechaAhora())) {
			return false;
		} else {
			if (fechaCampo1 < fechaCampo2) {
				return true;
			} else {
				return false;
			}
		}
	}

	function insertarFechaModal() {
		var FechaInicioForm = $('#reservaFechaInicio');
		var FechaTerminoForm = $('#reservaFechaTermino');
		var FechaInicioModal = $('#modalFechaInicio');
		var FechaTerminoModal = $('#modalFechaTermino');
		var FechaInicio = FechaInicioForm.datepicker('getDate');
		var FechaTermino = FechaTerminoForm.datepicker('getDate');
		var FechaActual = obtenerFechaAhora();
		if ((FechaInicioForm.val() == "") && (FechaTerminoForm.val() == "")) {
			FechaInicioModal.datepicker('setDate', FechaActual);
			FechaTerminoModal.datepicker('setDate', FechaActual);
		} else if ((FechaInicioForm.val() == "")) {
			FechaInicioModal.datepicker('setDate', FechaActual);
			FechaTerminoModal.datepicker('setDate', FechaTermino);
		} else if ((FechaTerminoForm.val() == "")) {
			FechaInicioModal.datepicker('setDate', FechaInicio);
			FechaTerminoModal.datepicker('setDate', FechaInicio);
		}
		FechaInicioModal.datepicker('setStartDate', FechaActual);
		FechaTerminoModal.datepicker('setStartDate', FechaActual);
	}

	function actualizarTabla(){
		var idEdificio = filaSeleccionada
			.find('td:eq(0)')
			.text()
			.trim();
		var numDpto = filaSeleccionada
			.find('td:eq(1)')
			.text()
			.trim();

		var fechaInicio = filaSeleccionada
			.find('td:eq(2)')
			.text()
			.trim();
		var fechaTermino = filaSeleccionada
			.find('td:eq(3)')
			.text()
			.trim();

		fechaInicioFila=fechaInicio;
		fechaTerminoFila=fechaTermino;

		insertarFechaModal();
		$('#modalIdEdificio')
			.text(idEdificio);
		$('#modalNumDpto')
			.text(numDpto);
		$('#errorDisponibilidad')
			.text('');
		$('#calendar')
			.fullCalendar('removeEvents');
		$('#calendar').fullCalendar('rerenderEvents');
		$('.botonEditar')
			.attr('disabled', 'disabled');
		$.when($.ajax({
				type: "POST",
				url: '<?php echo base_url('index.php/') ;?>/reservaciones/consultaReservasDepartamento',
				data: {
					idEdificio: $('#modalIdEdificio')
						.text()
						.trim(),
					numDpto: $('#modalNumDpto')
						.text()
						.trim()
				},
				dataType: "json",
				cache: false,
				success: function (data) {
					var nombreEdificio = '';
					$.each(data, function (linktext, link) {
						FechaInicio = link.fechaLlegada;
						FechaTermino = link.fechaPartida;
						nombreEdificio = link.nombreEdificio;
						var Titulo = link.nombreEdificio + ' #' + link.numDpto;
						$('#calendar')
							.fullCalendar('renderEvent', {
								title: Titulo,
								start: FechaInicio,
								end: FechaTermino,
								idEdificio: link.idEdificio,
								numDpto: link.numDpto,
								allDay: false
							}, true);
					});
					$('#modalNombreEdificio').text(nombreEdificio);
				}
			}))
			.then(function () {
				$('.botonEditar').removeAttr('disabled');
				$('#tablaDepartamentosReservados > tbody  > tr')
					.each(function () { // Reingreso de eventos
						if (($(this)
								.find('td:eq(0)')
								.text() == idEdificio) && ($(this)
								.find('td:eq(1)')
								.text() == numDpto)) {
							var titulo = nombreEdificio + ' #' + numDpto;
							var fechaInicioTabla = new Date($(this)
								.find('td:eq(2)')
								.text());
							var fechaTerminoTabla = new Date($(this)
								.find('td:eq(3)')
								.text());
							$('#calendar')
								.fullCalendar('renderEvent', {
									title: titulo,
									start: fechaInicioTabla,
									end: fechaTerminoTabla,
									idEdificio: idEdificio,
									numDpto: numDpto,
									allDay: false
								}, true);
						}
					});
				})
			}
</script>
