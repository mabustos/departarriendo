<aside class="right-side">
    <section class="content-header">
        <h1>Agregar reserva </h1>
    </section>

    <!-- Main content -->
    <section class="content-header">
        <?php echo form_open( 'reservaciones/agregarReserva', 'class="form" id="agregarReservaForm" data-valideasy-mode="unified');?>
        <button type="submit" class="btn btn-primary">Ingresar reserva</button>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Datos de la reserva</h3>
                    </div>
                    <div class="box-body">
                        <div class="box-body table-responsive">
                            <table id="tablaDepartamentos" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Edificio</th>
                                        <th>Nº</th>
                                        <th>Calendario</th>
                                        <th>Eliminar de lista</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Datos de la reserva</h3>
                    </div>

                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#reservaCliente" data-toggle="tab">Datos del cliente</a>
                        </li>
                        <li><a href="#reservaViaje" data-toggle="tab">Datos de viaje</a>
                        </li>
                        <li><a href="#reservaAcompaniantes" data-toggle="tab">Datos de acompañantes</a>
                        </li>
                        <li><a href="#reservaEspacio" data-toggle="tab">Datos de espacio</a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="reservaCliente">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Cliente</label>
                                    <?php echo $listaClientes; ?>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Tarjeta de crédito</label>
                                            <?php echo $listaTarjetas; ?>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Código CVC</label>
                                            <?php echo form_input($codigoCVC); ?>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label>Fecha de expiración</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Mes</label>
                                            <?php echo form_dropdown( 'mesExpiracion', $mesExpiracion, 0, 'id="mesExpiracion" class="form-control"'); ?>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Año</label>
                                            <?php echo form_dropdown( 'anioExpiracion', $anioExpiracion, 0, 'id="anioExpiracion" class="form-control"'); ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="tab-pane" id="reservaMonetario">
                            <div class="box-body">
                            </div>
                        </div>



                        <div class="tab-pane" id="reservaViaje">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Motivo del viaje</label>
                                    <?php echo form_input($motivoViaje); ?>
                                </div>
                                <div class="form-group">
                                    <label>Reserva de estacionamiento</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Inicio</label>
                                            <?php echo form_input($reservaEstacionamientoInicio); ?>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Fin</label>
                                            <?php echo form_input($reservaEstacionamientoFin); ?>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label>Modo de llegada</label>
                                    <?php echo form_input($modoLlegada); ?>
                                </div>
                                <div class="form-group">
                                    <label>Hora de llegada</label>

                                    <!-- time Picker -->
                                    <div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" />
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                        <!-- /.form group -->
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label>Peticiones adicionales</label>
                                    <?php echo form_input($peticionesAdicionales); ?>
                                </div>
                                <div class="form-group">
                                    <label>Datos de vuelo</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Empresa</label>
                                            <?php echo form_input($empresaVuelo); ?>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Número de vuelo</label>
                                            <?php echo form_input($numeroVuelo); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="reservaEspacio">
                            <div class="box-body">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#reservaEspacioFecha" data-toggle="tab">Solicitar por fecha</a>
                                    </li>
                                    <li><a href="#reservaEspacioDepartamento" data-toggle="tab">Solicitar por departamento</a>
                                    </li>
                                    <li><a href="#reservaDepartamento" data-toggle="tab">Datos de reserva</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="reservaEspacioFecha">
                                        <div class="box-body">
                                            <button type="button" id="botonPregunta" class="btn btn-primary">Preguntar</button>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Fecha de inicio</label>
                                                    <?php echo form_input($reservaFechaInicio); ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Fecha de fin</label>
                                                    <?php echo form_input($reservaFechaTermino); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="reservaEspacioDepartamento">
                                        <div class="box-body">
                                            Por departamento
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="reservaDepartamento">
                                        <div class="box-body">
                                            Faaaa
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body table-responsive">
                                <table id="tablaDepartamentos" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Edificio</th>
                                            <th>Nº</th>
                                            <th>Dormitorios</th>
                                            <th>M2</th>
                                            <th>Capacidad</th>
                                            <th>Selección</th>
                                            <th>Eliminar de lista</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->

                        </div>

                        <div class="tab-pane" id="reservaAcompaniantes">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Cantidad de acompañantes</label>
                                    <?php echo form_input($cantidadAcompaniantes); ?>
                                    <span id="errmsg"></span>
                                </div>
                                </form>
                                <!-- FIN DE FORMULARIO (SIN IMAGENES)!-->
                                <div id="imagenesAcompaniantes" class="dropzone">
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            </form>
        </div>
    </section>
</aside>

<script type="text/javascript">
    // When the document is ready
    $(document).ready(function() {
        $('#reservaEstacionamientoInicio').daterangepicker({
            singleDatePicker: true,
            format: 'DD/MM/YY',
            showDropdowns: true,
            startDate: <? php echo $fechaActualServidor ?>
        });
        $('#reservaEstacionamientoFin').daterangepicker({
            singleDatePicker: true,
            format: 'DD/MM/YY',
            showDropdowns: true
        });
        $('#reservaFechaInicio').daterangepicker({
            singleDatePicker: true,
            format: 'DD/MM/YYYY',
            showDropdowns: true
        });
        $('#reservaFechaTermino').daterangepicker({
            singleDatePicker: true,
            format: 'DD/MM/YYYY',
            showDropdowns: true
        });
        $(".timepicker").timepicker({
            showInputs: false
        });
        $('#reservaFechaTermino').on('apply.daterangepicker', function(ev, picker) {
            var FechaTermino = new Date(picker.startDate.format('YYYY'), picker.startDate.format('MM') - 1, picker.startDate.format('DD'));
            var SeleccionFechaInicio = $('#reservaFechaInicio').data('daterangepicker');
            var FechaInicio = new Date(SeleccionFechaInicio.startDate.format('YYYY'), SeleccionFechaInicio.startDate.format('MM') - 1, SeleccionFechaInicio.startDate.format('DD'));
            if (FechaTermino >= FechaInicio) {
                return true;
            } else {
                alert('La fecha de término debe ser posterior a la de inicio');
                SeleccionFechaInicio.setStartDate(FechaTermino);
                SeleccionFechaInicio.setEndDate(FechaTermino);
            }
        });
    });
</script>
<script>
    $(document).ready(function() {
        var myDropzone = new Dropzone("#imagenesAcompaniantes", {
            url: "<?php echo base_url('index.php/reservaciones/subirImagenesReserva'); ?>"
        });
        var banderaSubida = false;
        var archivosDisponibles = false;
        jQuery("#agregarReservaForm").submit(function(e) {
            if (archivosDisponibles) {
                if (!banderaSubida) {
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                    banderaSubida = true;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        });
        myDropzone.on("queuecomplete", function() {
            //return true;
            $("#agregarReservaForm").submit();
        });

        myDropzone.on("addedfile", function(file) {
            archivosDisponibles = true;
        });

        $('#cantidadAcompaniantes').on('input', function(e) {
            alert('Ingreso!'.concat($('#cantidadAcompaniantes').val()));

            return true;
        });
        $(".numericOnly").bind('keypress', function(e) {
            if (e.keyCode == '9' || e.keyCode == '16') {
                return;
            }
            var code;
            if (e.keyCode) code = e.keyCode;
            else if (e.which) code = e.which;
            if (e.which == 46)
                return false;
            if (code == 8 || code == 46)
                return true;
            if (code < 48 || code > 57)
                return false;
        });
        $(".numericOnly").bind("paste", function(e) {
            e.preventDefault();
        });
        $(".numericOnly").bind('mouseenter', function(e) {
            var val = $(this).val();
            if (val != '0') {
                val = val.replace(/[^0-9]+/g, "")
                $(this).val(val);
            }
        });
    });
    $("#botonPregunta").on("click", function() {
        $.ajax({
            type: "POST",
            url: '<?php echo base_url('
            index.php / '); ?>/reservaciones/consultaDisponibilidadDepartamento',
            data: {
                fechaInicio: $("#reservaFechaInicio").val(),
                fechaTermino: $("#reservaFechaTermino").val()
            },
            dataType: "json",
            cache: false,
            success: function(data) {
                if ($('#tablaDepartamentos > tbody').children().length != 0) {
                    $("#tablaDepartamentos > tbody").empty();
                }

                $.each(data, function(linktext, link) {
                    var cadena = '<tr>';
                    cadena = cadena + '<td>' + link.nombreEdificio + ' </td>';
                    cadena = cadena + '<td>' + link.numDpto + ' </td>';
                    cadena = cadena + '<td>' + link.cantDormitorios + ' </td>';
                    cadena = cadena + '<td>' + link.metrosConstruidos + ' </td>';
                    cadena = cadena + '<td>' + link.capacidadPersonas + ' </td>';
                    cadena = cadena + '<td><input type="checkbox" name="departamentoSeleccionado" id="ed' + link.idEdificio + 'dep' + link.numDpto + '" value="ed' + link.idEdificio + 'dep' + link.numDpto + '"><label for="ed' + link.idEdificio + 'dep' + link.numDpto + '">Clickar</label></td>';
                    cadena = cadena + '<td><button class="botonEliminar btn btn-default">Eliminar</button></td>';
                    cadena = cadena + '</tr>';
                    $('#tablaDepartamentos > tbody').append(cadena);
                });
            }

        });


    });

    $(document).on('click', 'button.botonEliminar', function() { // <-- changes
        $(this).closest('tr').remove();
        return false;
    });



    function actualizarRestante(cb) {
        if (cb.checked) cantidadRestante += 1;
        else cantidadRestante -= 1;
        $("#cantidadRestante").html(cantidadRestante);
    };
</script>

<div class="modal fade" id="modalReserva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Mi cuenta</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- /.Modal de cuenta -->