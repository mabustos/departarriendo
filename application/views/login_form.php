<div class="form-box" id="login-box">
    <div class="header">Departarriendo - Acceso</div>
    <?php echo form_open( '/login', 'role="form"');?>
    <div class="body bg-gray">
        <fieldset>
            <div class="form-group">
                <input class="form-control" placeholder="E-mail" name="email" autofocus>
            </div>
            <div class="form-group">
                <input class="form-control" placeholder="Contraseña" name="password" type="password" value="">
            </div>
            <div class="form-group">
                        <input type="checkbox" name="remember_me"/> Remember me
            </div>
        </fieldset>
    </div>
    <div class="footer">
        <button type="submit" class="btn bg-olive btn-block">Ingresar</button>

        <p><a href="<?php echo base_url('index.php/login/recuperar_contrasenia'); ?>">He olvidado mi contraseña</a>
        </p>

    </div>
    </form>
</div>
