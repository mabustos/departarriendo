<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	<div class="row">
	</div>
		<h1>Reserva #<?php echo $idReserva; ?></h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-6">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Datos de la reserva</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->

					<ul class="nav nav-tabs">
						<li class="active"><a href="#reservaOcupacion" data-toggle="tab">Datos monetarios</a>
						</li>
						<li><a href="#reservaCliente" data-toggle="tab">Datos del cliente</a>
						</li>
						<li><a href="#reservaViaje" data-toggle="tab">Datos del viaje</a>
						</li>
						<li><a href="#reservaAcompaniantes" data-toggle="tab">Datos de acompañantes</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="reservaOcupacion">
							<div class="box-body">
								<!-- INICIO TAB 1 -->
								<div class="box-body table-responsive no-padding">
									<table class="table table-hover">
										<tr>
											<td class="columna-valor">Estado</td>
											<td class="<?php echo $estadoColor; ?>"><?php echo $estado; ?></td>
										</tr>
										<tr>
											<td class="columna-valor">Fecha de creación</td>
											<td><?php echo $fechaCreacion; ?></td>
										</tr>
										<tr>
											<td class="columna-valor">Total ocupación</td>
											<td class="precio"><?php echo $valorReserva; ?></td>
										</tr>
										<tr>
											<td class="columna-valor">Total descuentos</td>
											<td ><?php echo $descuento.'%'; ?></td>
										</tr>
										<tr>
											<td class="columna-valor">Total reserva</td>
											<td class="precio"><?php echo $totalReserva; ?></td>
										</tr>
										<tr>
											<td class="columna-valor">Inicio de reserva de estacionamiento</td>
											<td><?php echo $estacInicio; ?></td>
										</tr>
										<tr>
											<td class="columna-valor">Inicio de reserva de estacionamiento</td>
											<td><?php echo $estacFin; ?></td>
										</tr>

									</table>
								</div><!-- /.box-body -->
								<!-- FIN TAB 1 -->
							</div>
						</div>
						<div class="tab-pane" id="reservaCliente">
							<div class="box-body">
								<!-- INICIO TAB 1 -->
								<div class="box-body table-responsive no-padding">
									<table class="table table-hover">
										<tr>
											<td class="columna-valor">Nombre</td>
											<td><?php echo '<a href ="'.(base_url('index.php/clientes/visualizarCliente/')).'/'.$idCliente.'">'.$nombreCliente.'</td>'; ?></td>
										</tr>
										<tr>
											<td class="columna-valor">Tarjeta de crédito</td>
											<td><?php echo $idTarjeta; ?></td>
										</tr>
										<tr>
											<td class="columna-valor">Código CVC</td>
											<td><?php echo $codigoCVC; ?></td>
										</tr>
										<tr>
											<td class="columna-valor">Fecha de expiración (mm-YYYY)</td>
											<td><?php echo $mesExpiracion.'-'.$anioExpiracion; ?></td>
										</tr>

									</table>
								</div><!-- /.box-body -->
								<!-- FIN TAB 1 -->
							</div>
						</div>
						<div class="tab-pane" id="reservaViaje">
							<div class="box-body">
								<!-- INICIO TAB 1 -->
								<div class="box-body table-responsive no-padding">
									<table class="table table-hover">
										<tr>
											<td class="columna-valor">Motivo del viaje</td>
											<td><?php echo $motivoViaje; ?></td>
										</tr>
										<tr>
											<td class="columna-valor">Modo de llegada</td>
											<td><?php echo $modoLlegada; ?></td>
										</tr>
										<tr>
											<td class="columna-valor">Fecha de llegada</td>
											<td id="fechaLlegada"><?php echo $fechaLlegada; ?></td>
										</tr>
										<tr>
											<td class="columna-valor">Hora de llegada</td>
											<td id="horaLlegada"><?php echo $horaLlegada; ?></td>
										</tr>
										<tr>
											<td class="columna-valor">Empresa</td>
											<td><?php echo $idEmpresa; ?></td>
										</tr>
										<tr>
											<td class="columna-valor">Número de vuelo</td>
											<td><?php echo $numeroVuelo; ?></td>
										</tr>
										<tr>
											<td class="columna-valor">Peticiones adicionales</td>
											<td><?php echo $peticionAdicional; ?></td>
										</tr>
									</table>
								</div><!-- /.box-body -->
								<!-- FIN TAB 1 -->
							</div>
						</div>
						<div class="tab-pane" id="reservaAcompaniantes">
							<div class="box-body">
								<!-- INICIO TAB 1 -->
								<div class="box-body table-responsive no-padding">
									<table class="table table-hover">
										<tr>
											<td class="columna-valor">Cantidad de acompañantes</td>
											<td><?php echo $cantidadAcompaniantes; ?></td>
										</tr>
										<tr>
											<td class="columna-valor">Lista de acompañantes</td>
											<td>

											<div class="table-responsive">
											<table class="table">
												<thead>
													<tr>
														<td>#</td>
														<td>Fotografía</td>
													</tr>
												</thead>
												<tbody>
											    <?php
												if(!empty($fotografias)){
													foreach($fotografias as $fotografia){
														echo '<tr>';
														echo '<td>';
														echo $fotografia['numFotografia'];
														echo '</td>';
														echo '<td>';
														echo '<a href="'.base_url('uploads/reservas/')."/".$fotografia['nombreArchivo'].'" data-lightbox="fotografiasReserva" data-title="Fotografía #'.$fotografia['numFotografia'].' de la reserva" >';
														echo "<img src='".base_url('uploads/reservas/')."/".$fotografia['nombreArchivo']."' class='img-responsive-fix'></a>";
														echo '</td>';
														echo '</tr>';
												    }
												}
												?>
												</tbody>
											</table>
											</div>

											</td>
										</tr>


									</table>
								</div><!-- /.box-body -->
								<!-- FIN TAB 1 -->
							</div>
						</div>
					</div>

					<!-- /.box-body -->
					<!-- input states -->
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
			<div class="col-md-6">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Departamentos reservados</h3>
					</div>
					<div class="box-body">
						<!-- INICIO CALENDARIO -->
						<div id='calendar'>
						</div>
						<!-- FIN CALENDARIO -->
						<!-- INICIO TAB 1 -->
						<div class="box-body table-responsive no-padding">
							<table class="table table-hover">
								<thead>
									<th>Edificio</th>
									<th>Núm. dpto</th>
									<th>Fecha llegada</th>
									<th>Fecha partida</th>
									<th>Capacidad</th>
									<th>Precio Total</th>
									<th>Estado</th>
								</thead>
								<tbody>
									<?php
										if(!empty($ocupaciones)){
										foreach($ocupaciones as $ocupacion){
											echo '<tr>';
											echo '<td>';
											echo $ocupacion['idEdificio'];
											echo '</td>';
											echo '<td>';
											echo $ocupacion['numDpto'];
											echo '</td>';
											echo '<td>';
											echo $ocupacion['fechaLlegada'];
											echo '</td>';
											echo '<td>';
											echo $ocupacion['fechaPartida'];
											echo '</td>';
											echo '<td>';
											echo $ocupacion['capacidad'];
											echo '</td>';
											echo '<td>';
											echo $ocupacion['precioTotal'];
											echo '</td>';
											echo '<td class="'.$ocupacion['estadoColor'].'">';
											echo $ocupacion['estado'];
											echo '</td>';
											echo '</tr>';
										}
									}
									?>
								</tbody>
							</table>
						</div><!-- /.box-body -->
						<!-- FIN TAB 1 -->
					</div>
				</div>
			</div>
		</div>
		<!--/.col (right) -->
		<!-- /.row -->
	</section>
	<!-- /.content -->
</aside>

<script type="text/javascript">
			$(function() {
				$('#horaLlegada').mask('00:00:00');
				$('.precio').mask('000.000.000.000.000', {reverse: true});

				$('#calendar').fullCalendar({
					header: {
						left: 'prev,next today',
						center: 'title',
						right: 'month,agendaWeek,agendaDay'
					},
					events: [
						<?php
							foreach($ocupaciones as $ocupacion){
								echo '{ ';
								echo 'title   : "'.$ocupacion['nombreEdificio'].' #'.$ocupacion['numDpto'].'",';
								echo 'start   : "'.$ocupacion['fechaLlegada'].'",';
								echo 'end     : "'.$ocupacion['fechaPartida'].'",';
								echo 'allDay: false';
								echo '},';
							}
						?>
					],
					defaultView: 'month',
					defaultDate: '2014-11-12',
					selectable: true,
					eventLimit: true // allow "more" link when too many events

				});


			});
</script>
