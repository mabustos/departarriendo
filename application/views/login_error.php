
    <!-- Danger box -->
    <div class="box box-solid bg-red">
        <div class="box-header">
            <h3 class="box-title">Intento de acceso fallido</h3>
        </div>
        <div class="box-body">
            <p>
                Datos de ingreso erróneos. Por favor, intente nuevamente.
            </p>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
