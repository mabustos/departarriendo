<aside class="right-side">
    <!-- Content Header (Page header) -->

    <section class="content-header">
        <h1>Editar departamento <?php echo $nombreEdificio[$idEdificioSeleccionado]; ?></h1>
    </section>

    <section class="content-header">
        <?php echo form_open( 'departamentos/editarDepartamento/'.$idEdificio[$idEdificioSeleccionado].'/'.$numDpto['value'], 'id="editarDepartamentoForm" class="form"');?>
        <button type="submit" class="btn btn-primary">Guardar cambios</button>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->

                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Datos de departamento</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    <?php
                        if (validation_errors()) {
                            echo '<div class="alert alert-warning alert-dismissable">
                                                                <i class="fa fa-warning"></i>' . validation_errors() . '</div>';
                        } else {
                            if ($datosIdenticos == TRUE) {
                                echo '<div class="alert alert-warning alert-dismissable">
                                                                <i class="fa fa-warning"></i>Los datos son idénticos</div>';
                            } else {
                                if (isset($ingresoCorreto)) { {
                                        if ($ingresoCorreto) {
                                            echo '<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i>Departamento editado  correctamente</div>';
                                        } else {
                                            echo '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>No se ha podido editar el departamento</div>';
                                        }
                                    }
                                }
                            }
                        }
                        ?>


                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#departamentoPrincipal" data-toggle="tab">Datos principales</a>
                        </li>
                        <li><a href="#departamentoInterior" data-toggle="tab">Datos del interior</a>
                        </li>
                        <li><a href="#departamentoAdicionales" data-toggle="tab">Datos adicionales</a>
                        </li>
                        <li><a href="#departamentoPrecios" data-toggle="tab">Precios</a>
                        </li>
                        <li><a href="#departamentoMultimedia" data-toggle="tab">Multimedia</a>
                        </li>
                        <li><a href="#departamentoAgregarMultimedia" data-toggle="tab">Agregar Multimedia</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="departamentoPrincipal">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Nombre del edificio</label>

                                    <?php echo form_dropdown( 'idEdificio', $nombreEdificio, $idEdificioSeleccionado, 'id="idEdificio" class="form-control"'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Nº de departamento</label>
                                    <?php echo form_input($numDpto); ?>
                                </div>
                                <div class="form-group">
                                    <label>Nº de piso</label>
                                    <?php echo form_dropdown( 'numPiso', $pisosEdificio, $numPisoSeleccionado, 'id="numPiso" class="form-control"'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Orientación del departamento</label>
                                    <?php echo form_dropdown( 'orientacion', $orientacion, $orientacionSeleccionado, 'class="form-control"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="departamentoInterior">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Nº de habitaciones</label>
                                    <?php echo form_input($cantDormitorios); ?>
                                </div>
                                <div class="form-group">
                                    <label>Nº de baños</label>
                                    <?php echo form_input($cantBanios); ?>
                                </div>
                                <div class="form-group">
                                    <label>M^2 Construidos</label>
                                    <?php echo form_input($metrosConstruidos); ?>
                                </div>
                                <div class="form-group">
                                    <label>Capacidad de personas</label>
                                    <?php echo form_input($capacidadPersonas); ?>
                                </div>
                                <div class="form-group">
                                    <label>Descripción</label>
                                    <?php echo form_input($descripcion); ?>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="departamentoAdicionales">
                            <div class="box-body">
                                <div class="form-group">
                                    <?php echo form_checkbox($dispWIFI); ?>
                                    <label for="dispWIFI">WIFI</label>
                                </div>
                                <div class="form-group">
                                    <?php echo form_checkbox($dispCocina); ?>
                                    <label for="dispCocina">Cocina</label>
                                </div>
                                <div class="form-group">
                                    <?php echo form_checkbox($dispCable); ?>
                                    <label for="dispCable">Cable</label>
                                </div>
                                <div class="form-group">
                                    <?php echo form_checkbox($dispRefrigerador); ?>
                                    <label for="dispRefrigerador">Refrigerador</label>
                                </div>
                                <div class="form-group">
                                    <?php echo form_checkbox($dispCalefactor); ?>
                                    <label for="dispCalefactor">Calefactor</label>
                                </div>
                                <div class="form-group">
                                    <?php echo form_checkbox($dispMicroondas); ?>
                                    <label for="dispMicroondas">Microondas</label>
                                </div>
                            </div>
                      </div>

                        <!-- INICIO DE PRECIOS -->
                    <div class="tab-pane" id="departamentoPrecios">
                            <div class="box-body">

                                <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <td># de personas</td>
                                            <td>Precio actual</td>
                                            <td>Nuevo precio </td>
                                        </tr>
                                    </thead>
                                    <?php

                                        for($i=1;$i<=intval($capacidadPersonas['value']);$i++){
                                            echo '<tr>';
                                            echo '<td>';
                                            echo ($i);
                                            echo '</td>';
                                            echo '<td>';
                                            if(!empty($precio)){
                                                if($precio[$i]){
                                                    echo $precio[$i]['value'];
                                                }
                                            }
                                            else{
                                                echo 'No está fijado';
                                            }
                                            echo '</td>';
                                            echo '<td>';
                                            echo form_input($precio[$i]);
                                            echo '</td>';
                                            echo '</tr>';
                                        }
                                    ?>
                              </table>
                            </div>

                            </div>
                      </div>
                        <!--FIN DE PRECIOS-->


                        <!-- FIN DE FORMULARIO (SIN IMAGENES)!-->
                        <div class="tab-pane" id="departamentoMultimedia">
                            <div class="box-body">
                                <!-- INICIO TAB 2 -->

                                <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <td>#</td>
                                            <td>Fotografía</td>
                                            <td>Estado</td>
                                        </tr>
                                    </thead>
                                    <?php
                                    if(!empty($fotografias)){
                                        $i=0;
                                        foreach($fotografias as $item){
                                            echo '<tr>';
                                            echo '<td>';
                                            echo ($i+1);
                                            echo '</td>';
                                            echo '<td>';
                                            echo '<a href="'.base_url('uploads/departamentos/')."/".$item.'" data-lightbox="fotografiasDepartamento" data-title="Fotografía #'.($i+1).' del departamento" >';
            echo "<img src='".base_url('uploads/departamentos/')."/".$item."' class='img-responsive-fix'></a>";
                                            echo '</td>';
                                            echo '<td>';
                                            echo '<input type="checkbox" value="aceptado" name="'.$item.'" id="'.$item.'" onclick="actualizarRestante(this);">';
                                            echo '<label for="'.$item.'">¿Eliminar?</label>';
                                            echo '</td>';
                                            echo '</tr>';
                                            $i++;
                                        }
                                    }

                                    ?></form>
  </table>
</div>
                                <!-- FIN TAB 2 -->
                            </div>
                        </div>
                    <div class="tab-pane" id="departamentoAgregarMultimedia">
                            <div class="box-body">
                                <div class="callout callout-info">
                                        <h4>Información</h4>
                                        <p>Como máximo se permiten <strong>15</strong> fotografías por departamento. <em>Restan (<span id="cantidadRestante"></span>) fotografías</em></p>
                                    </div>
                                    <div id="imagenesDepartamento" class="dropzone">

                                    </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- /.TAB CONTENT -->

                <!-- /.box-header -->
                <!-- textarea -->
                <!-- input states -->


            </div>
            <!-- /.box-body -->
            <!-- input states -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</aside>

<script>
var maximoFotografias = 15;
var cantidadRestante = <?php if(isset($fotografias)) echo (15-count($fotografias));
                               else echo 15?>; // Cantidad restante de fotografías
$("#cantidadRestante").html(cantidadRestante);

$(document).ready(function() {
  var myDropzone = new Dropzone("div#imagenesDepartamento", { url: "<?php echo base_url('index.php/departamentos/subirImagenesDepartamento'); ?>"});
  var banderaSubida = false;
  var archivosDisponibles = false;
  jQuery("#editarDepartamentoForm").submit(function(e) {
      if(archivosDisponibles){
          if(!banderaSubida){
             e.preventDefault();
             e.stopPropagation();
             myDropzone.processQueue();
             banderaSubida=true;
          }
          else{
              return true;
          }
      }
      else{
          return true;
      }
    });
  myDropzone.on("queuecomplete", function() {
      //return true;
      $("#editarDepartamentoForm").submit();
    });

  myDropzone.on("addedfile", function(file) {
      archivosDisponibles = true;
  });



  // Primera carga de la página. Enumera cantidad de pisos del elemento por defecto.
  document.getElementById("numPiso").options.length = 0;
  var idEdificio = document.getElementById("idEdificio");
  var valorIdEdificio = idEdificio.options[idEdificio.selectedIndex].value;
  switch (valorIdEdificio) {
        <?php
        foreach (array_keys( $pisosEdificio ) as $index=>$key) {
            echo 'case "'.$key.'":'.PHP_EOL;
            for($i=1;$i<=$pisosEdificio[$key];$i=$i+1){
                echo 'var option = document.createElement("option");
                          option.text = '.$i.';
                          option.value = '.$i.';'.PHP_EOL;
                echo 'try {
                          numPiso.add(option, null); //Standard
                      }catch(error) {
                          numPiso.add(option); // IE only
                      }';
            }
            echo 'break;'.PHP_EOL;
        }
        ?>
    }
    $("#numPiso").val(<?php echo $numPisoSeleccionado; ?>);

   // Accionado al momento de seleccionar el combobox idEdificio.
   //Enumera cantidad de pisos del elemento seleccionado.
  $("#idEdificio").change(function() {

    var idEdificio = document.getElementById("idEdificio");
    var numPiso = document.getElementById("numPiso");
    var valorIdEdificio = idEdificio.options[idEdificio.selectedIndex].value;
    numPiso.options.length = 0;

    switch (valorIdEdificio) {
        <?php
        foreach (array_keys( $pisosEdificio ) as $index=>$key) {
            echo 'case "'.$key.'":'.PHP_EOL;
            for($i=1;$i<=$pisosEdificio[$key];$i=$i+1){
                echo 'var option = document.createElement("option");
                          option.text = '.$i.';
                          option.value = '.$i.';'.PHP_EOL;
                echo 'try {
                          numPiso.add(option, null); //Standard
                      }catch(error) {
                          numPiso.add(option); // IE only
                      }';
            }
            echo 'break;'.PHP_EOL;
        }
        ?>
    }
  });
});
  function actualizarRestante(cb) {
      if(cb.checked) cantidadRestante+=1;
      else cantidadRestante-=1;
      $("#cantidadRestante").html(cantidadRestante);
  };
</script>
