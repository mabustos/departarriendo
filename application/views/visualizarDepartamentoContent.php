<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="row">
      </div>
        <h1>Departamento </h1>


    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->

            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Datos del departamento <?php  echo '#'.$numDpto.' Edificio '.$edificios[$idEdificio]; ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    <div class="row">

                        <div class="col-md-12">

                            <ul class="nav nav-tabs">
                        <li class="active"><a href="#departamentoPrincipal" data-toggle="tab">Departamento</a>
                        </li>
                        <li><a href="#departamentoInterior" data-toggle="tab">Interior</a>
                        </li>
                        <li><a href="#departamentoPrecios" data-toggle="tab">Precios</a>
                        </li>
                        <li><a href="#departamentoAdicional" data-toggle="tab">Adicional</a>
                        </li>
                        <li><a href="#departamentoMultimedia" data-toggle="tab">Multimedia</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="departamentoPrincipal">
                            <div class="box-body">
                                <!-- INICIO TAB 1 -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover">
                                        <tr>
                                            <td class="columna-valor">Edificio</td>
                                            <td><?php  echo '<a href="'.base_url("index.php/edificios/visualizarEdificio/".$idEdificio).'">'.$edificios[$idEdificio].'</a>'; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Nº Dpto</td>
                                            <td><?php  echo $numDpto; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Piso</td>
                                            <td><?php  echo $numPiso; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Orientación</td>
                                            <td><?php  echo $orientacion; ?></td>
                                        </tr>
                                    </table>
                                </div><!-- /.box-body -->
                                <!-- FIN TAB 1 -->
                            </div>
                        </div>
                        <div class="tab-pane" id="departamentoInterior">
                            <div class="box-body">
                                <!-- INICIO TAB 2 -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover">
                                        <tr>
                                            <td class="columna-valor">Habitaciones</td>
                                            <td><?php  echo $cantDormitorios; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Baños</td>
                                            <td><?php  echo $cantBanios; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">M2 construídos</td>
                                            <td><?php  echo $metrosConstruidos; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Capacidad personas</td>
                                            <td><?php  echo $capacidadPersonas; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Descripción</td>
                                            <td><?php  echo $descripcion; ?></td>
                                        </tr>
                                    </table>
                                </div><!-- /.box-body -->
                                <!-- FIN TAB 2 -->
                            </div>
                         </div>
                            <!-- INICIO DE PRECIOS -->
                    <div class="tab-pane" id="departamentoPrecios">
                            <div class="box-body">

                                <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <td># de personas</td>
                                            <td>Precio actual</td>

                                        </tr>
                                    </thead>
                                    <?php

                                        for($i=1;$i<=intval($capacidadPersonas);$i++){
                                            echo '<tr>';
                                            echo '<td>';
                                            echo ($i);
                                            echo '</td>';
                                            echo '<td>';
                                            if(!empty($precios)){
                                                if($precios[$i]){
                                                    echo $precios[$i]['precio'];
                                                }
                                            }
                                            else{
                                                echo 'No está fijado';
                                            }
                                            echo '</td>';
                                            echo '</tr>';
                                        }
                                    ?>
                              </table>
                            </div>

                            </div>
                      </div>
                        <!--FIN DE PRECIOS-->
                         <div class="tab-pane" id="departamentoAdicional">
                            <div class="box-body">
                                <!-- INICIO TAB 2 -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover">
                                        <tr>
                                            <td class="columna-valor">WIFI</td>
                                            <td><?php

	if ($dispWIFI){
		echo '<span class="glyphicon glyphicon-ok"></span>';
	} else {
		echo '<span class="glyphicon glyphicon-remove"></span>';
	}

	?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Cocina</td>
                                            <td><?php

	if ($dispCocina){
		echo '<span class="glyphicon glyphicon-ok"></span>';
	} else {
		echo '<span class="glyphicon glyphicon-remove"></span>';
	}

	?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Cable</td>
                                            <td><?php

	if ($dispCable){
		echo '<span class="glyphicon glyphicon-ok"></span>';
	} else {
		echo '<span class="glyphicon glyphicon-remove"></span>';
	}

	?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Refrigerador</td>
                                            <td><?php

	if ($dispRefrigerador){
		echo '<span class="glyphicon glyphicon-ok"></span>';
	} else {
		echo '<span class="glyphicon glyphicon-remove"></span>';
	}

	?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Calefactor</td>
                                            <td><?php

	if ($dispCalefactor){
		echo '<span class="glyphicon glyphicon-ok"></span>';
	} else {
		echo '<span class="glyphicon glyphicon-remove"></span>';
	}

	?></td>
                                        </tr>
                                        <tr>
                                            <td class="columna-valor">Microondas</td>
                                            <td><?php

	if ($dispMicroondas){
		echo '<span class="glyphicon glyphicon-ok"></span>';
	} else {
		echo '<span class="glyphicon glyphicon-remove"></span>';
	}

	?></td>
                                        </tr>
                                    </table>
                                </div><!-- /.box-body -->
                                <!-- FIN TAB 2 -->
                            </div>
                         </div>
                        <div class="tab-pane" id="departamentoMultimedia">
                            <div class="box-body">

<!-- INICIO TAB 2 -->

<div class="table-responsive">
<table class="table">
    <thead>
        <tr>
            <td>#</td>
            <td>Fotografía</td>
        </tr>
    </thead>
<?php
                                    if(!empty($fotografias)){
                                        $i=0;
                                        foreach($fotografias as $item){
                                            echo '<tr>';
                                            echo '<td>';
                                            echo ($i+1);
                                            echo '</td>';
                                            echo '<td>';
                                            echo '<a href="'.base_url('uploads/departamentos/')."/".$item.'" data-lightbox="fotografiasDepartamento" data-title="Fotografía #'.($i+1).' del departamento" >';
            echo "<img src='".base_url('uploads/departamentos/')."/".$item."' class='img-responsive-fix'></a>";
                                            echo '</td>';
                                            echo '</tr>';
                                            $i++;
                                        }
                                    }

                                    ?>
  </table>
</div>


                                <!-- FIN TAB 2 -->

                            </div>
                         </div>
                    </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <!-- input states -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
        <!-- /.row -->
    </section>
    <!-- /.content -->
</aside>
