
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->

            <div class="user-panel">
                <div class="pull-left info">
                  <div class="row">
                    <div class="col-lg-9 col-xs-6">
                      <div class="fecha"></div>
                    </div>
                    <div class="col-lg-3 col-xs-6">
                      <div class="reloj"></div>
                    </div>
                  </div>



                </div>

            </div>

            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <!-- Menú - calendario !-->
                <li>
                  <a href="<?php echo base_url('index.php/dashboard'); ?>">
                        <i class="fa fa-calendar"></i>  <span>Calendario</span>
                    </a>
                </li>
                <!-- Menú - calendario !-->
                <!-- Menú - edificios !-->
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-building-o"></i>
                        <span>Edificios</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('index.php/edificios/agregarEdificio'); ?>"><i class="fa fa-angle-double-right"></i> Agregar edificios</a>
                        </li>
                        <li><a href="<?php echo base_url('index.php/edificios/listarEdificios'); ?>"><i class="fa fa-angle-double-right"></i> Listar edificios</a>
                        </li>
                    </ul>
                </li>
                <!-- Menú - edificios !-->
                <!-- Menú - departamentos !-->
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-key"></i>
                        <span>Departamentos</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('index.php/departamentos/agregarDepartamento'); ?>"><i class="fa fa-angle-double-right"></i> Agregar departamentos</a>
                        </li>
                        <li><a href="<?php echo base_url('index.php/departamentos/listarDepartamentos'); ?>"><i class="fa fa-angle-double-right"></i> Listar departamentos</a>
                        </li>
                    </ul>
                </li>
                <!-- Menú - departamentos !-->
                <!-- Menú - clientes !-->
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-group"></i>
                        <span>Clientes</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('index.php/clientes/agregarCliente'); ?>"><i class="fa fa-angle-double-right"></i> Agregar clientes</a>
                        </li>
                        <li><a href="<?php echo base_url('index.php/clientes/listarClientes'); ?>"><i class="fa fa-angle-double-right"></i> Listar clientes</a>
                        </li>
                    </ul>
                </li>
                <!-- Menú - clientes !-->
                <!-- Menú - reservaciones !-->
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-suitcase"></i>
                        <span>Reservaciones</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('index.php/reservaciones/agregarReserva'); ?>"><i class="fa fa-angle-double-right"></i> Agregar reserva</a>
                        </li>
                        <li><a href="<?php echo base_url('index.php/reservaciones/listarReservas'); ?>"><i class="fa fa-angle-double-right"></i> Listar reservas</a>
                        </li>
                    </ul>
                </li>
                <!-- Menú - reservaciones !-->
                <!-- Menú -  !-->
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-bar-chart-o"></i>
                        <span>Indicadores y precios</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('index.php/indicadoresPrecios/listarPrecios'); ?>"><i class="fa fa-angle-double-right"></i> Precios</a>
                        </li>
                        <li><a href="<?php echo base_url('index.php/indicadoresPrecios/informes'); ?>"><i class="fa fa-angle-double-right"></i> Informe</a>
                        </li>
                        <li><a href="<?php echo base_url('index.php/indicadoresPrecios/indicadores'); ?>"><i class="fa fa-angle-double-right"></i> Indicadores económicos</a>
                        </li>
                    </ul>
                </li>
                <!-- Menú - clientes !-->
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
