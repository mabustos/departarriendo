<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Agregar Edificios </h1>
    </section>

    <!-- Main content -->
    <section class="content-header">
        <?php echo form_open( 'edificios/agregarEdificio', 'class="form"');?>
        <button type="submit" class="btn btn-primary">Ingresar Edificio</button>
    </section>
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Datos del edificio</h3>
                    </div>
                    <?php
                        if(validation_errors()){
                            echo '<div class="alert alert-warning alert-dismissable">
                                        <i class="fa fa-warning"></i>'.validation_errors(). '</div>';
                        }
                        else{
                            if(isset($mensaje)){
                                echo $mensaje;

                            }
                        }
                        ?>

                    <!-- /.box-header -->
                    <!-- form start -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#edificioPrincipal" data-toggle="tab">Datos principales</a>
                        </li>
                        <li><a href="#edificioAdministracion" data-toggle="tab">Datos de administración</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="edificioPrincipal">
                            <div class="box-body">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Nombre del edificio</label>
                                    <?php echo form_input($nombreEdificio); ?>
                                </div>
                                <div class="form-group">
                                    <label>Cantidad de pisos</label>
                                    <?php echo form_input($cantidadPisos); ?>
                                </div>
                                <div class="form-group">
                                    <label>Dirección</label>
                                    <?php echo form_input($direccionEdificio); ?>
                                </div>
                                <div class="form-group">
                                    <label>Número</label>
                                    <?php echo form_input($numeroEdificio); ?>
                                </div>

                                <div class="form-group">
                                    <label>Ciudad</label>
                                    <?php echo form_dropdown( 'ciudadEdificio', $ciudadEdificio, $ciudadSeleccionada, 'class="form-control"'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Descripción</label>
                                    <?php echo form_input($descripcionEdificio); ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="edificioAdministracion">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Nombre del administrador</label>
                                    <?php echo form_input($nombreAdministrador); ?>
                                </div>
                                <div class="form-group">
                                    <label>E-Mail del administrador</label>
                                    <?php echo form_input($emailAdministrador); ?>
                                </div>
                                <div class="form-group">
                                    <label>Teléfono de administración</label>
                                    <?php echo form_input($telefonoAdministracion); ?>
                                </div>
                                <div class="form-group">
                                    <label>Teléfono de conserjería</label>
                                    <?php echo form_input($telefonoConsejeria); ?>
                                </div>
                                <div class="form-group">
                                    <label>Datos bancarios</label>
                                    <?php echo form_input($datosBancarios); ?>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <!-- textarea -->
                        <!-- input states -->
                        </form>
                    </div>
                    <!-- /.box-body -->
                    <!-- input states -->

                    <!-- /.box-body -->

                    <!-- /.box -->
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</aside>
