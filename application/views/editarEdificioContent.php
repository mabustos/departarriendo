<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Editar edificio <?php echo $nombreEdificio['value']; ?></h1>
    </section>

    <!-- Main content -->
    <section class="content-header">
        <?php echo form_open( 'edificios/editarEdificio/'.$idEdificio, 'class="form"',$idEdificio);?>
        <button type="submit" class="btn btn-primary">Ingresar Edificio</button>
    </section>
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Datos de ubicación</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">

                        <?php
                        if (validation_errors()) {
                            echo '<div class="alert alert-warning alert-dismissable">
                                                                <i class="fa fa-warning"></i>' . validation_errors() . '</div>';
                        } else {
                            if ($datosIdenticos == TRUE) {
                                echo '<div class="alert alert-warning alert-dismissable">
                                                                <i class="fa fa-warning"></i>Los datos son idénticos</div>';
                            } else {
                                if (isset($ingresoCorreto)) { {
                                        if ($ingresoCorreto) {
                                            echo '<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i>Edificio editado  correctamente</div>';
                                        } else {
                                            echo '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>No se ha podido editar el edificio</div>';
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                        
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#edificioPrincipal" data-toggle="tab">Datos principales</a>
                        </li>
                        <li><a href="#edificioAdministracion" data-toggle="tab">Datos de administración</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="edificioPrincipal">
                            <div class="box-body">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Nombre del edificio</label>
                                    <?php echo form_input($nombreEdificio,set_value('nombreEdificio')); ?>
                                </div>
                                <div class="form-group">
                                    <label>Cantidad de pisos</label>
                                    <?php echo form_input($cantidadPisos,set_value('cantidadPisos')); ?>
                                </div>
                                <div class="form-group">
                                    <label>Dirección</label>
                                    <?php echo form_input($direccionEdificio,set_value('direccionEdificio')); ?>
                                </div>
                                <div class="form-group">
                                    <label>Número</label>
                                    <?php echo form_input($numeroEdificio,set_value('numeroEdificio')); ?>
                                </div>

                                <div class="form-group">
                                    <label>Ciudad</label>
                                    <?php echo form_dropdown( 'ciudadEdificio', $ciudadEdificio, $ciudadSeleccionada, 'class="form-control"'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Descripción</label>
                                    <?php echo form_input($descripcionEdificio,set_value('descripcionEdificio')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="edificioAdministracion">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Nombre del administrador</label>
                                    <?php echo form_input($nombreAdministrador,$nombreAdministrador['value']); ?>
                                </div>
                                <div class="form-group">
                                    <label>E-Mail del administrador</label>
                                    <?php echo form_input($emailAdministrador,set_value('emailAdministrador')); ?>
                                </div>
                                <div class="form-group">
                                    <label>Teléfono de administración</label>
                                    <?php echo form_input($telefonoAdministracion,set_value('telefonoAdministracion')); ?>
                                </div>
                                <div class="form-group">
                                    <label>Teléfono de conserjería</label>
                                    <?php echo form_input($telefonoConsejeria,set_value('telefonoConsejeria')); ?>
                                </div>
                                <div class="form-group">
                                    <label>Datos bancarios</label>
                                    <?php echo form_input($datosBancarios,set_value('datosBancarios')); ?>
                                </div>
                            </div>
                        </div>


                        <!-- text input -->
                        
                        <!-- /.box-header -->
                        <!-- textarea -->
                        <!-- input states -->
                        </form>
                    </div>

                    </form>

                    <!-- /.box-body -->
                    <!-- input states -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</aside>


