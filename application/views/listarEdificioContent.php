<aside class="right-side">

<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Lista de edificios</h3>
                                </div><!-- /.box-header -->
                                <div class="box-header">
                                    <?php
                                    if(isset($mensaje)){
                                        echo $mensaje;
                                    }
                                    ?>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nombre</th>
                                                <th>Administrador</th>
                                                <th>Pisos</th>
                                                <th>Ciudad</th>
                                                <th>Ver</th>
                                                <th>Editar</th>
                                                <th>Eliminar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($edificios)){
                                                    foreach ($edificios as $row){
                                                        echo '<tr>';
                                                        echo '<td>'.$row['idEdificio'].'</td>';
                                                        echo '<td>'.$row['nombreEdificio'].'</td>';
                                                        echo '<td>'.$row['nombreAdministrador'].'</td>';
                                                        echo '<td>'.$row['cantidadPisos'].'</td>';
                                                        echo '<td>'.$ciudadEdificio[$row['ciudad']].'</td>';
                                                        echo '<td><a href ="'.(base_url('index.php/edificios/visualizarEdificio/')).'/'.$row['idEdificio'].' >" <i class="fa fa-fw fa-eye"></i></td>';
                                                        echo '<td><a href ="'.(base_url('index.php/edificios/editarEdificio/')).'/'.$row['idEdificio'].' >" <i class="fa fa-fw fa-edit"></i></td>';
                                                        echo '<td><a href ="#" class="botonEliminar"> <i class="fa fa-fw fa-minus"></i></td>';
                                                        echo '</tr>';
                                                    }
                                                }


                                            ?>
                                        </tbody>

                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->



<!-- Modal de confirmación -->
<div class="modal fade" id="modalReserva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div align="center">
                    <h2>Eliminación de edificio<h2> <h4 class="modal-title" style="display: inline-block;" id="modalNombreEdificio"></h4>
                    (identicador: <h4 class="modal-title" style="display: inline-block;" id="modalIdEdificio"></h4>)
                </div>
                </div>
            <div class="modal-body">
                <div class="alert alert-warning alert-dismissable">
                    <i class="fa fa-warning"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b>!Atención!</b> Continuar con la eliminación de este edificio conlleva la eliminación de los elementos que se indican más abajo.
                </div>

                <h2 class="modal-title" style="display: inline-block;">Departamentos</h2>
                <div class="table-responsive">
                    <table class="table" id="tablaConflictoDepartamentos">
                        <thead>
                            <tr>
                                <th>N° dep.</th>
                                <th>N° piso.</th>
                                <th>Enlace</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <h2 class="modal-title" style="display: inline-block;">Ocupaciones</h2>

                <div class="table-responsive">
                    <table class="table" id="tablaConflictoOcupaciones">
                        <thead>
                            <tr>
                                <th>N° dep.</th>
                                <th>N° ocu.</th>
                                <th>Enlace</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>




            </div>

            <div class="modal-footer">
                <div align="center">
                <a href="" id="modalEnlace"><button type="button" class="btn btn-default registroCalendario">Confirmar eliminación</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.Modal de confirmación -->

<script type="text/javascript">
            var enlaceEliminacion = '<?php echo (base_url('index.php/edificios/eliminarEdificio/') ); ?>';
            var enlaceDepartamento = '<?php echo (base_url('index.php/departamentos/visualizarDepartamento/') ); ?>';
            var enlaceReserva = '<?php echo (base_url('index.php/reservaciones/visualizarReserva/') ); ?>';
            var idEdificio = '';
            var nombreEdificio = '';
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });

                $('.botonEliminar').on("click",function(){
                    idEdificio = $(this)
                        .closest('tr')
                        .find('td:eq(0)')
                        .text()
                        .trim();
                    nombreEdificio = $(this)
                        .closest('tr')
                        .find('td:eq(1)')
                        .text();
                    $("#tablaConflictoDepartamentos tr").remove();
                    $("#tablaConflictoOcupaciones tr").remove();
                    $('#modalIdEdificio').text(idEdificio);
                    $('#modalNombreEdificio').text(nombreEdificio);
                    $("#modalEnlace").attr("href", enlaceEliminacion+'/'+idEdificio);
                    consultarDepartamentos();

                    $('#modalReserva')
                        .modal('show');
                })


            function consultarDepartamentos(){
                $.when($.ajax({
                        type: "POST",
                        url: '<?php echo base_url('index.php/') ;?>/edificios/consultarDependencias/',
                        data: {
                            idEdificio: idEdificio,
                            tipo: 'Dep'
                        },
                        dataType: "json",
                        cache: false,
                        success: function (data) {
                            /*$('#tablaDepartamentosReservados > tbody') // AQUI
                                .append(fila);*/
                            var listado = '';
                            $.each(data, function (linktext, link) {
                                var fila = '<tr>';
                                fila = fila + '<td> N° dep. ' + link.numDpto + '</td>';
                                fila = fila + '<td> N° piso ' + link.numPiso + '</td>';
                                fila = fila + '<td> <a href="' + enlaceDepartamento + '/' + idEdificio + '/' + link.numDpto + '">Enlace</a></td>';
                                fila = fila + '</tr>';
                                listado = listado + fila;
                            });
                            $('#tablaConflictoDepartamentos > tbody') // AQUI
                                .append(listado);
                        }
                        ,
                        error: function(xhr, textStatus, error){
                              console.log(xhr.statusText);
                              console.log(textStatus);
                              console.log(error);
                          }
                    }))
                    .then(function () {
                        consultarOcupaciones();
                    });

            };

            function consultarOcupaciones(){
                $.when($.ajax({
                        type: "POST",
                        url: '<?php echo base_url('index.php/') ;?>/edificios/consultarDependencias/',
                        data: {
                            idEdificio: idEdificio,
                            tipo: 'Ocu'
                        },
                        dataType: "json",
                        cache: false,
                        success: function (data) {
                            var listado = '';
                            $.each(data, function (linktext, link) {
                                var fila = '<tr>';
                                fila = fila + '<td> N° dep. ' + link.numDpto + '</td>';
                                fila = fila + '<td> N° ocu ' + link.capacidad + '</td>';
                                fila = fila + '<td> <a href="' + enlaceReserva + '/' + link.idReserva + '">Enlace</a></td>';
                                fila = fila + '</tr>';
                                listado = listado + fila;
                            });
                            $('#tablaConflictoOcupaciones > tbody') // AQUI
                                .append(listado);
                        },
                        error: function(xhr, textStatus, error){
                              console.log(xhr.statusText);
                              console.log(textStatus);
                              console.log(error);
                          }
                    }))
                    .then(function () {

                    });
            };

        });

        </script>
