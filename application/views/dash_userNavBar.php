<ul class="nav navbar-top-links navbar-right">
    <!-- /.dropdown -->
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-user">
            <li><a href="#"><i class="fa fa-user fa-fw"></i> Perfil de usuario</a>
            </li>
            <li><a href="#" data-toggle="modal" data-target="#modalCuenta"><i class="fa fa-gear fa-fw"></i> Configurar cuenta</a>
            </li>
            <li class="divider"></li>
            <li><a href="<?php echo base_url('index.php/login/cerrar_sesion'); ?>"><i class="fa fa-sign-out fa-fw"></i> Cerrar sesión</a>
            </li>
        </ul>
        <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown -->
</ul>
<!-- /.navbar-top-links -->

</div>


