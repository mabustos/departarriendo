<aside class="right-side">
    <ol class="breadcrumb">
        <li><a href="#">Inicio</a></li>
        <li><a href="#">Clientes</a></li>
        <li class="active">Agregar Cliente</li>
    </ol>
</aside>

<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="row">
      </div>
        <h1>Agregar Clientes</h1>
    </section>

  <section class="content-header">
    <?php echo form_open( 'clientes/agregarCliente', 'class="form" id="agregarClienteForm"');?>
        <button type="submit" class="btn btn-primary box-title">Ingresar Cliente</button>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Datos Cliente</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?php
                        if(validation_errors()){
                            echo '<div class="alert alert-warning alert-dismissable">
                                        <i class="fa fa-warning"></i>'.validation_errors(). '</div>';
                        }
                        else{
                            if(isset($ingresoCorreto)){
                                {
                                   if($ingresoCorreto){
                                       echo '<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i>Cliente ingresado correctamente</div>';
                                   }
                                   else{
                                        echo '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>No se ha podido agregar el cliente</div>';
                                   }
                                }

                            }
                        }
                        ?>
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#clienteDatosPersonales" data-toggle="tab">Datos personales</a>
                        </li>
                        <li><a href="#clienteDatosContacto" data-toggle="tab">Datos de contacto</a>
                        </li>
                        <li><a href="#clienteImagen" data-toggle="tab">Imagen del cliente</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <form role="form">
                            <div class="tab-pane active" id="clienteDatosPersonales">
                                <div class="box-body">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Rut o Pasaporte</label>
                                        <?php echo form_input($CIF); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Tratamiento</label>
                                        <?php echo form_dropdown( 'tratamientoCliente', $tratamientoCliente, '', 'id="tratamientoCliente" class="form-control"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Nombres</label>
                                        <?php echo form_input($nombreCliente); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Apellidos</label>
                                        <?php echo form_input($apellidoCliente); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Sexo</label>
                                        <?php echo form_dropdown( 'sexoCliente', $sexoCliente, (isset($sexoClienteSeleccionado))?$sexoClienteSeleccionado:'', 'id="sexoCliente" class="form-control"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Fecha de nacimiento</label>
                                        <?php echo form_input($fechaNacCliente); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Nacionalidad</label>
                                        <?php echo form_dropdown( 'nacionalidad', $nacionalidad, (isset($nacionalidadSeleccionado))?$nacionalidadSeleccionado:'', 'id="nacionalidad" class="form-control"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Profesión</label>
                                        <?php echo form_input($profesion); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="clienteDatosContacto">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Teléfono</label>
                                        <?php echo form_input($telefonoContacto); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Teléfono alternativo</label>
                                        <?php echo form_input($telefonoContactoAlt); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>E-mail</label>
                                        <?php echo form_input($email); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>E-mail alternativo</label>
                                        <?php echo form_input($emailAlt); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Skype</label>
                                        <?php echo form_input($skypeCliente); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Whatsapp</label>
                                        <?php echo form_input($whatsappCliente); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Contacto</label>
                                        <?php echo form_dropdown( 'contacto', $contacto, (isset($contactoSeleccionado))?$contactoSeleccionado:'', 'id="contacto" class="form-control"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Tarjeta de crédito</label>
                                        <?php echo form_dropdown( 'tipoTarjetaCliente', $tipoTarjetaCliente, (isset($tipoTarjetaSeleccionado))?tipoTarjetaSeleccionado:'', 'id="tipoTarjetaCliente" class="form-control"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Nº Tarjeta de crédito</label>
                                        <?php echo form_input($numTarjetaCliente); ?>
                                    </div>
                                </div>
                            </div>
                            </form> <!-- FIN DE FORMULARIO (SIN IMAGENES)!-->
                            <div class="tab-pane" id="clienteImagen">
                                <div class="box-body">
                                    <div id="imagenesCliente" class="dropzone">

                                    </div>
                                </div>
                            </div>
                            </form>
                            <!-- /.box-header -->
                            <!-- textarea -->
                            <!-- input states -->

                    </div>
                    <!-- /.box-body -->
                    <!-- input states -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</aside>


<script>
$(document).ready(function() {
  var myDropzone = new Dropzone("div#imagenesCliente", { url: "<?php echo base_url('index.php/clientes/subirImagenesCliente'); ?>"});
  var banderaSubida = false;
  var archivosDisponibles = false;
  jQuery("#agregarClienteForm").submit(function(e) {
      if(archivosDisponibles){
          if(!banderaSubida){
             e.preventDefault();
             e.stopPropagation();
             myDropzone.processQueue();
             banderaSubida=true;
          }
          else{
              return true;
          }
      }
      else{
          return true;
      }
    });
  myDropzone.on("queuecomplete", function() {
      //return true;
      $("#agregarClienteForm").submit();
    });

  myDropzone.on("addedfile", function(file) {
      archivosDisponibles = true;
  });

});
</script>

<script>
            $(function() {
                $('#fechaNacCliente')
                .datepicker({
                    format: 'dd/mm/yyyy',
                    startDate: obtenerFechaAhora()
                });
                
                //Money Euro
                $("[data-mask]").inputmask();

                //Date range picker
                $('#reservation').daterangepicker();
                //Date range picker with time picker
                $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                        {
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                                'Last 7 Days': [moment().subtract('days', 6), moment()],
                                'Last 30 Days': [moment().subtract('days', 29), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                            },
                            startDate: moment().subtract('days', 29),
                            endDate: moment()
                        },
                function(start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
                );

                //Timepicker
                $(".timepicker").timepicker({
                    showInputs: false
                });
            });

            function obtenerFechaAhora() {
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                var today = dd + '/' + mm + '/' + yyyy;
                return today;
            }
        </script>
