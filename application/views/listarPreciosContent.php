
<aside class="right-side">

<!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <div class="row">
                                        <div class="col-xs-10">
                                            <h3 class="box-title">Lista de precios</h3>     
                                        </div>
                                        <div class="col-xs-12">
                                        <button class="btn btn-warning" onClick ="popupImpresion();">Imprimir</button>         
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    
                                <div id="holaja" class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Edificio</th>
                                                <th>Departamento</th>
                                                <th>Cantidad de personas</th>
                                                <th>Precios</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                    foreach($Departamentos as $precio){
                                                        echo '<tr>';
                                                        echo '<td>';
                                                        echo $precio['idEdificio'];
                                                        echo '</td>';
                                                        echo '<td>';
                                                        echo $precio['numDpto'];
                                                        echo '</td>';
                                                        echo '<td>';
                                                        echo $precio['cantidadPersonas'];
                                                        echo '</td>';
                                                        echo '<td>';
                                                        echo $precio['precio'];
                                                        echo '</td>';
                                                        echo '</tr>';
                                                    }                        
                                            ?>
                                        </tbody>
                                        
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->

<script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    
            function popupImpresion(){
        var newwindow = window.open('<?php echo($enlaceImpresion) ?>','Impresión','height=200,width=150');
        return false;
    }
</script>

<script>
    
</script>
